#!/bin/zsh

ROS_SERVICES=("init" "cargo_prepare" "drone_prepare" "finish" "shutdown" "start_flight" "land" "wait" "continue" "emergency_land" "return_home" "land_now" "emergency_kill" "cancel_flight")

for service in $ROS_SERVICES
do
	touch "test_"$service".py"
done
