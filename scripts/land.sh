#!/usr/bin/zsh

az iot hub invoke-device-method --device-id cr-drone-1 --hub-name IoTHub-AV --method-name land --method-payload '{"timestamp": 1520601811, "device_id": "cr-drone-1", "device_type": "drone", "flight_id": "123", "operator_id": "cr-operator-1", "name": "land", "parameters": {} }'
