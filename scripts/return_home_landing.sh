#!/bin/zsh

idsreturnhome=('3' '2' '1')

x=('0.0' '0.0' '-10.099443977348372')

y=('0.0' '0.0' '-12.771505856302518' )

z=('0.0' '-25.0' '-25.0')

lat=('50.01828' '50.01828' '50.01818920190005')

lon=('21.98508' '21.98508' '21.984901798695162')

alt_amsl=('0.0' '25.0' '25.0')

PATH_SATELITES=./random_satelites.sh
PATH_ATTITUDE=./random_attitude.sh
REMAINING=80

for i in "${idsreturnhome[@]}"
do
   xcor=$x[$i]
   ycor=$y[$i]
   zcor=$z[$i]
   lat_global=$lat[$i]
   lon_global=$lon[$i]
   alt_global=$alt_amsl[$i]
   NUMBER_SATELITES=$("$PATH_SATELITES")
   QW=$("$PATH_ATTITUDE")
   QX=$("$PATH_ATTITUDE")
   QY=$("$PATH_ATTITUDE")
   QZ=$("$PATH_ATTITUDE")

   command_local='ros2 topic pub /px_control/local_pos drone_types/msg/PxControlLocalPos "{x: '$xcor', y: '$ycor', z: '$zcor'}" --once'
   eval $command_local

   command_global='ros2 topic pub /px_control/global_pos drone_types/msg/PxControlGlobalPos "{latitude: '$lat_global', longitude: '$lon_global', altitude: '$alt_global', num_sats: '$NUMBER_SATELITES'}" --once'
   eval $command_global

   command_attitude='ros2 topic pub /px_control/attitude drone_types/msg/PxControlAttitude "{q_w: '$QW', q_x: '$QX', q_y: '$QY', q_z: '$QZ'}" --once'
   eval $command_attitude

   command_battery='ros2 topic pub /px_control/battery drone_types/msg/PxControlBattery "{voltage: 12, current: 50, remaining: '$REMAINING'}" --once'
   eval $command_battery

   REMAINING=$(($REMAINING-1))

done

sleep 1.0

command_velocity='ros2 topic pub /px_control/velocity drone_types/msg/PxControlVelocity "{vx: 0.0, vy: 0.0, vz: 0.0}" --once'
eval $command_velocity

sleep 1.0

ros2 topic pub /px_control/status drone_types/msg/PxControlStatus "{landed_state: 1}" --once

echo 'Landing in return home finished'

