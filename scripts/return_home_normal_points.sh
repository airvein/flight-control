#!/bin/zsh

idsnormal=('1' '2' '3' '4' '5' '6' '7' '8' '9' '10' '11')

x=('0.0' '0.0' '-10.099443977348372' '-10.311535327934166' '-4.934521356078321' '4.100286640422141' '9.607840577591688' '10.231019606451554' '9.523578609607014' '4.287402835427699' '0.553431654141349')

y=('0.0' '0.0' '-12.771505856302518' '-20.448376109453854' '-32.576739723418264' '-58.8470814089588' '-80.7092905446765' '-89.41305616049716' '-92.73564798657252' '-100.13656472738407' '-102.75521470337662')

z=('0.0' '-25.0' '-25.0' '-25.0' '-24.0' '-24.0' '-24.0' '-24.0' '-24.0' '-24.0' '-24.0')

lat=('50.01828' '50.01828' '50.01818920190005' '50.018187294901686' '50.01823563582035' '50.01831686028014' '50.0183663727768' '50.01837197415769' '50.018365613479105' '50.018318537021834' '50.0182570339083')

lon=('21.98508' '21.98508' '21.984901798695162' '21.984794683045113' '21.98462545500784' '21.98425890226908' '21.983953856247744' '21.98383241170386' '21.983786051456498' '21.983682786969577' '21.983576485291866')

alt_amsl=('0.0' '25.0' '25.0' '25.0' '24.0' '25.0' '26.0' '26.0' '26.0' '25.0' '25.0')


PATH_SATELITES=./random_satelites.sh
PATH_ATTITUDE=./random_attitude.sh
REMAINING=100

command_velocity='ros2 topic pub /px_control/velocity drone_types/msg/PxControlVelocity "{vx: 2.0, vy: 2.0, vz: 0.0}" --once'
eval $command_velocity

for i in "${idsnormal[@]}"
do
   xcor=$x[$i]
   ycor=$y[$i]
   zcor=$z[$i]
   lat_global=$lat[$i]
   lon_global=$lon[$i]
   alt_global=$alt_amsl[$i]
   NUMBER_SATELITES=$("$PATH_SATELITES")
   QW=$("$PATH_ATTITUDE")
   QX=$("$PATH_ATTITUDE")
   QY=$("$PATH_ATTITUDE")
   QZ=$("$PATH_ATTITUDE")

   command_local='ros2 topic pub /px_control/local_pos drone_types/msg/PxControlLocalPos "{x: '$xcor', y: '$ycor', z: '$zcor'}" --once'
   eval $command_local

   command_global='ros2 topic pub /px_control/global_pos drone_types/msg/PxControlGlobalPos "{latitude: '$lat_global', longitude: '$lon_global', altitude: '$alt_global', num_sats: '$NUMBER_SATELITES'}" --once'
   eval $command_global

   command_attitude='ros2 topic pub /px_control/attitude drone_types/msg/PxControlAttitude "{q_w: '$QW', q_x: '$QX', q_y: '$QY', q_z: '$QZ'}" --once'
   eval $command_attitude

   command_battery='ros2 topic pub /px_control/battery drone_types/msg/PxControlBattery "{voltage: 12, current: 50, remaining: '$REMAINING'}" --once'
   eval $command_battery

   REMAINING=$(($REMAINING-1))

done

echo 'Normal flight before return_home command finished'

