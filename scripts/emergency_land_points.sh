#!/bin/zsh

idsemergency=('1' '2' '3' '4' '5' '6' '7' '8')

x=('4.2875441113595265' '9.523892425880923' '10.231356733683157' '9.608157170316023' '4.100421750725376' '10.879992237754337' '15.834915272026908' '21.13461031994939' '21.13453404884197')

y=('-100.13985522249065' '-92.73869528637252' '-89.4159942798911' '-80.71194265790271' '-58.84901512840088' '-54.3217735875995' '-51.7095817545007' '-50.88635172141674' '-50.8861685893708')

z=('-24.999214010062055' '-25.999320021304282' '-25.99936627515665' '-25.999483096551124' '-24.99972773377063' '-24.99975984927418' '-24.99977113460791' '-24.999762370608593' '-1.999762371588763')

lat=('50.018318537021834' '50.018365613479105' '50.01837197415769' '50.0183663727768' '50.01831686028014' '50.018377809698464' '50.01842235516344' '50.01847' '50.01847')

lon=('21.983682786969577' '21.983786051456498' '21.98383241170386' '21.983953856247744' '21.98425890226908' '21.98432206817462' '21.984358514464148' '21.98437' '21.98437')

alt_amsl=('25.0' '26.0' '26.0' '26.0' '25.0' '25.0' '25.0' '25.0' '2.0')

PATH_SATELITES=./random_satelites.sh
PATH_ATTITUDE=./random_attitude.sh
REMAINING=79

for i in "${idsemergency[@]}"
do
   xcor=$x[$i]
   ycor=$y[$i]
   zcor=$z[$i]
   lat_global=$lat[$i]
   lon_global=$lon[$i]
   alt_global=$alt_amsl[$i]
   NUMBER_SATELITES=$("$PATH_SATELITES")
   QW=$("$PATH_ATTITUDE")
   QX=$("$PATH_ATTITUDE")
   QY=$("$PATH_ATTITUDE")
   QZ=$("$PATH_ATTITUDE")

   command_local='ros2 topic pub /px_control/local_pos drone_types/msg/PxControlLocalPos "{x: '$xcor', y: '$ycor', z: '$zcor'}" --once'
   eval $command_local

   command_global='ros2 topic pub /px_control/global_pos drone_types/msg/PxControlGlobalPos "{latitude: '$lat_global', longitude: '$lon_global', altitude: '$alt_global', num_sats: '$NUMBER_SATELITES'}" --once'
   eval $command_global

   command_attitude='ros2 topic pub /px_control/attitude drone_types/msg/PxControlAttitude "{q_w: '$QW', q_x: '$QX', q_y: '$QY', q_z: '$QZ'}" --once'
   eval $command_attitude

   command_battery='ros2 topic pub /px_control/battery drone_types/msg/PxControlBattery "{voltage: 12, current: 50, remaining: '$REMAINING'}" --once'
   eval $command_battery

   REMAINING=$(($REMAINING-1))

done

sleep 1.0

command_velocity='ros2 topic pub /px_control/velocity drone_types/msg/PxControlVelocity "{vx: 0.0, vy: 0.0, vz: 0.0}" --once'
eval $command_velocity

sleep 1.0

ros2 topic pub /px_control/status drone_types/msg/PxControlStatus "{landed_state: 1}" --once

sleep 1.0

ros2 topic pub /sensors/button_press drone_types/msg/SensorsButtonPress "{shutdown: True}" --once

sleep 1.0

echo 'Emergency flight finished !!!!'
