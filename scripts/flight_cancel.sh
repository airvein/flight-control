#!/usr/bin/zsh

FLIGHT_CANCEL='{"command": "flight_cancel", "parameters": {}}'

mosquitto_pub --cafile ~/OpenSSL-temp/ca.crt -h airvein-hangar -p 8883 -t hangar --tls-version tlsv1.1 -m $FLIGHT_CANCEL
