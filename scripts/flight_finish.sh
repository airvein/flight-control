#!/usr/bin/zsh

FINISH='{"command": "flight_finish", "parameters": {}}'

mosquitto_pub --cafile ~/OpenSSL-temp/ca.crt -h airvein-hangar -p 8883 -t hangar --tls-version tlsv1.1 -m $FINISH
