#!/usr/bin/zsh

INIT='{"command": "drone_initializing", "parameters": {"route_id": "123", "flight_id": "123", "cargo": false}}'

mosquitto_pub --cafile ~/OpenSSL-temp/ca.crt -h airvein-hangar -p 8883 -t hangar --tls-version tlsv1.1 -m $INIT
