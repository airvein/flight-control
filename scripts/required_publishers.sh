#!/usr/bin/zsh

ros2 run flight_control ticker_run &

command_velocity='ros2 topic pub /px_control/velocity drone_types/msg/PxControlVelocity "{vx: 0.0, vy: 0.0, vz: 0.0}" --once'
eval $command_velocity

command_local='ros2 topic pub /px_control/local_pos drone_types/msg/PxControlLocalPos "{x: 0.0, y: 0.0, z: 0.0}" --once'
eval $command_local

command_global='ros2 topic pub /px_control/global_pos drone_types/msg/PxControlGlobalPos "{latitude: 50.01828, longitude: 21.98508, altitude: 0.0, num_sats: 10}" --once'
eval $command_global

command_attitude='ros2 topic pub /px_control/attitude drone_types/msg/PxControlAttitude "{q_w: 0.6, q_x: 0.7, q_y: 0.9, q_z: 0.7}" --once'
eval $command_attitude

command_battery='ros2 topic pub /px_control/battery drone_types/msg/PxControlBattery "{voltage: 12, current: 50, remaining: 99}" --once'
eval $command_battery

while :
do
	ros2 topic pub /sensors/presence drone_types/msg/SensorsPresence '{cargo: False, battery: True}' --once
	ros2 topic pub /px_control/status drone_types/msg/PxControlStatus '{arm_state: 1}' --once
done
