#!/usr/bin/zsh

CARGO_PREPARE='{"command": "cargo_prepare", "parameters": {}}'

mosquitto_pub --cafile ~/OpenSSL-temp/ca.crt -h airvein-hangar -p 8883 -t hangar --tls-version tlsv1.1 -m $CARGO_PREPARE
