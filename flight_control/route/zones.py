"""zones.py

Zones class storing zones data

Copyright 2019 Cervi Robotics
"""
from collections import namedtuple


class Zones(namedtuple('Zones', ["home_maneuver", "home_approach", "landing_approach", "landing_maneuver"])):
    def __new__(cls, *args, **kwargs):
        if kwargs == {}:
            raise TypeError('Zones MUST be called with arguments')
        if not isinstance(kwargs.get("home_maneuver"), int):
            raise TypeError('home_maneuver of point MUST be integer')
        if not isinstance(kwargs.get("home_approach"), int):
            raise TypeError('home_approach of point MUST be integer')
        if not isinstance(kwargs.get("landing_approach"), int):
            raise TypeError('landing_approach of point MUST be integer')
        if not isinstance(kwargs.get("landing_maneuver"), int):
            raise TypeError('landing_maneuver of point MUST be integer')
        return super(Zones, cls).__new__(cls, *args, **kwargs)
