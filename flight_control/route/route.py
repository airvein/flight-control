"""route.py

Route class storing data of drone track

Copyright 2019 Cervi Robotics
"""
import json
from typing import Text, Iterable, Optional

from flight_control.route.emergency_route import EmergencyRoute
from flight_control.route.point import Point
from flight_control.route.zones import Zones


class RouteError(Exception):
    pass


class ParseError(RouteError):
    pass


class Route:
    def __init__(self):
        self.route_id: Optional[Text] = None
        self.start_point: Optional[Point] = None
        self.end_point: Optional[Point] = None
        self.route_points: Optional[Iterable[Point]] = None
        self.zones: Optional[Zones] = None
        self.route_exit_points: Optional[Iterable[Text]] = None
        self.emergency_routes: Iterable[EmergencyRoute] = []
        self.user_points: Optional[Iterable[Text]] = None
        self.start_yaw: Optional[float] = None
        self.end_yaw: Optional[float] = None

    @staticmethod
    def from_json(route_json: Text):
        try:
            route = Route()
            data = json.loads(route_json)

            route._parse_route_id(data)
            route._parse_start_point(data)
            route._parse_end_point(data)
            route._parse_route_points(data)
            route._parse_zones(data)
            route._parse_route_exit_points(data)
            route._parse_emergency_routes(data)
            route._parse_user_points(data)

            return route

        except json.JSONDecodeError as error:
            raise ParseError('JSONDecodeError!!! Error while parsing JSON. '
                             f'Error: {error}')

    def _parse_route_id(self, data):
        self.route_id = data['route_id']
        return self.route_id

    def _parse_start_point(self, data):
        try:
            self.start_point = Point(
                id=data['start_point'][0],
                latitude=data['start_point'][1],
                longitude=data['start_point'][2],
                altitude=data['start_point'][3]
            )
            self.start_yaw = data['start_point'][4]

            return self.start_point

        except IndexError as error:
            raise ParseError('Incorrect format of end point. '
                             f'There is missing field. Error: {error}')

    def _parse_end_point(self, data):
        try:
            self.end_point = Point(
                id=data['end_point'][0],
                latitude=data['end_point'][1],
                longitude=data['end_point'][2],
                altitude=data['end_point'][3],

            )
            self.end_yaw = data['end_point'][4]

            return self.end_point

        except IndexError as error:
            raise ParseError('Incorrect format of end point. '
                             f'There is missing field. Error: {error}')

    def _parse_route_points(self, data):
        try:
            self.route_points = self._get_route_points(data['route_points'])
            return self.route_points

        except IndexError as error:
            raise ParseError('Incorrect format of route points. '
                             f'There is missing field. Error: {error}')

    @staticmethod
    def _get_route_points(route_points: list):
        parsed_points = []
        for point in route_points:
            parsed_point: Point = Point(
                id=point[0],
                latitude=point[1],
                longitude=point[2],
                altitude=point[3]
            )
            parsed_points.append(parsed_point)

        return parsed_points

    def _parse_zones(self, data):
        zones = data['zones']
        self.zones = Zones(
            home_maneuver=zones['home_maneuver'],
            home_approach=zones['home_approach'],
            landing_approach=zones['landing_approach'],
            landing_maneuver=zones['landing_maneuver']
        )
        return self.zones

    def _parse_route_exit_points(self, data):
        self.route_exit_points = data['route_exit_points']
        return self.route_exit_points

    def _get_emergency_routes(self, data):
        emergency_routes = []

        for route_id, route_points in data["emergency_routes_points"].items():
            emergency_route = EmergencyRoute()
            emergency_route.route_id = route_id
            emergency_route.route_points = self._get_route_points(route_points)
            emergency_routes.append(emergency_route)

        return emergency_routes

    def _parse_emergency_routes(self, data):
        try:
            self.emergency_routes = self._get_emergency_routes(data)
            return self.emergency_routes

        except IndexError as error:
            raise ParseError('Incorrect format of emergency routes points. '
                             f'There is missing field. Error: {error}')

    def _parse_user_points(self, data):
        self.user_points = data['user_points']
        return self.user_points
