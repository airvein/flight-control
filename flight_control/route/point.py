"""point.py

Point class storing data points of drone track

Copyright 2019 Cervi Robotics
"""
from collections import namedtuple

PointTuple = namedtuple(
    'PointTuple', ["id", "latitude", "longitude", "altitude"]
)


class Point(PointTuple):
    def __new__(cls, *args, **kwargs):
        cls._check_parameters(kwargs)
        parsed_kwargs = {
            "id": int(kwargs.get("id")),
            "latitude": float(kwargs.get("latitude")),
            "longitude": float(kwargs.get("longitude")),
            "altitude": float(kwargs.get("altitude")),
        }
        return super(Point, cls).__new__(cls, *args, **parsed_kwargs)

    def list(self) -> list:
        return [self.id, self.latitude, self.longitude, self.altitude]

    @staticmethod
    def _check_parameters(parameters: dict):
        for parameter_name, parameter_value in parameters.items():
            if not isinstance(parameter_value, (int, float)):
                raise TypeError(
                    f'{parameter_name} of point MUST be integer or float'
                )
