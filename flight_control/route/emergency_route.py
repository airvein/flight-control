"""emergency_route.py

EmergencyRoute class storing data of drone emergency track

Copyright 2019 Cervi Robotics
"""
from typing import Text, Iterable, Optional

from flight_control.route.point import Point


class EmergencyRoute:
    def __init__(self):
        self.route_id: Optional[Text] = None
        self.route_points: Optional[Iterable[Point]] = None
