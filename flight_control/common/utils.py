import time
import typing as tp

from flight_control.route.point import Point


def get_timestamp() -> int:
    return int(time.time() * 1000)  # mS - miliSeconds


def fix_points_id(points: tp.List[Point], start_id: int = 0) -> tp.List[Point]:
    output = [Point(id=idx, latitude=p.latitude,
                    longitude=p.longitude, altitude=p.altitude)
              for idx, p in enumerate(points, start_id)]
    return output


def fix_point_altitude(point: Point, value: float) -> Point:
    fixed_altitude = point.altitude - value
    p = Point(id=point.id,
              latitude=point.latitude,
              longitude=point.longitude,
              altitude=fixed_altitude)
    return p
