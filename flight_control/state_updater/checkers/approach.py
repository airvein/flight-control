"""approach.py

Approach checker class which checks if UAV come into approach zone

Copyright 2019 Cervi Robotics
"""

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import AbstractChecker


class Approach(AbstractChecker):
    def check(self) -> bool:
        in_approach = self._approach_zone_achieved

        if in_approach:
            self.state_machine_controller.in_approach()

        return in_approach

    @property
    def _approach_zone_achieved(self) -> bool:
        machine_state = self.state_machine_controller.state

        if machine_state == StateNames.CRUISE:
            result = (self.telemetry.previous_point.id
                      >= self.telemetry.route.zones.landing_approach)

        elif machine_state == StateNames.CRUISE_RETURN_HOME:
            result = (self.telemetry.previous_point.id
                      <= self.telemetry.route.zones.home_approach)
        else:
            result = False

        return result
