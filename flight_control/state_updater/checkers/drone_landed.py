"""drone_landed.py

DroneLanded checker class which checks if UAV landed - touch
ground in landing place.

Copyright 2019 Cervi Robotics
"""

from drone_types.msg import PxControlStatus
from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import AbstractChecker


class DroneLanded(AbstractChecker):
    ALLOWED_STATES = (
        StateNames.LANDING,
        StateNames.EMERGENCY_LANDING,
        StateNames.LN_LANDING,
    )

    def check(self) -> bool:
        condition = (self.state_machine_controller.state in self.ALLOWED_STATES
                     and self._drone_landed)

        if condition:
            self.state_machine_controller.landed()

        return condition

    @property
    def _drone_landed(self) -> bool:
        result = (self.telemetry.px_control_status.flight_state
                  == PxControlStatus.FLIGHT_STATE_ON_GROUND)

        return result
