__all__ = ['approach', 'battery_land', 'battery_shutdown', 'cancel_flight_data',
           'cargo', 'cruise', 'drone_landed', 'exit_waiting',
           'maneuver', 'manual_shutdown', 'route_done', 'takeoff',
           ]
