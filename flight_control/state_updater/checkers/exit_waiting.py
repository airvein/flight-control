"""exit_waiting.py

Checker class which checks if drone is not in waiting state for too long

Copyright 2020 Cervi Robotics
"""
from typing import Optional

from flight_control.state_updater.checkers.abstract_checker import \
    AbstractChecker
from flight_control.common.utils import get_timestamp
from flight_control.commander import StateDataNames, StateNames
from flight_control.settings import MAX_WAITING_TIME


class ExitWaiting(AbstractChecker):
    LAST_TIMESTAMP: Optional[int] = None  # 13-digit timestamp
    TIME_IN_WAITING_STATE: int = 0

    def check(self) -> bool:
        waiting = self.state_machine_controller.state_data[
            StateDataNames.FLAGS][StateDataNames.Flag.WAITING]

        correct_state = (self.state_machine_controller.state
                         != StateNames.APPROACH_CRUISE_WAIT)

        if waiting and correct_state:
            if self.LAST_TIMESTAMP is None:
                self.LAST_TIMESTAMP = get_timestamp()

            current_timestamp = get_timestamp()
            delta = current_timestamp - self.LAST_TIMESTAMP
            self.TIME_IN_WAITING_STATE += delta
            self.LAST_TIMESTAMP = current_timestamp

            should_exit = self.TIME_IN_WAITING_STATE >= MAX_WAITING_TIME
            if should_exit:
                self.state_machine_controller.state_data[StateDataNames.FLAGS][
                    StateDataNames.Flag.WAITING_TIME_EXCEEDED] = True

                self.state_machine_controller.state_data[StateDataNames.FLAGS][
                    StateDataNames.Flag.WAITING] = False

            result = should_exit
        else:
            self.LAST_TIMESTAMP = None
            result = False

        return result

