"""manual_shutdown.py

ManualShutdown checker class which checks if UAV is turn off by using button

Copyright 2019 Cervi Robotics
"""

from periphery import GPIO

from flight_control.commander import StateNames, Telemetry
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.state_updater.checkers.abstract_checker import \
    AbstractChecker
from flight_control.settings import Power


class ManualShutdown(AbstractChecker):
    ALLOWED_STATES = (
        StateNames.EMERGENCY_LANDED,
        StateNames.LN_LANDED,
        StateNames.KILLED
    )

    def __init__(self, telemetry: Telemetry,
                 state_machine_controller: StateMachineController):
        super().__init__(telemetry, state_machine_controller)
        if Power.USE_MANUAL_SHUTDOWN:
            self._shutdown_button = GPIO(Power.MANUAL_SHUTDOWN_PIN, 'in')

    def check(self) -> bool:
        condition = (self.state_machine_controller.state in self.ALLOWED_STATES
                     and self._button_pressed)
        if condition:
            self.state_machine_controller.man_shutdown()

        return condition

    @property
    def _button_pressed(self):
        if Power.USE_MANUAL_SHUTDOWN:
            return self._shutdown_button.read()
        else:
            return self.telemetry.sensors.button_press.shutdown
