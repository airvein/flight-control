"""route_done.py

RouteDone checker class which checks if UAV did route - is exactly
above landing point

Copyright 2019 Cervi Robotics
"""

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import AbstractChecker


class RouteDone(AbstractChecker):
    ALLOWED_STATES = (
        StateNames.CRUISE_LAND,
        StateNames.EMERGENCY_CRUISE,
    )

    def check(self) -> bool:
        condition = (self.state_machine_controller.state in self.ALLOWED_STATES
                     and self.telemetry.route_done)

        if condition:
            self.state_machine_controller.route_done()

        return condition
