"""cruise.py

Cruise checker class which checks if UAV left maneuver zone after takeoff

Copyright 2019 Cervi Robotics
"""

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import AbstractChecker


class Cruise(AbstractChecker):
    def check(self) -> bool:
        condition = self._correct_machine_state and self._achieved_maneuver_zone

        if condition:
            self.state_machine_controller.cruising()

        return condition

    @property
    def _correct_machine_state(self) -> bool:
        result = (self.state_machine_controller.state
                  == StateNames.TAKEOFF_MONITOR)

        return result

    @property
    def _achieved_maneuver_zone(self) -> bool:
        result = (self.telemetry.previous_point.id
                  >= self.telemetry.route.zones.home_maneuver)

        return result
