"""battery_shutdown.py

BatteryShutdown checker class which checks if UAV is turn off when
battery is low

Copyright 2020 Cervi Robotics
"""

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import \
    AbstractChecker
from flight_control.settings import LowBattery


class BatteryShutdown(AbstractChecker):
    ALLOWED_STATES = (
        StateNames.EMERGENCY_LANDED,
        StateNames.LN_LANDED,
        StateNames.KILLED
    )

    def check(self) -> bool:
        condition = (self.state_machine_controller.state in self.ALLOWED_STATES
                     and self.telemetry.battery.remaining < LowBattery.SHUTDOWN)

        if condition:
            self.state_machine_controller.battery_shutdown()

        return condition
