"""abstract_checker.py

The AbstractChecker class which is base class for other checkers

Copyright 2019 Cervi Robotics
"""

from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.telemetry import Telemetry


class AbstractChecker:
    def __init__(self, telemetry: Telemetry,
                 state_machine_controller: StateMachineController):
        self.telemetry = telemetry
        self.state_machine_controller = state_machine_controller

    def check(self) -> bool:
        raise NotImplementedError('Not implemented')
