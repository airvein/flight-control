"""maneuver.py

Maneuver checker class which checks if UAV come into maneuver zone
while landing

Copyright 2019 Cervi Robotics
"""

from flight_control.commander import StateDataNames, StateNames
from flight_control.state_updater.checkers.abstract_checker import AbstractChecker


class Maneuver(AbstractChecker):
    ALLOWED_STATES = (
        StateNames.APPROACH,
        StateNames.APPROACH_CRUISE,
    )

    def check(self) -> bool:
        condition = (self.state_machine_controller.state in self.ALLOWED_STATES
                     and self._achieved_maneuver_zone)

        if condition:
            self.state_machine_controller.in_maneuver()

        return condition

    @property
    def _achieved_maneuver_zone(self) -> bool:
        rth = self.state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.RTH]

        if not rth:
            result = (self.telemetry.previous_point.id
                      >= self.telemetry.route.zones.landing_maneuver)
        else:
            result = (self.telemetry.previous_point.id
                      <= self.telemetry.route.zones.home_maneuver)

        return result
