"""battery_land.py

BatteryLand checker class which checks if UAV has to abort mission and
force land because of low battery level

Copyright 2020 Cervi Robotics
"""

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import \
    AbstractChecker
from flight_control.settings import LowBattery


class BatteryLand(AbstractChecker):
    ALLOWED_STATES = (
        StateNames.CRUISE,
        StateNames.CRUISE_RETURN_HOME,
        StateNames.EMERGENCY_CRUISE,
        StateNames.APPROACH_CRUISE,
        StateNames.APPROACH_CRUISE_WAIT,
        StateNames.APPROACH,
    )

    def check(self) -> bool:
        condition = (self.state_machine_controller.state in self.ALLOWED_STATES
                     and self.telemetry.battery.remaining <= LowBattery.LAND)

        if condition:
            self.state_machine_controller.land_now()

        return condition

