"""takeoff.py

Takeoff checker class which checks if UAV start flying - is above ground

Copyright 2019 Cervi Robotics
"""

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import AbstractChecker


class Takeoff(AbstractChecker):

    def check(self) -> bool:
        condition = (self._correct_machine_state
                     and self._drone_in_takeoff())

        if condition:
            self.state_machine_controller.takeoff()

        return condition

    @property
    def _correct_machine_state(self):
        result = (self.state_machine_controller.state
                  == StateNames.TAKEOFF_START)

        return result

    def _drone_in_takeoff(self, diff=0.5) -> bool:
        z_diff = abs(self.telemetry.route.start_point.altitude
                     - self.telemetry.global_pos.altitude)
        return z_diff > diff
