"""cargo.py

Cargo checker class which checks if cargo is present. If it is, then it
triggers machine state transition.

Copyright 2019 Cervi Robotics
"""

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import AbstractChecker


class Cargo(AbstractChecker):
    def check(self) -> bool:
        condition = (self._machine_in_correct_state
                     and self.telemetry.sensors.presence.cargo)

        if condition:
            self.state_machine_controller.cargo_detected()

        return condition

    @property
    def _machine_in_correct_state(self) -> bool:
        result = self.state_machine_controller.state == StateNames.WAIT_CARGO

        return result
