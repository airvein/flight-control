"""cancel_data_uploaded.py

Checker class which checks if drone finished sending data from cancel_flight

Copyright 2020 Cervi Robotics
"""

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.abstract_checker import \
    AbstractChecker


class CancelFlightData(AbstractChecker):
    def check(self) -> bool:
        # TODO: When logs will actually be sent to cloud this has to be
        #  put into condition

        is_in_proper_state = (self.state_machine_controller.state
                              == StateNames.CANCEL_UPLOAD_DATA)

        if is_in_proper_state:
            self.state_machine_controller.done_flight_cancel()

        return is_in_proper_state
