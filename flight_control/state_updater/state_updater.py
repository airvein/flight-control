"""state_updater.py

StateUpdater class which handle with internal changing of states depending
on Drone position on track.

Copyright 2019 Cervi Robotics
"""
import logging

from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.telemetry import Telemetry
from flight_control.state_updater.checkers import *

logger = logging.getLogger(__name__)


class StateUpdater:
    def __init__(self, telemetry: Telemetry, state_machine_controller: StateMachineController):
        logger.debug("Initializing StateUpdater")

        self._checkers = {
            "approach": approach.Approach(telemetry, state_machine_controller),
            "cargo": cargo.Cargo(telemetry, state_machine_controller),
            "cruise": cruise.Cruise(telemetry, state_machine_controller),
            "drone_landed": drone_landed.DroneLanded(telemetry, state_machine_controller),
            "exit_waiting": exit_waiting.ExitWaiting(telemetry, state_machine_controller),
            "maneuver": maneuver.Maneuver(telemetry, state_machine_controller),
            "manual_shutdown": manual_shutdown.ManualShutdown(telemetry, state_machine_controller),
            "route_done": route_done.RouteDone(telemetry, state_machine_controller),
            "takeoff": takeoff.Takeoff(telemetry, state_machine_controller),
            "battery_shutdown": battery_shutdown.BatteryShutdown(telemetry, state_machine_controller),
            "cancel_flight_data": cancel_flight_data.CancelFlightData(telemetry, state_machine_controller),
            "battery_land": battery_land.BatteryLand(telemetry, state_machine_controller)
        }

    def update(self):
        for checker in self._checkers.values():
            checker.check()
