"""state.py

State class storing data of Navigator state

Copyright 2019 Cervi Robotics
"""
from enum import Enum


class StateError(Exception):
    pass


class EmergencyCruiseStateError(StateError):
    pass


class StateSetError(StateError):
    pass


class NavigatorState(Enum):
    CRUISE = 0
    RETURN_HOME = -1
    EMERGENCY_CRUISE = 1


class EmergencySubState(Enum):
    EMERGENCY_CRUISE = 0
    EMERGENCY_RETURN_HOME = -1
    EMERGENCY_ROUTE = None


class State:

    def __init__(self, state=NavigatorState.CRUISE, value=0):
        self._state = state
        self._value = value

    def __repr__(self):
        return f'<{__class__.__name__}.{self._state.name}: {self._value}>'

    @property
    def name(self):
        return self._state.name

    @property
    def state(self):
        return self._state

    @property
    def value(self):
        return self._value

    def set_state(self, *args):
        try:
            state = args[0]
        except IndexError as error:
            raise StateSetError(
                f'To update State state argument must be declared as first positional argument. Error: {error}')

        self._state = state
        self._value = state.value

        return self._state

    def _set_emergency_cruise_value(self, *args):
        try:
            value = args[1]
        except IndexError as error:
            raise EmergencyCruiseStateError(
                f'In Emergency Route State value as second argument must be defined. Error: {error}')

        if not isinstance(value, int):
            raise EmergencyCruiseStateError(
                'In Emergency Route State value as second argument must be integer. Error: tuple index out of range')

        if not value >= 1:
            raise EmergencyCruiseStateError(
                'In Emergency Route State value as second argument must be in range [1...n].'
                ' Error: tuple index out of range')

        self._value = value

        return self._value

    def _update(self, *args):
        self._state = self.set_state(*args)

        if self._state.name == "EMERGENCY_ROUTE":
            self._set_emergency_cruise_value(*args)
        else:
            self._value = self._state.value
