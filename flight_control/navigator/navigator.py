"""navigator.py

Navigator class which manage route points for missions

Copyright 2019 Cervi Robotics
"""
import typing as tp
import logging

from flight_control.commander.telemetry import Telemetry
from flight_control.navigator.mission_manager import MissionManager
from flight_control.navigator.progress import Progress, AbsoluteDistanceError
from flight_control.navigator.state import EmergencySubState, NavigatorState,\
    State
from flight_control.command_interpreter import MissionTypes
from flight_control.route.point import Point

logger = logging.getLogger(__name__)


class NavigatorError(Exception):
    pass


class Navigator:
    def __init__(self, telemetry: Telemetry):
        logger.debug("Initializing Navigator")

        self.telemetry = telemetry

        self.state: State = State()
        self.progress = Progress(self.telemetry, self.state)
        self.mission_manager = MissionManager(self.telemetry)

        self._waiting: bool = False

        self.mission_state_map = {
            MissionTypes.NORMAL: NavigatorState.CRUISE,
            MissionTypes.RTH: NavigatorState.RETURN_HOME,
            MissionTypes.EMERGENCY: NavigatorState.EMERGENCY_CRUISE
        }

        self._prepare_points_handler = {
            MissionTypes.NORMAL: self._prepare_points_normal_mission,
            MissionTypes.RTH: self._prepare_points_return_home_mission,
            MissionTypes.EMERGENCY: self._prepare_points_emergency_mission,
        }

    @property
    def waiting(self) -> bool:
        return self._waiting

    def wait(self):
        self._waiting = True

    def continue_flight(self):
        self._waiting = False

    def update(self):
        try:
            self.progress.calculate_progress()
        except AbsoluteDistanceError as e:
            logger.warning(f"AbsoluteDistanceError: {e}")
            self._update_telemetry_points()  # force next route point
        self._evaluate_target_point()

    def prepare_mission(self, mission: str) -> dict:
        logger.info(f"Preparing mission data. Mission: {mission}")

        mission_data = self.mission_manager.prepare_mission_data(mission)
        self._update_mission_points(mission)
        self.state.set_state(self.mission_state_map[mission])

        return mission_data

    def _update_mission_points(self, mission: str):
        previous_point, next_point = self._prepare_points_handler[mission]()
        self.telemetry.previous_point = previous_point
        self.telemetry.next_point = next_point

    def _prepare_points_normal_mission(self) -> tp.Tuple[Point, Point]:
        new_previous_point = Point(
            id=0,
            latitude=self.telemetry.route.start_point.latitude,
            longitude=self.telemetry.route.start_point.longitude,
            altitude=0,  # relative altitude for start point is always zero
        )

        new_next_point = next(self.telemetry.actual_route_points)

        return new_previous_point, new_next_point

    def _prepare_points_return_home_mission(self) -> tp.Tuple[Point, Point]:
        new_previous_point = self.telemetry.next_point
        new_next_point = next(self.telemetry.actual_route_points)

        return new_previous_point, new_next_point

    def _prepare_points_emergency_mission(self) -> tp.Tuple[Point, Point]:
        next_point: Point = self.telemetry.next_point
        previous_point: Point = self.telemetry.previous_point
        emergency_route_id: int = self.telemetry.emergency_route_id

        drone_ahead_emergency_point = previous_point.id >= emergency_route_id
        moving_forward = next_point.id > previous_point.id

        replace_point = (
                drone_ahead_emergency_point and moving_forward
                or (not drone_ahead_emergency_point and not moving_forward)
        )

        new_previous_point = next_point if replace_point else previous_point
        new_next_point = next(self.telemetry.actual_route_points)

        return new_previous_point, new_next_point

    def _update_telemetry_points(self):
        self.telemetry.previous_point = self.telemetry.next_point

        self.telemetry.next_point = next(self.telemetry.actual_route_points)

    def _evaluate_target_point(self):
        if (self.progress.target_point_achieved
                and not self.telemetry.route_done):
            next_point = self._get_next_route_point()

            self.telemetry.previous_point = self.telemetry.next_point
            self.telemetry.next_point = next_point

            self.telemetry.route_done = self._is_route_done()

    def _get_next_route_point(self) -> Point:
        route_end_point = self._get_landing_point()

        if self._next_target_point_id == route_end_point.id:
            next_point = route_end_point
        else:
            next_point = next(self.telemetry.actual_route_points)

        return next_point

    def _get_landing_point(self) -> Point:
        if self.state.state == NavigatorState.CRUISE:
            fixed_altitude = self.telemetry.route.end_point.altitude \
                             - self.telemetry.route.start_point.altitude

            end_point = Point(id=self.telemetry.route.end_point.id,
                              latitude=self.telemetry.route.end_point.latitude,
                              longitude=self.telemetry.route.end_point.longitude,
                              altitude=fixed_altitude)

        elif self.state.state == NavigatorState.RETURN_HOME:
            end_point = Point(id=self.telemetry.route.start_point.id,
                              latitude=self.telemetry.route.start_point.latitude,
                              longitude=self.telemetry.route.start_point.longitude,
                              altitude=0)  # relative alt. of start point == 0

        else:
            end_point = self.telemetry.emergency_end_point

        return end_point

    def _is_route_done(self) -> bool:
        if self.state.state == NavigatorState.CRUISE:
            finish_route_point = self.telemetry.route.end_point

        elif self.state.state == NavigatorState.EMERGENCY_CRUISE:
            finish_route_point = self.telemetry.emergency_end_point

        else:
            finish_route_point = self.telemetry.route.start_point

        output = self.telemetry.next_point.id == finish_route_point.id

        return output

    @property
    def _next_target_point_id(self) -> int:
        if self.telemetry.next_point.id > self.telemetry.previous_point.id:
            next_point_id = self.telemetry.next_point.id + 1
        else:
            next_point_id = self.telemetry.next_point.id - 1

        return next_point_id
