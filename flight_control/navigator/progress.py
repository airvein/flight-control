"""progress.py

Progress class calculating progress of waypoint while drone is flight

Copyright 2019 Cervi Robotics
"""
import math
from typing import Union, Optional

from drone_types import msg

from flight_control.commander.telemetry import Telemetry
from flight_control.navigator.state import State
from flight_control.route.point import Point


class ProgressError(Exception):
    pass


class AbsoluteDistanceError(ProgressError):
    pass


class ProgressValueError(Exception):
    pass


def _validate_progress(func):
    def wrapper(*args, **kwargs):
        output: float = func(*args, **kwargs)

        if output >= 1.0:
            output = 1.0
        elif output <= 0.0:
            output = 0.0
        return output
    return wrapper


class Progress:
    def __init__(self, telemetry: Telemetry, state: State):
        self.telemetry = telemetry
        self.state = state

        self.next_point_id: int = self.telemetry.next_point.id
        self.next_point_route_id: int = self.state.value
        self.progress: float = 0.0

        self.absolute_distance: float = 0.0
        self.distance_drone_from_previous_point: float = 0.0
        self.distance_drone_to_next_point: float = 0.0
        self.target_point_achieved: bool = False

    def calculate_progress(self) -> float:
        self._update_points_distance()
        progress = self._get_progress(self.distance_drone_from_previous_point,
                                      self.absolute_distance)

        self._update(progress)
        return progress

    def is_target_position_achieved(self, buffer: float = 0.4) -> bool:
        dist_from_previous_point = self.distance_drone_from_previous_point
        dist_to_next_point = self.distance_drone_to_next_point
        dist_between_points = self.absolute_distance

        if (
                dist_from_previous_point < dist_to_next_point  # drone behind points
                and dist_to_next_point > dist_between_points):
            return False
        elif (
                dist_from_previous_point > dist_to_next_point  # drone ahead points
                and dist_from_previous_point > dist_between_points):
            return True
        else:
            output = self.distance_drone_to_next_point <= buffer
            return output

    def _update_points_distance(self):
        self.absolute_distance = self._get_distance(
            self.telemetry.previous_point,
            self.telemetry.next_point)

        self.distance_drone_from_previous_point = self._get_distance(
            self.telemetry.previous_point, self.telemetry.global_pos,
            fix_relative_height=self.telemetry.route.start_point.altitude
        )
        self.distance_drone_to_next_point = self._get_distance(
            self.telemetry.next_point, self.telemetry.global_pos,
            fix_relative_height=self.telemetry.route.start_point.altitude
        )

    @_validate_progress
    def _get_progress(self, distance_reached: float, absolute_distance: float
                      ) -> float:
        if absolute_distance != 0:
            progress = distance_reached / absolute_distance
        else:
            raise AbsoluteDistanceError(
                "Absolute distance between two points is 0")

        return progress

    @staticmethod
    def _get_distance(pointA: Union[Point, msg.PxControlGlobalPos],
                      pointB: Union[Point, msg.PxControlGlobalPos],
                      fix_relative_height: Optional[float] = None) -> float:
        """Calculate total distance, including height difference between
        two points.

        Unit: metres
        """

        alt_A = pointA.altitude
        alt_B = pointB.altitude
        if fix_relative_height is not None:
            alt_B -= fix_relative_height
        dAlt = alt_B - alt_A

        distance_haversine = Progress._calculate_distance_haversine(pointA,
                                                                    pointB)

        value = math.sqrt(distance_haversine ** 2 + dAlt ** 2)

        return value

    @staticmethod
    def _calculate_distance_haversine(
            pointA: Union[Point, msg.PxControlGlobalPos],
            pointB: Union[Point, msg.PxControlGlobalPos]
            ) -> float:
        """Calculate great-circle distance between two points using
        Haversine formula.

        Unit: metres
        """

        lat_A = math.radians(pointA.latitude)
        lat_B = math.radians(pointB.latitude)
        dLat = abs(lat_B - lat_A)

        lon_A = math.radians(pointA.longitude)
        lon_B = math.radians(pointB.longitude)
        dLon = abs(lon_B - lon_A)

        a = (math.sin(dLat/2)**2
             + math.cos(lat_A) * math.cos(lat_B) * math.sin(dLon/2)**2)
        c = 2 * math.asin(math.sqrt(a))

        R = 6371e3  # earth radius, metres
        distance_haversine = R * c

        return distance_haversine

    def _update(self, progress: float):
        self.progress = progress
        self.target_point_achieved = self.is_target_position_achieved()
        self.next_point_id = self.telemetry.next_point.id
        self.next_point_route_id = self.state.value
