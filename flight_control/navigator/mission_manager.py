import logging

from flight_control.command_interpreter import MissionTypes
from flight_control.commander import Telemetry
from flight_control.navigator.mission_preparation.normal_mission import \
    NormalMission
from flight_control.navigator.mission_preparation.rth_mission import RthMission
from flight_control.navigator.mission_preparation.emergency_mission import \
    EmergencyMission
from flight_control.route.point import Point

logger = logging.getLogger(__name__)


class MissionManager:
    def __init__(self, telemetry: Telemetry):
        logger.debug("Initializing MissionManager")
        self._telemetry = telemetry
        self._normal_mission = NormalMission(telemetry)
        self._rth_mission = RthMission(telemetry)
        self._emergency_mission = EmergencyMission(telemetry)

        self._mission_map = {MissionTypes.NORMAL: self._normal_mission,
                             MissionTypes.RTH: self._rth_mission,
                             MissionTypes.EMERGENCY: self._emergency_mission}

    def prepare_mission_data(self, mission: str) -> dict:
        logger.debug(f"Preparing mission data for mission: {mission}")
        mission_data = self._mission_map[mission].get_mission_data()

        if mission == MissionTypes.EMERGENCY:
            landing_point = mission_data['route_points'].pop()
            emergency_end_point = Point(id=landing_point[0],
                                        latitude=landing_point[1],
                                        longitude=landing_point[2],
                                        altitude=landing_point[3])
            self._telemetry.emergency_end_point = emergency_end_point
        logger.debug(f"Mission data:\n{mission_data}")
        return mission_data
