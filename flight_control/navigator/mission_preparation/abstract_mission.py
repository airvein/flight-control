"""abstract_mission.py

Abstract class that is used for mission preparation from route points

Copyright 2020 Cervi Robotics
"""

from abc import ABC, abstractmethod
import typing as tp

from flight_control.commander import Telemetry
from flight_control.route.point import Point


class AbstractMission(ABC):
    def __init__(self, telemetry: Telemetry):
        self._telemetry = telemetry

    def get_mission_data(self) -> dict:
        mission_data: tp.Dict[str, tp.Union[list, dict]] = {"route_points": [],
                                                            "zones": {}}

        route_points = self._get_mission_route_points()
        self._telemetry.actual_route_points = (p for p in route_points)

        mission_data["route_points"] = [point.list() for point in route_points]
        mission_data["zones"] = self._get_mission_zones()
        mission_data["landing_yaw"] = self._get_landing_yaw()
        return mission_data

    @abstractmethod
    def _get_mission_route_points(self) -> tp.List[Point]:
        raise NotImplementedError

    @abstractmethod
    def _get_mission_zones(self) -> dict:
        raise NotImplementedError

    @abstractmethod
    def _get_landing_yaw(self) -> float:
        raise NotImplementedError
