import typing as tp

from flight_control.commander import Telemetry
from flight_control.navigator.mission_preparation.abstract_mission import \
    AbstractMission
from flight_control.route.point import Point


class NormalMission(AbstractMission):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def _get_mission_route_points(self) -> tp.List[Point]:
        return self._telemetry.route.route_points[1:-1]

    def _get_mission_zones(self) -> dict:
        zones = self._telemetry.route.zones
        data = {"home_maneuver": zones.home_maneuver,
                "home_approach": zones.home_approach,
                "landing_approach": zones.landing_approach,
                "landing_maneuver": zones.landing_maneuver}
        return data

    def _get_landing_yaw(self) -> float:
        return self._telemetry.route.end_yaw
