import typing as tp

from flight_control.commander import Telemetry
from flight_control.common.utils import fix_points_id
from flight_control.navigator.mission_preparation.abstract_mission import \
    AbstractMission
from flight_control.route.point import Point


class EmergencyMission(AbstractMission):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def _get_mission_route_points(self) -> tp.List[Point]:
        route_points = self._get_normal_route_points_to_emergency_point()

        emergency_point_id = self._telemetry.emergency_route_id
        emergency_route_points = self._get_emergency_route(emergency_point_id)

        route_points.extend(emergency_route_points)

        route_points = fix_points_id(route_points,
                                     self._telemetry.next_point.id + 1)

        return route_points

    def _get_mission_zones(self):
        return {}

    def _get_normal_route_points_to_emergency_point(self) -> tp.List[Point]:
        previous_point_id = self._telemetry.previous_point.id
        next_point_id = self._telemetry.next_point.id
        emergency_point_id = self._telemetry.emergency_route_id

        moving_forward = next_point_id > previous_point_id
        ahead_emergency_point = previous_point_id >= emergency_point_id

        all_route_points = self._telemetry.route.route_points[1:-1]

        if ahead_emergency_point:
            start_id = next_point_id - 1 if moving_forward else next_point_id
            route_points = [point for point in reversed(all_route_points)
                            if start_id >= point.id > emergency_point_id]
        else:
            start_id = next_point_id if moving_forward else next_point_id + 1
            route_points = [point for point in all_route_points
                            if start_id <= point.id < emergency_point_id]

        return route_points

    def _get_emergency_route(self, emergency_route_id) -> list:
        emergency_route = []
        for route in self._telemetry.route.emergency_routes:
            if str(route.route_id) == str(emergency_route_id):
                emergency_route = route.route_points
        return emergency_route

    def _get_landing_yaw(self) -> float:
        return 0.0
