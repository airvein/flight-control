import typing as tp

from flight_control.commander import Telemetry
from flight_control.navigator.mission_preparation.abstract_mission import \
    AbstractMission
from flight_control.route.point import Point


class RthMission(AbstractMission):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def _get_mission_route_points(self) -> tp.List[Point]:
        reversed_route_points = reversed(
            self._telemetry.route.route_points[1:-1])

        mission_points = [point for point in reversed_route_points
                          if point.id < self._telemetry.next_point.id]

        return mission_points

    def _get_mission_zones(self) -> dict:
        zones = self._telemetry.route.zones
        data = {"home_maneuver": zones.home_maneuver,
                "home_approach": zones.home_approach}
        return data

    def _get_landing_yaw(self) -> float:
        return self._telemetry.route.start_yaw
