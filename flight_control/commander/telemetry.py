"""telemetry.py

Telemetry class storing data of telemetry

Copyright 2019 Cervi Robotics
"""
import logging
import math
from collections import namedtuple
from typing import Optional, Text, Tuple, Generator

import transforms3d
from drone_types import msg

from flight_control.commander.sensors import Sensors
from flight_control.route import Route
from flight_control.route.point import Point

logger = logging.getLogger(__name__)


class PointNED(namedtuple('PointNED', ['id', 'x', 'y', 'z', 'yaw'])):
    def __new__(cls, *args, **kwargs):
        if kwargs == {}:
            raise TypeError('PointNED MUST be called with arguments')
        if not isinstance(kwargs.get("id"), int):
            raise TypeError('id of point MUST be integer')
        if not isinstance(kwargs.get("x"), float):
            raise TypeError('x of point MUST be float')
        if not isinstance(kwargs.get("y"), float):
            raise TypeError('y of point MUST be float')
        if not isinstance(kwargs.get("z"), float):
            raise TypeError('z of point MUST be float')
        if not isinstance(kwargs.get("yaw"), float):
            raise TypeError('yaw of point MUST be float')
        return super(PointNED, cls).__new__(cls, *args, **kwargs)

    def __eq__(self, other):
        ERROR = 0.01
        if isinstance(other, PointNED):
            if all([abs(a-b) < ERROR for a, b in zip(self, other)][1:]):
                return True
            else:
                return False
        else:
            raise TypeError('To comparsion files have to be PointNED instance')


class PreviousPoint(PointNED):
    def __new__(cls, *args, **kwargs):
        return super(PreviousPoint, cls).__new__(cls, *args, **kwargs)


class NextPoint(PointNED):
    def __new__(cls, *args, **kwargs):
        return super(NextPoint, cls).__new__(cls, *args, **kwargs)


class Attitude(msg.PxControlAttitude):
    def __init__(self,
                 timestamp: int = 0,
                 q_w: float = 0.0, q_x: float = 0.0, q_y: float = 0.0, q_z: float = 0.0,
                 roll_speed: float = 0.0, pitch_speed: float = 0.0, yaw_speed: float = 0.0):
        super(Attitude, self).__init__(timestamp=timestamp,
                                       q_w=q_w,
                                       q_x=q_x,
                                       q_y=q_y,
                                       q_z=q_z,
                                       roll_speed=roll_speed,
                                       pitch_speed=pitch_speed,
                                       yaw_speed=yaw_speed)

    @property
    def quat(self) -> Tuple[float, float, float, float]:
        return self.q_w, self.q_x, self.q_y, self.q_z

    @property
    def roll(self) -> float:
        roll, _, _ = transforms3d.euler.quat2euler(self.quat)
        return roll

    @property
    def pitch(self) -> float:
        _, pitch, _ = transforms3d.euler.quat2euler(self.quat)
        return pitch

    @property
    def yaw(self) -> float:
        _, _, yaw = transforms3d.euler.quat2euler(self.quat)
        return yaw


class Velocity(msg.PxControlVelocity):
    def __init__(self, timestamp: int = 0, vx: float = 0.0,
                 vy: float = 0.0, vz: float = 0.0):
        super(Velocity, self).__init__(timestamp=timestamp,
                                       vx=vx, vy=vy, vz=vz)

    @property
    def effective(self):
        return math.sqrt(self.vx**2 + self.vy**2 + self.vz**2)


class Telemetry:
    def __init__(self):
        logger.debug("Initializing Telemetry")

        self.previous_point = Point(id=0, latitude=0.0, longitude=0.0, altitude=0.0)
        self.next_point = Point(id=0, latitude=0.0, longitude=0.0, altitude=0.0)
        self.px_control_status = msg.PxControlStatus()
        self.local_pos = msg.PxControlLocalPos()
        self.global_pos = msg.PxControlGlobalPos()
        self.velocity = Velocity()
        self.attitude = Attitude()
        self.route = Route()
        self.sensors = Sensors()
        self.emergency_route_id: Optional[int] = None
        self.emergency_end_point: Optional[Point] = None
        self.initial_yaw: Optional[float] = None
        self.ekf_status = msg.PxControlEKFStatus()
        self.route_data: Optional[Text] = None
        self.actual_route_points: Optional[Generator] = None
        self.battery = msg.PxControlBattery()
        self.rth: bool = False
        self.route_done: bool = False
        self.logs_upload_status = msg.CommLogsUploadStatus()
