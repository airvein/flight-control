"""sensors.py

Sensors class storing data from sensors

Copyright 2019 Cervi Robotics
"""
from drone_types import msg


class Sensors:
    def __init__(self):
        self.presence: msg.SensorsPresence = msg.SensorsPresence()
        self.button_press: msg.SensorsButtonPress = msg.SensorsButtonPress()
