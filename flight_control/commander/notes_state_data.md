## state_data

#### Structure

```json5
{
  '_flags': {
    'enable_navigator': true,
    'rth': true,
    'waiting': false
  },
  'init_flight': {
    'route_id': 'route123',
    'flight_id': 'flight321',
    'cargo': true
  },
  'download_route': {
    'route_data': 'data'
  },
  'emergency_land': {
    'emergency_route_id': 'route_id'
  },
  'stream_type': 'in_flight'
}
```
