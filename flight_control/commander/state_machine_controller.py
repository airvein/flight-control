import logging
import functools
from queue import Queue
from typing import Dict, Callable, Union

from rclpy.executors import Executor
from transitions import Machine, MachineError

from flight_control.commander import StateDataNames
from flight_control.commander.states import STATES, TRANSITIONS, StateNames
from flight_control.command_interpreter import CloudStateNames

logger = logging.getLogger(__name__)


class StateMachineControllerError(MachineError):
    def __init__(self, transition_name):
        super(StateMachineControllerError, self).__init__(
            f"Cannot change state with incomplete data using {transition_name}."
        )


class PendingTransition:
    def __init__(self, transition: Callable, *args, **kwargs):
        self.transition = transition
        self.args = args
        self.kwargs = kwargs


class PendingTask:
    def __init__(self, task: Callable, *args, **kwargs):
        self.task = task
        self.args = args
        self.kwargs = kwargs


class StateMachineController(Machine):
    def __init__(self, state_data: Dict, executor: Executor):
        super(StateMachineController, self).__init__(
            self,
            states=STATES,
            transitions=TRANSITIONS,
            initial=StateNames.WAIT_FOR_NODES
        )
        logger.debug("Initializing StateMachineController")

        self._state_data = state_data
        self._executor = executor

        self.command_status: Dict[str, Union[str, bool]] = {'name': '',
                                                            'success': False}
        self._pending_tasks: Queue[PendingTask] = Queue()
        self._pending_transitions: Queue[PendingTransition] = Queue()

        self.current_device_state = CloudStateNames.SHUTDOWN

    @property
    def state_data(self) -> Dict:
        return self._state_data

    def add_pending_task(self, task: Callable, *args, **kwargs):
        pending_task = PendingTask(task, *args, **kwargs)
        self._pending_tasks.put_nowait(pending_task)

    def add_pending_transition(self, transition: functools.partial, *args,
                               **kwargs):
        logger.debug(f"Adding pending transition: {transition.func}")
        pending_transition = PendingTransition(transition, *args, **kwargs)
        self._pending_transitions.put_nowait(pending_transition)

    def clear_pending_transitions(self):
        while not self._pending_transitions.empty():
            self._pending_transitions.get_nowait()

    def update(self):
        if not self._pending_tasks.empty():
            t = self._pending_tasks.get_nowait()
            self._executor.create_task(t.task, *t.args, **t.kwargs)

        if not self._pending_transitions.empty() and self._pending_tasks.empty():
            pending_transition = self._pending_transitions.get_nowait()
            pending_transition.transition(*pending_transition.args,
                                          **pending_transition.kwargs)

    # noinspection PyUnusedLocal
    def set_init_flight_data(self, *args, **kwargs):
        try:
            route_id = kwargs['route_id']
            flight_id = kwargs['flight_id']
            cargo = kwargs['cargo']
        except KeyError:
            raise StateMachineControllerError(
                'init_flight: required keys not found')

        logger.debug(f"set_init_flight_data. `route_id`: {route_id}, "
                     f"`flight_id`: {flight_id}, `cargo`: {cargo}")

        self._state_data[StateDataNames.INIT_FLIGHT] = {'route_id': route_id,
                                                        'flight_id': flight_id,
                                                        'cargo': cargo}

    # noinspection PyUnusedLocal
    def set_download_route_data(self, *args, **kwargs):
        try:
            route_data: str = kwargs['route_data']
        except KeyError:
            raise StateMachineControllerError(
                'download_route: required keys not found')

        logger.debug("set_download_route_data. "
                     f"`route_data`: {route_data[:100]}")
        self._state_data[StateDataNames.DOWNLOAD_ROUTE] = {
            'route_data': route_data
        }

    # noinspection PyUnusedLocal
    def set_emergency_land_data(self, *args, **kwargs):
        try:
            emergency_route_id = kwargs['emergency_route_id']
        except KeyError:
            raise StateMachineControllerError(
                'emergency_land: required keys not found')

        logger.debug("set_emergency_land_data. "
                     f"`emergency_route_id`: {emergency_route_id}")
        self._state_data[StateDataNames.EMERGENCY_LAND] = {
            'emergency_route_id': emergency_route_id
        }
