import logging
from typing import Text, Dict, List, Type

from flight_control.commander import StateNames
from flight_control.commander import states_handlers
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.telemetry import Telemetry
from flight_control.command_interpreter.command_manager import CommandManager

logger = logging.getLogger(__name__)


class StatesManager:
    def __init__(self, telemetry: Telemetry,
                 state_machine: StateMachineController,
                 command_manager: CommandManager):
        logger.debug("Initializing StatesManager")

        self._state_machine = state_machine
        self._telemetry = telemetry
        self._command_manager = command_manager

        state_handler_class_map: Dict[
            Text, Type[states_handlers.AbstractStateHandler]] = {
            StateNames.WAIT_FOR_NODES: states_handlers.WaitForNodes,
            StateNames.POWERED_ON: states_handlers.PoweredOn,
            StateNames.SERVICE_MODE: states_handlers.ServiceMode,
            StateNames.GET_ROUTE: states_handlers.GetRoute,
            StateNames.INITIALIZE_ROUTE: states_handlers.InitializeRoute,
            StateNames.PARSE_ERROR: states_handlers.ParseError,
            StateNames.WAIT_CARGO: states_handlers.WaitCargo,
            StateNames.CARGO_PRESENT: states_handlers.CargoPresent,
            StateNames.CHECK_READY: states_handlers.CheckReady,
            StateNames.READY: states_handlers.Ready,
            StateNames.FLIGHT_CANCELED: states_handlers.FlightCanceled,
            StateNames.TAKEOFF_START: states_handlers.TakeoffStart,
            StateNames.TAKEOFF_MONITOR: states_handlers.TakeoffMonitor,
            StateNames.CRUISE: states_handlers.Cruise,
            StateNames.CRUISE_RETURN_HOME: states_handlers.CruiseReturnHome,
            StateNames.APPROACH_CRUISE: states_handlers.ApproachCruise,
            StateNames.APPROACH_CRUISE_WAIT: states_handlers.ApproachCruiseWait,
            StateNames.APPROACH: states_handlers.Approach,
            StateNames.CRUISE_LAND: states_handlers.CruiseLand,
            StateNames.EMERGENCY_CRUISE: states_handlers.EmergencyCruise,
            StateNames.LANDING: states_handlers.Landing,
            StateNames.LN_LANDING: states_handlers.LandNowLanding,
            StateNames.EMERGENCY_LANDING: states_handlers.EmergencyLanding,
            StateNames.LANDED: states_handlers.Landed,
            StateNames.EMERGENCY_LANDED: states_handlers.EmergencyLanded,
            StateNames.LN_LANDED: states_handlers.LandNowLanded,
            StateNames.CANCEL_UPLOAD_DATA: states_handlers.CancelUploadData,
            StateNames.CANCEL_END_FLIGHT: states_handlers.CancelEndFlight,
            StateNames.UPLOAD_DATA: states_handlers.UploadData,
            StateNames.EMERGENCY_SHUTDOWN: states_handlers.EmergencyShutdown,
            StateNames.END_FLIGHT: states_handlers.EndFlight,
            StateNames.SHUTDOWN: states_handlers.Shutdown,
            StateNames.BATTERY_SHUTDOWN: states_handlers.BatteryShutdown,
            StateNames.KILLED: states_handlers.Killed
        }

        self._state_map: Dict[Text, states_handlers.AbstractStateHandler] = {}
        for state, handler_class in state_handler_class_map.items():
            self._state_map[state] = handler_class(self._state_machine,
                                                   self._telemetry,
                                                   self._command_manager)

    def handle_state_change(self, name: Text):
        self._state_map[name].handle()

    @property
    def handled_states(self) -> List[Text]:
        return list(self._state_map.keys())
