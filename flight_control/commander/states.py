"""states.py

Definition for she state machine

## Transitions
`before` is a method name that will be called with data passed to the `trigger` method.

Naming convention:
'set_<trigger_name>_data'
"""
from flight_control.commander import StateNames, TriggerNames, CallbackNames

STATES = [
    StateNames.WAIT_FOR_NODES,
    StateNames.POWERED_ON,
    StateNames.SERVICE_MODE,
    StateNames.GET_ROUTE,
    StateNames.INITIALIZE_ROUTE,
    StateNames.PARSE_ERROR,
    StateNames.WAIT_CARGO,
    StateNames.CARGO_PRESENT,
    StateNames.CHECK_READY,
    StateNames.READY,
    StateNames.FLIGHT_CANCELED,
    StateNames.TAKEOFF_START,
    StateNames.TAKEOFF_MONITOR,
    StateNames.CRUISE,
    StateNames.CRUISE_RETURN_HOME,
    StateNames.APPROACH_CRUISE,
    StateNames.APPROACH_CRUISE_WAIT,
    StateNames.APPROACH,
    StateNames.CRUISE_LAND,
    StateNames.EMERGENCY_CRUISE,
    StateNames.LANDING,
    StateNames.LN_LANDING,
    StateNames.EMERGENCY_LANDING,
    StateNames.LANDED,
    StateNames.EMERGENCY_LANDED,
    StateNames.LN_LANDED,
    StateNames.CANCEL_UPLOAD_DATA,
    StateNames.CANCEL_END_FLIGHT,
    StateNames.UPLOAD_DATA,
    StateNames.EMERGENCY_SHUTDOWN,
    StateNames.END_FLIGHT,
    StateNames.SHUTDOWN,
    StateNames.BATTERY_SHUTDOWN,
    StateNames.KILLED
]

TRANSITIONS = [
    {
        'trigger': TriggerNames.REPEAT_WAIT_FOR_NODES,
        'source': StateNames.WAIT_FOR_NODES,
        'dest': StateNames.WAIT_FOR_NODES
    },
    {
        'trigger': TriggerNames.NODES_READY,
        'source': StateNames.WAIT_FOR_NODES,
        'dest': StateNames.POWERED_ON
    },
    {
        'trigger': TriggerNames.SERVICE_MODE,
        'source': StateNames.POWERED_ON,
        'dest': StateNames.SERVICE_MODE,
    },
    {
        'trigger': TriggerNames.EXIT_SERVICE_MODE,
        'source': StateNames.SERVICE_MODE,
        'dest': StateNames.POWERED_ON,
    },
    {
        'trigger': TriggerNames.INIT_FLIGHT,
        'source': StateNames.POWERED_ON,
        'dest': StateNames.GET_ROUTE,
        'before': CallbackNames.INIT_FLIGHT_CALLBACK
    },
    {
        'trigger': TriggerNames.DOWNLOAD_ROUTE,
        'source': StateNames.GET_ROUTE,
        'dest': StateNames.INITIALIZE_ROUTE,
        'before': CallbackNames.DOWNLOAD_ROUTE_CALLBACK
    },
    {
        'trigger': TriggerNames.PARSE_ERROR,
        'source': StateNames.INITIALIZE_ROUTE,
        'dest': StateNames.PARSE_ERROR
    },
    {
        'trigger': TriggerNames.CARGO_PREPARED,
        'source': StateNames.INITIALIZE_ROUTE,
        'dest': StateNames.WAIT_CARGO
    },
    {
        'trigger': TriggerNames.CARGO_DETECTED,
        'source': StateNames.WAIT_CARGO,
        'dest': StateNames.CARGO_PRESENT
    },
    {
        'trigger': TriggerNames.DRONE_PREPARED,
        'source': [StateNames.INITIALIZE_ROUTE, StateNames.CARGO_PRESENT],
        'dest': StateNames.CHECK_READY
    },
    {
        'trigger': TriggerNames.REPEAT_CHECK,
        'source': StateNames.CHECK_READY,
        'dest': StateNames.CHECK_READY
    },
    {
        'trigger': TriggerNames.READY,
        'source': StateNames.CHECK_READY,
        'dest': StateNames.READY
    },
    {
        'trigger': TriggerNames.FLIGHT_START,
        'source': StateNames.READY,
        'dest': StateNames.TAKEOFF_START
    },
    {
        'trigger': TriggerNames.FLIGHT_CANCEL,
        'source': [StateNames.POWERED_ON, StateNames.GET_ROUTE,
                   StateNames.PARSE_ERROR, StateNames.INITIALIZE_ROUTE,
                   StateNames.WAIT_CARGO, StateNames.CARGO_PRESENT,
                   StateNames.CHECK_READY, StateNames.READY],
        'dest': StateNames.FLIGHT_CANCELED
    },
    {
        'trigger': TriggerNames.TAKEOFF,
        'source': StateNames.TAKEOFF_START,
        'dest': StateNames.TAKEOFF_MONITOR
    },
    {
        'trigger': TriggerNames.CRUISING,
        'source': StateNames.TAKEOFF_MONITOR,
        'dest': StateNames.CRUISE
    },
    {
        'trigger': TriggerNames.RETURN_HOME,
        'source': [StateNames.TAKEOFF_MONITOR, StateNames.CRUISE],
        'dest': StateNames.CRUISE_RETURN_HOME
    },
    {
        'trigger': TriggerNames.IN_APPROACH,
        'source': [StateNames.CRUISE, StateNames.CRUISE_RETURN_HOME],
        'dest': StateNames.APPROACH_CRUISE
    },
    {
        'trigger': TriggerNames.IN_MANEUVER,
        'source': StateNames.APPROACH_CRUISE,
        'dest': StateNames.APPROACH_CRUISE_WAIT
    },
    {
        'trigger': TriggerNames.IN_MANEUVER,
        'source': StateNames.APPROACH,
        'dest': StateNames.CRUISE_LAND
    },
    {
        'trigger': TriggerNames.LAND,
        'source': StateNames.APPROACH_CRUISE,
        'dest': StateNames.APPROACH
    },
    {
        'trigger': TriggerNames.LAND,
        'source': StateNames.APPROACH_CRUISE_WAIT,
        'dest': StateNames.CRUISE_LAND
    },
    {
        'trigger': TriggerNames.EMERGENCY_LAND,
        'source': [StateNames.CRUISE, StateNames.APPROACH_CRUISE,
                   StateNames.APPROACH_CRUISE_WAIT,
                   StateNames.CRUISE_RETURN_HOME],
        'dest': StateNames.EMERGENCY_CRUISE,
        'before': CallbackNames.EMERGENCY_LAND_CALLBACK
    },
    {
        'trigger': TriggerNames.LAND_NOW,
        'source': [StateNames.CRUISE, StateNames.CRUISE_RETURN_HOME,
                   StateNames.APPROACH_CRUISE, StateNames.APPROACH_CRUISE_WAIT,
                   StateNames.APPROACH, StateNames.EMERGENCY_CRUISE],
        'dest': StateNames.LN_LANDING,
    },
    {
        'trigger': TriggerNames.ROUTE_DONE,
        'source': StateNames.CRUISE_LAND,
        'dest': StateNames.LANDING
    },
    {
        'trigger': TriggerNames.ROUTE_DONE,
        'source': StateNames.EMERGENCY_CRUISE,
        'dest': StateNames.EMERGENCY_LANDING
    },
    {
        'trigger': TriggerNames.LANDED,
        'source': StateNames.LANDING,
        'dest': StateNames.LANDED
    },
    {
        'trigger': TriggerNames.LANDED,
        'source': StateNames.EMERGENCY_LANDING,
        'dest': StateNames.EMERGENCY_LANDED
    },
    {
        'trigger': TriggerNames.LANDED,
        'source': StateNames.LN_LANDING,
        'dest': StateNames.LN_LANDED,
    },
    {
        'trigger': TriggerNames.FLIGHT_FINISH,
        'source': StateNames.LANDED,
        'dest': StateNames.UPLOAD_DATA
    },
    {
        'trigger': TriggerNames.WAIT_DATA_UPLOAD,
        'source': StateNames.UPLOAD_DATA,
        'dest': StateNames.UPLOAD_DATA
    },
    {
        'trigger': TriggerNames.MAN_SHUTDOWN,
        'source': [StateNames.EMERGENCY_LANDED, StateNames.LN_LANDED,
                   StateNames.KILLED],
        'dest': StateNames.EMERGENCY_SHUTDOWN
    },
    {
        'trigger': TriggerNames.BATTERY_SHUTDOWN,
        'source': [StateNames.EMERGENCY_LANDED, StateNames.LN_LANDED,
                   StateNames.KILLED],
        'dest': StateNames.BATTERY_SHUTDOWN
    },
    {
        'trigger': TriggerNames.FLIGHT_CANCELED,
        'source': StateNames.FLIGHT_CANCELED,
        'dest': StateNames.CANCEL_UPLOAD_DATA
    },
    {
        'trigger': TriggerNames.DONE_DATA_UPLOAD,
        'source': StateNames.UPLOAD_DATA,
        'dest': StateNames.END_FLIGHT
    },
    {
        'trigger': TriggerNames.DONE_FLIGHT_CANCEL,
        'source': StateNames.CANCEL_UPLOAD_DATA,
        'dest': StateNames.CANCEL_END_FLIGHT
    },
    {
        'trigger': TriggerNames.SYSTEM_SHUTDOWN,
        'source': [StateNames.CANCEL_END_FLIGHT, StateNames.END_FLIGHT],
        'dest': StateNames.SHUTDOWN
    },
    {
        'trigger': TriggerNames.DRONE_KILL,
        'source': [StateNames.TAKEOFF_START, StateNames.TAKEOFF_MONITOR,
                   StateNames.CRUISE, StateNames.CRUISE_LAND,
                   StateNames.CRUISE_RETURN_HOME, StateNames.APPROACH_CRUISE,
                   StateNames.APPROACH, StateNames.APPROACH_CRUISE_WAIT,
                   StateNames.EMERGENCY_CRUISE, StateNames.EMERGENCY_LANDING,
                   StateNames.LN_LANDING, StateNames.LANDING],
        'dest': StateNames.KILLED
    }
]

TRANSITIONS_NAMES = [t['trigger'] for t in TRANSITIONS]

WAIT_ALLOWED_STATES = [
    StateNames.CRUISE,
    StateNames.CRUISE_RETURN_HOME,
    StateNames.APPROACH_CRUISE,
    StateNames.APPROACH,
    StateNames.EMERGENCY_CRUISE
]

LAND_NOW_ALLOWED_STATES = [
    StateNames.CRUISE,
    StateNames.CRUISE_RETURN_HOME,
    StateNames.APPROACH_CRUISE,
    StateNames.APPROACH_CRUISE_WAIT,
    StateNames.APPROACH,
    StateNames.EMERGENCY_CRUISE
]
