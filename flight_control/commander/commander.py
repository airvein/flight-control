import logging
import time

from rclpy.executors import Executor
from rclpy.node import Node

import drone_types.msg as drone_msgs
from flight_control.command_interpreter import CloudStateNames
from flight_control.command_interpreter.publishers import Publishers
from flight_control.commander import StateNames, StateDataNames
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states_manager import StatesManager
from flight_control.navigator.navigator import Navigator
from flight_control.state_updater.state_updater import StateUpdater
from flight_control.command_interpreter.command_manager import CommandManager

logger = logging.getLogger(__name__)


class Commander:
    def __init__(self, node: Node, executor: Executor,
                 states_manager: StatesManager,
                 state_machine_controller: StateMachineController,
                 navigator: Navigator,
                 publishers: Publishers,
                 command_manager: CommandManager):
        logger.debug("Initializing Commander")

        self._node = node
        self._executor = executor
        self._states_manager = states_manager
        self._state_machine = state_machine_controller
        self._state_machine.after_state_change = self.state_changed
        self._navigator = navigator
        self._publishers = publishers
        self._command_manager = command_manager

        self._state_updater = StateUpdater(
            self._navigator.telemetry, self._state_machine)
        self._prev_status = drone_msgs.FlightControlStatus()

        self.DRONE_STREAM_INTERVAL = {
            StateDataNames.StreamType.IN_HANGAR: 10.0,
            StateDataNames.StreamType.IN_FLIGHT: 1.0,
            StateDataNames.StreamType.POSITION: 5.0
        }

        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR

        self._tick1_loops = 0
        self._ticker_1s = self._node.create_timer(1.0, self._ticker_1s_cb)
        self._ticker_100ms = self._node.create_timer(0.1,
                                                     self._ticker_100ms_cb)

        time.sleep(1)  # status msg won't be received without sleeping (fix?)
        self.state_changed()  # State handle has to be force-called on init

    def state_changed(self, *args, **kwargs):
        if self._prev_status.flight_state != self._state_machine.state:
            logger.info(f"State changed to {self._state_machine.state}")
        self._states_manager.handle_state_change(self._state_machine.state)
        self._prev_status = self._command_manager.execute(
            "send_drone_status",
            previous_status=self._prev_status,
            current_flag_state=self._get_flag_state(
                StateDataNames.Flag.WAITING)
        )

    def _get_flag_state(self, flag_name) -> bool:
        return bool(self._state_machine.state_data[
                        StateDataNames.FLAGS].get(flag_name))

    def _ticker_1s_cb(self):
        self._command_manager.execute("send_heartbeat")
        if self._state_machine.state != StateNames.WAIT_FOR_NODES:
            self._send_drone_stream()

    def _send_drone_stream(self):
        loops_to_call_drone_stream = self.DRONE_STREAM_INTERVAL[
            self._state_machine.state_data[StateDataNames.STREAM_TYPE]]

        if (self._tick1_loops % loops_to_call_drone_stream) == 0:
            self._command_manager.execute(
                "send_drone_stream",
                stream_type=self._state_machine.state_data[
                    StateDataNames.STREAM_TYPE]
            )

            self._tick1_loops = 0
        self._tick1_loops += 1

    def _ticker_100ms_cb(self):
        self._timer_update_callback()

    def _timer_update_callback(self):
        self._state_updater.update()
        self._state_machine.update()

        if (self._state_machine.state is StateNames.GET_ROUTE
                and self._navigator.telemetry.route_data is not None):
            self._state_machine.add_pending_transition(
                self._state_machine.download_route,
                route_data=self._navigator.telemetry.route_data
            )

        # navigator initialization
        if self._get_flag_state(StateDataNames.Flag.NAVIGATOR_INIT):
            self._state_machine.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.NAVIGATOR_INIT] = False
            logger.info("Initializing Navigator")

        # entering waiting state
        if (self._get_flag_state(StateDataNames.Flag.WAITING)
                and not self._navigator.waiting):
            self._navigator.wait()
            self._command_manager.execute("wait")

            current_device_state = self._state_machine.current_device_state
            # update cloud state only if new state is not APPROACH_CRUISE_WAIT
            if self._state_machine.state is not StateNames.APPROACH_CRUISE_WAIT:
                self._command_manager.execute(
                    "send_device_state",
                    new_state=CloudStateNames.waiting(current_device_state)
                )

        # exiting waiting state
        if (not self._get_flag_state(StateDataNames.Flag.WAITING)
                and self._navigator.waiting):
            self._navigator.continue_flight()
            self._command_manager.execute("continue")

            current_device_state = self._state_machine.current_device_state
            # update cloud state only if new state is not CRUISE_LAND
            if self._state_machine.state is not StateNames.CRUISE_LAND:
                self._command_manager.execute(
                    "send_device_state",
                    new_state=CloudStateNames.del_waiting(current_device_state)
                )

        # update navigator state
        if self._get_flag_state(StateDataNames.Flag.NAVIGATOR_ENABLE):
            self._navigator.update()

            self._command_manager.execute("send_route_progress")
