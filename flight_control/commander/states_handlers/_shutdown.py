import os
import time
import logging

from periphery import GPIO

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager
from flight_control.common.exceptions import MachineShutdown
from flight_control.settings import Power

logger = logging.getLogger(__name__)


class Shutdown(AbstractStateHandler):

    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

        if Power.SHUTDOWN_STREAM:
            self._shutdown_pin = GPIO(Power.STREAM_SHUTDOWN_PIN, 'low')

    def handle(self):
        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR

        self._command_manager.execute(
            "send_device_state", new_state=CloudStateNames.SHUTDOWN
        )

        self._command_manager.execute(
            "set_mqtt_client_state", connected=False
        )

        self.shutdown_system()

    def shutdown_system(self):
        # force send_drone_status because we never leave this state
        self._command_manager.execute(
            "send_drone_status",
            previous_status=None,
            current_flag_state=False
        )
        self._shutdown_stream()

        time.sleep(1)  # let process all messages before shutdown

        # Check is should NOT shutdown system
        if Power.NO_SHUTDOWN:
            logger.info("Should shutdown system... but not going to")
            raise MachineShutdown("Intended interrupt")

        logger.warning("!!! Shutting down system !!!")

        os.system('shutdown -h now')

    def _shutdown_stream(self):
        if Power.SHUTDOWN_STREAM:
            self._shutdown_pin.write(True)
