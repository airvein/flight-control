import logging

from drone_types.msg import PxControlStatus
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager

logger = logging.getLogger(__name__)


class WaitForNodes(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR

        if (self._telemetry.px_control_status.connection_state
                == PxControlStatus.CONNECTION_STATE_CONNECTED):
            self._state_machine.add_pending_transition(
                self._state_machine.nodes_ready
            )
        else:
            self._state_machine.add_pending_transition(
                self._state_machine.repeat_wait_for_nodes
            )
