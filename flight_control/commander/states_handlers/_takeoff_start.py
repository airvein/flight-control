from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager


class TakeoffStart(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_FLIGHT

        # enable navigator
        self._state_machine.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.NAVIGATOR_ENABLE] = True

        self._command_manager.execute("start_mission")

        self._command_manager.execute("send_device_state",
                                      new_state=CloudStateNames.TAKEOFF)

        self._command_manager.execute("set_mqtt_client_state", connected=False)
