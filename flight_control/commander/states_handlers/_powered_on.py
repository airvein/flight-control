from flight_control.command_interpreter import DroneHangarMessageNames, CloudStateNames
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager


class PoweredOn(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._state_machine.add_pending_task(
            self._command_manager.execute, "send_drone_state",
            state=DroneHangarMessageNames.DRONE_LAUNCHED
        )

        self._state_machine.add_pending_task(
            self._command_manager.execute, "send_device_state",
            new_state=CloudStateNames.POWERED_ON
        )

        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR
