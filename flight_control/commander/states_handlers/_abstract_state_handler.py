import logging
from abc import ABC, abstractmethod
from typing import Optional

from flight_control.commander import Telemetry
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.command_interpreter.command_manager import CommandManager

logger = logging.getLogger(__name__)


class AbstractStateHandler(ABC):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry,
                 command_manager: Optional[CommandManager]):
        self._state_machine = state_machine
        self._telemetry = telemetry
        self._command_manager = command_manager

    @abstractmethod
    def handle(self):
        pass
