from flight_control.command_interpreter import CloudStateNames, MissionTypes
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager


class EmergencyCruise(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_FLIGHT

        state_data = self._state_machine.state_data[
            StateDataNames.EMERGENCY_LAND]
        self._telemetry.emergency_route_id = state_data['emergency_route_id']

        self._command_manager.execute("send_mission_data",
                                      mission=MissionTypes.EMERGENCY)

        self._command_manager.execute("send_device_state",
                                      new_state=CloudStateNames.EL_CRUISE)
