import logging

from flight_control.command_interpreter import DroneHangarMessageNames, MissionTypes
from flight_control.commander import StateDataNames
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.commander.telemetry import Telemetry
from flight_control.route import Route
from flight_control.route.route import RouteError
from flight_control.command_interpreter.command_manager import CommandManager

logger = logging.getLogger(__name__)


class InitializeRoute(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        try:
            self._telemetry.route = Route.from_json(
                self._state_machine.state_data[StateDataNames.DOWNLOAD_ROUTE][
                    "route_data"]
            )

            self._command_manager.execute("send_mission_data",
                                          mission=MissionTypes.NORMAL)

            drone_state = DroneHangarMessageNames.DRONE_INITIALIZED
            self._command_manager.execute("send_drone_state", state=drone_state)

            self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
                StateDataNames.StreamType.IN_HANGAR

        except RouteError as error:
            logger.error(f'Failed to parse route. Reason: {error}')
            self._state_machine.add_pending_transition(
                self._state_machine.parse_error)
