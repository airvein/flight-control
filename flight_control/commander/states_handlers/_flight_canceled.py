from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.commander.telemetry import Telemetry
from flight_control.command_interpreter.command_manager import CommandManager


class FlightCanceled(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._command_manager.execute(
            "send_device_state", new_state=CloudStateNames.FLIGHT_CANCELING
        )

        self._telemetry.route = None

        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR

        self._state_machine.add_pending_transition(
            self._state_machine.flight_canceled)
