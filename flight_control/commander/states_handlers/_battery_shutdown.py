import logging

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_handlers._shutdown import Shutdown
from flight_control.command_interpreter.command_manager import CommandManager

logger = logging.getLogger(__name__)


class BatteryShutdown(Shutdown):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.POSITION

        self._command_manager.execute(
            "send_device_state", new_state=CloudStateNames.SHUTDOWN
        )

        logger.info("System shutdown called by low battery")
        self.shutdown_system()
