from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager


class LandNowLanded(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.POSITION

        self._state_machine.add_pending_task(
            self._command_manager.execute, "send_device_state",
            new_state=CloudStateNames.LN_LANDED
        )

        # disable navigator
        self._state_machine.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.NAVIGATOR_ENABLE] = False
