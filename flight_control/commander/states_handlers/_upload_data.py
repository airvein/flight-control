from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager
from drone_types.msg import CommLogsUploadStatus


class UploadData(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR

        if (self._telemetry.logs_upload_status.upload_status
                == CommLogsUploadStatus.WAITING_FOR_TRIGGER):

            self._command_manager.execute("send_device_state",
                                          new_state=self._get_cloud_state_name)

            self._command_manager.execute("trigger_logs_upload")
            self._telemetry.logs_upload_status.upload_status = \
                CommLogsUploadStatus.IN_PROGRESS

            self._state_machine.add_pending_transition(
                self._state_machine.wait_data_upload)

        elif (self._telemetry.logs_upload_status.upload_status
                == CommLogsUploadStatus.IN_PROGRESS):

            self._state_machine.add_pending_transition(
                self._state_machine.wait_data_upload)
        else:
            self._state_machine.add_pending_transition(
                self._state_machine.done_data_upload)

    @property
    def _get_cloud_state_name(self):
        rth = self._state_machine.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.RTH]
        new_state = CloudStateNames.rth(CloudStateNames.FLIGHT_ENDING) if rth \
            else CloudStateNames.FLIGHT_ENDING

        return new_state
