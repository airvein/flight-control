from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager


class ApproachCruiseWait(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    def handle(self):
        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_FLIGHT

        self._state_machine.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = True

        rth = self._state_machine.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.RTH]
        new_state = CloudStateNames.rth(CloudStateNames.WAITING_APPROACH) if rth \
            else CloudStateNames.WAITING_APPROACH

        self._state_machine.add_pending_task(
            self._command_manager.execute, "send_device_state",
            new_state=new_state
        )
