import logging

from drone_types.msg import PxControlStatus
from flight_control.commander import Telemetry, StateDataNames
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager
from flight_control.settings import MaxEkfVariance

logger = logging.getLogger(__name__)


class CheckReady(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)

    @property
    def _is_battery_present(self) -> bool:
        battery_present = self._telemetry.sensors.presence.battery
        logger.debug(f"Battery present: {battery_present}")
        return battery_present

    @property
    def _is_armable(self) -> bool:
        drone_armable = (self._telemetry.px_control_status.arm_state
                         == PxControlStatus.ARM_STATE_READY)
        logger.debug(f"Drone armable: {drone_armable}")
        return drone_armable

    @property
    def _is_ekf_ready(self) -> bool:
        ekf = self._telemetry.ekf_status

        vel_ok = ekf.velocity_variance < MaxEkfVariance.VELOCITY
        pos_horiz_ok = ekf.pos_horiz_variance < MaxEkfVariance.POS_HORIZ
        pos_vert_ok = ekf.pos_vert_variance < MaxEkfVariance.POS_VERT
        compass_ok = ekf.compass_variance < MaxEkfVariance.COMPASS

        ekf_ok = vel_ok and pos_horiz_ok and pos_vert_ok and compass_ok

        if ekf_ok:
            return True
        else:
            logger.warning("EKF is not ready")
            if not vel_ok:
                logger.warning(f"vel_variance: {ekf.velocity_variance}")
            if not pos_horiz_ok:
                logger.warning(f"pos_horiz_variance: {ekf.pos_horiz_variance}")
            if not pos_vert_ok:
                logger.warning(f"pos_vert_variance: {ekf.pos_vert_variance}")
            if not compass_ok:
                logger.warning(f"compass_variance: {ekf.compass_variance}")

            return False

    @property
    def _is_normal_mission_ready(self) -> bool:
        normal_mission_ready = (self._telemetry.px_control_status.mission_state
                                == PxControlStatus.MISSION_STATE_NORMAL)
        logger.debug(f"Normal mission ready: {normal_mission_ready}")
        return normal_mission_ready

    def handle(self):
        ready = all((self._is_battery_present, self._is_armable,
                    self._is_ekf_ready, self._is_normal_mission_ready))

        self._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR

        if ready:
            self._state_machine.add_pending_transition(
                self._state_machine.ready
            )
        else:
            self._state_machine.add_pending_transition(
                self._state_machine.repeat_check
            )
