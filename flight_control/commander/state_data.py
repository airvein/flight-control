"""state_data.py

State Data object that holds all data and flags required for proper
state handling

Copyright 2020 Cervi Robotics

"""

from collections import UserDict

from flight_control.commander._constants import StateDataNames


def get_state_data():
    """:returns pre-populated StateData object"""
    _default_keys = {
        StateDataNames.INIT_FLIGHT: None,
        StateDataNames.DOWNLOAD_ROUTE: None,
        StateDataNames.EMERGENCY_LAND: None,
        StateDataNames.STREAM_TYPE: None,
        StateDataNames.FLAGS: {
            StateDataNames.Flag.NAVIGATOR_INIT: False,
            StateDataNames.Flag.NAVIGATOR_ENABLE: False,
            StateDataNames.Flag.NAVIGATOR_EMERGENCY: False,
            StateDataNames.Flag.RTH: False,
            StateDataNames.Flag.WAITING: False,
            StateDataNames.Flag.WAITING_TIME_EXCEEDED: False
        }
    }
    return StateData(_default_keys)


class StateData(UserDict):
    def __missing__(self, key):
        if isinstance(key, str):
            raise KeyError(key)
        return self[str(key)]

    def __contains__(self, item):
        return str(item) in self.data

    def __setitem__(self, key, value):
        self.data[str(key)] = value
