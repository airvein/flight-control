"""abstract_service.py

Abstract Service for services in flight control module

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node
from rclpy.executors import Executor

from flight_control.commander.state_machine_controller import \
    StateMachineController


class AbstractService:
    def __init__(self, node: Node, executor: Executor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller
        self.name = None
