"""get_command_status.py

GetCommandStatus service that returns success/failure of cloud command
execution.

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node
from std_srvs import srv

from flight_control.command_interpreter.services.abstract_service import \
    AbstractService
from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class GetCommandStatus(AbstractService):

    def __init__(self, node: Node, executor,
                 state_machine_controller: StateMachineController):
        super().__init__(node, executor, state_machine_controller)
        self.service = self.node.create_service(
            srv.Trigger, "/flight_control/command_status",
            self._command_response_callback
        )

    # noinspection PyUnusedLocal
    def _command_response_callback(self, request: srv.Trigger.Request,
                                   response: srv.Trigger.Response):
        logger.debug("Command response service called")
        command = self.state_machine_controller.command_status

        response.success = command["success"]
        response.message = command["name"]

        self.state_machine_controller.command_status.update(
            name='', success=False)
        logger.debug(f"Sending response... Success: {response.success}, "
                     f"Message: {response.message}")
        return response
