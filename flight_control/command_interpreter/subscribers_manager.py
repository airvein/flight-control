"""subscribers_manager.py

ROS Subscribers Manager which manage subscribers

Copyright 2019 Cervi Robotics
"""
import logging

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node

from flight_control.command_interpreter.subscribers import *
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.telemetry import Telemetry


logger = logging.getLogger(__name__)


class SubscribersManager:
    def __init__(self, node: Node, telemetry: Telemetry,
                 executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        logger.debug("Initializing SubscribersManager")

        self.node = node
        self.telemetry = telemetry
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self._subscribers = {
            'comm_kill': comm_kill.CommKill(
                self.node, self.state_machine_controller),
            'comm_flight_cancel': comm_cancel_flight.CommFlightCancel(
                self.node, self.executor, self.state_machine_controller),
            'comm_wait': comm_wait.CommWait(
                self.node, self.executor, self.state_machine_controller),
            'comm_continue': comm_continue.CommContinue(
                self.node, self.executor, self.state_machine_controller),
            'comm_route_data': comm_route_data.CommRouteData(
                self.node, self.telemetry),
            'comm_init': comm_init.CommInit(
                self.node, self.executor, self.state_machine_controller),
            'comm_cargo_prepare': comm_cargo_prepare.CommCargoPrepare(
                self.node, self.executor, self.state_machine_controller),
            'comm_drone_prepare': comm_drone_prepare.CommDronePrepare(
                self.node, self.executor, self.state_machine_controller),
            'comm_start_flight': comm_start_flight.CommStartFlight(
                self.node, self.executor, self.state_machine_controller),
            'comm_land': comm_land.CommLand(
                self.node, self.executor, self.state_machine_controller),
            'comm_finish': comm_finish.CommFinish(
                self.node, self.executor, self.state_machine_controller),
            'comm_shutdown_drone': comm_shutdown_drone.CommShutdownDrone(
                self.node, self.executor, self.state_machine_controller),
            'comm_return_home': comm_return_home.CommReturnHome(
                self.node, self.executor, self.state_machine_controller),
            'comm_emergency_land': comm_emergency_land.CommEmergencyLand(
                self.node, self.state_machine_controller, self.telemetry),
            'comm_land_now': comm_land_now.CommLandNow(
                self.node, self.executor, self.state_machine_controller),
            'comm_logs_upload_status': comm_logs_upload_status.CommunicationLogsUploadStatus(
                self.node, self.telemetry),
            'comm_service_mode': comm_service_mode.CommServiceMode(
                self.node, self.state_machine_controller),
            'px_control_attitude': px_control_attitude.PxControlAttitude(
                self.node, self.telemetry),
            'px_control_local_pos': px_control_local_pos.PxControlLocalPos(
                self.node, self.telemetry),
            'px_control_global_pos': px_control_global_pos.PxControlGlobalPos(
                self.node, self.telemetry),
            'px_control_status': px_control_status.PxControlStatus(
                self.node, self.telemetry),
            'px_control_velocity': px_control_velocity.PxControlVelocity(
                self.node, self.telemetry),
            'px_control_ekf_status': px_control_ekf_status.PxControlEKFStatus(
                self.node, self.telemetry),
            'px_control_battery': px_control_battery.PxControlBattery(
                self.node, self.telemetry),
            'sensors_button_press': sensors_button_press.SensorsButtonPress(
                self.node, self.telemetry),
            'sensors_presence': sensors_presence.SensorsPresence(
                self.node, self.telemetry),
        }

    @property
    def subscribers(self) -> dict:
        return self._subscribers

    def _is_subscription_registered(self, subscription_name: str) -> bool:
        return subscription_name in self._subscribers.keys()
