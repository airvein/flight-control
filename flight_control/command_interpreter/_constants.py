from enum import IntEnum
from typing import Text


class CloudStateNames:
    class Flag:
        RTH = 'rth_'
        WAITING = '-waiting'
    SHUTDOWN = 'shutdown'
    POWERED_ON = 'powered_on'
    INITIALIZING = 'initializing'
    CARGO_PREPARE = 'cargo_prepare'
    READY = 'ready'
    TAKEOFF = 'takeoff'
    CRUISE = 'cruise'
    APPROACH_CRUISE = 'approach_cruise'
    WAITING_APPROACH = 'waiting_approach'
    APPROACH = 'approach'
    LANDING = 'landing'
    LANDED = 'landed'
    FLIGHT_ENDING = 'flight_ending'
    FLIGHT_CANCELING = 'flight_canceling'
    EL_CRUISE = 'el_cruise'
    EL_LANDING = 'el_landing'
    EL_LANDED = 'el_landed'
    LN_LANDING = 'ln_landing'
    LN_LANDED = 'ln_landed'
    KILLED = 'killed'

    @staticmethod
    def rth(state: Text) -> Text:
        return CloudStateNames.Flag.RTH + state

    @staticmethod
    def waiting(state: Text) -> Text:
        return state + CloudStateNames.Flag.WAITING

    @staticmethod
    def del_waiting(state: Text) -> Text:
        if state.endswith(CloudStateNames.Flag.WAITING):
            return state[:-len(CloudStateNames.Flag.WAITING)]
        return state


class DroneHangarMessageNames:
    FLIGHT_CANCELED = 'flight_canceled'
    DRONE_LAUNCHED = 'drone_launched'
    DRONE_INITIALIZED = 'drone_initialized'
    CARGO_READY = 'cargo_ready'
    DRONE_READY = 'drone_ready'
    FLIGHT_ENDED = 'flight_ended'


class RosTopicNames:
    class Communication:
        NAME = 'communication'
        SEND_DRONE_STATE = f"/{NAME}/send_drone_state"
        SEND_DRONE_STREAM = f"/{NAME}/send_drone_stream"
        SEND_DEVICE_STATE = f"/{NAME}/send_device_state"
        DOWNLOAD_ROUTE = f"/{NAME}/download_route"
        HANGAR_CONNECTION_CONTROL = f"/{NAME}/hangar_connection_control"
        SEND_ALERT = f"/{NAME}/send_alert"
        DUMP_DATA = f"/{NAME}/dump_data"

    class PxControl:
        NAME = 'px_control'
        TAKEOFF = f"/{NAME}/takeoff"
        LAND = f"/{NAME}/land"
        KILL = f"/{NAME}/kill"


class MissionTypes:
    NORMAL = 'normal'
    EMERGENCY = 'emergency'
    RTH = 'rth'


class AlertCodes(IntEnum):
    ROUTE_PARSE_ERROR = 19
