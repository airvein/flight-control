"""comm_continue.py

Communication subscriber dealing with cloud `continue` command

Copyright 2020 Cervi Robotics
"""
import logging
from typing import Tuple, Optional

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from std_msgs import msg

from flight_control.commander import StateNames
from flight_control.commander import StateDataNames
from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommContinue:
    def __init__(self, node: Node, executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/continue", self.handle, qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug(f"Processing 'continue' command. Message: {message}")

        can_trigger_continue, reason = self._can_trigger_continue()
        if can_trigger_continue:
            self.state_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.WAITING] = False

            logger.info("Continue performed CORRECTLY.")
        else:
            logger.error(f"Continue FAILED. Reason: {reason}")

        self.state_machine_controller.command_status.update(
            name="continue", success=can_trigger_continue)

    def _can_trigger_continue(self) -> Tuple[bool, Optional[str]]:
        machine_state = self.state_machine_controller.state

        waiting_flag = self.state_machine_controller.state_data[
            StateDataNames.FLAGS][StateDataNames.Flag.WAITING]

        correct_state = machine_state != StateNames.APPROACH_CRUISE_WAIT

        reason = None
        if not correct_state:
            reason = f"Drone state is {machine_state}"
        elif not waiting_flag:
            reason = "Drone not in waiting state"

        result = waiting_flag and correct_state

        response = (result, reason)

        return response
