"""comm_init.py

Communication subscriber dealing with Init flight

Copyright 2019 Cervi Robotics
"""
import logging

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError

from drone_types import msg
from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommInit:
    def __init__(self, node: Node, executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.CommInit, "/communication/init", self.handle, qos_profile=10)

    def handle(self, message: msg.CommInit):
        logger.debug(f"Processing Init message. Message: {message}")
        try:
            self.state_machine_controller.init_flight(
                route_id=message.route_id,
                flight_id=message.flight_id,
                cargo=message.cargo
            )
            logger.info(f"Init performed CORRECTLY.")
        except MachineError:
            logger.error("Init FAILED.")
