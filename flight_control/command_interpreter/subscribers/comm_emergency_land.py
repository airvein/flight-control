"""comm_emergency_land.py

Communication subscriber dealing with DronePrepare

Copyright 2019 Cervi Robotics
"""
import logging
from rclpy.node import Node
from transitions import MachineError

from drone_types import msg
from flight_control.commander import Telemetry
from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommEmergencyLand:
    def __init__(self,
                 node: Node,
                 state_machine_controller: StateMachineController,
                 telemetry: Telemetry):
        self.node = node
        self.state_machine_controller = state_machine_controller
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.CommEmergencyLand, "/communication/emergency_land",
            self.handle, qos_profile=10)

    def handle(self, message: msg.CommEmergencyLand):
        logger.debug(f"Processing EmergencyLand message. Message: {message}")

        route_ids = [route.route_id for route
                     in self.telemetry.route.emergency_routes]
        if str(message.emergency_route) in route_ids:
            try:
                self.state_machine_controller.emergency_land(
                    emergency_route_id=message.emergency_route)

                success = True
                logger.info("EmergencyLand performed CORRECTLY.")

            except MachineError as e:
                success = False
                logger.error(f"EmergencyLand FAILED. MachineError: {e}")
        else:
            success = False
            logger.error("EmergencyLand FAILED. No emergency route with such ID")

        self.state_machine_controller.command_status.update(
            name="emergency_land", success=success)
