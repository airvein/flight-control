"""px_control_local_pos.py

PxControl subscriber dealing with LocalPos

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander.telemetry import Telemetry


class PxControlLocalPos:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.PxControlLocalPos, "/px_control/local_pos", self.handle,
            qos_profile=10)

    def handle(self, message: msg.PxControlLocalPos):
        self.telemetry.local_pos = message
