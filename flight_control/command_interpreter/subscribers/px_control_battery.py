"""px_control_battery.py

PxControl subscriber dealing with Battery

Copyright 2020 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander.telemetry import Telemetry


class PxControlBattery:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.PxControlBattery, "/px_control/battery", self.handle,
            qos_profile=10)

    def handle(self, message: msg.PxControlBattery):
        self.telemetry.battery = message
