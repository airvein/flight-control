"""comm_drone_prepare.py

Communication subscriber dealing with DronePrepare

Copyright 2019 Cervi Robotics
"""
import logging
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError
from std_msgs import msg

from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommStartFlight:
    def __init__(self, node: Node, executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/start_flight", self.handle,
            qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug("Processing Start Flight message.")
        try:
            self.state_machine_controller.flight_start()

            success = True
            logger.info("StartFlight performed CORRECTLY.")
        except MachineError:
            logger.error("StartFlight FAILED.")
            success = False

        self.state_machine_controller.command_status.update(
            name="start_flight", success=success)
