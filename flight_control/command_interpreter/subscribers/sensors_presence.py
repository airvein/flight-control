"""sensors_presence.py

Sensors subscriber dealing with Presence

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander.telemetry import Telemetry


class SensorsPresence:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.SensorsPresence, "/sensors/presence", self.handle,
            qos_profile=10)

    def handle(self, message: msg.SensorsPresence):
        self.telemetry.sensors.presence = message
