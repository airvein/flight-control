"""comm_route_data.py

Communication subscriber dealing with Route Data

Copyright 2019 Cervi Robotics
"""
import logging

from rclpy.node import Node

from drone_types import msg
from flight_control.commander.telemetry import Telemetry

logger = logging.getLogger(__name__)


class CommRouteData:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.CommRouteData, "/communication/route_data", self.handle,
            qos_profile=10)

    def handle(self, message: msg.CommRouteData):
        logger.debug(f"Received route data: {message.data}")
        self.telemetry.route_data = message.data
