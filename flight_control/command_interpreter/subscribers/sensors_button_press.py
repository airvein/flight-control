"""button_press.py

Sensors subscriber dealing with Button Press

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander.telemetry import Telemetry


class SensorsButtonPress:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.SensorsButtonPress, "/sensors/button_press", self.handle,
            qos_profile=10)

    def handle(self, message: msg.SensorsButtonPress):
        self.telemetry.sensors.button_press = message
