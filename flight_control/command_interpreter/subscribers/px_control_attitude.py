"""px_control_attitude.py

PxControl subscriber dealing with attitude

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander.telemetry import Telemetry, Attitude


class PxControlAttitude:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.PxControlAttitude, "/px_control/attitude", self.handle,
            qos_profile=10)

    def handle(self, message: msg.PxControlAttitude):
        self.telemetry.attitude = Attitude(
            message.timestamp,
            message.q_w,
            message.q_x,
            message.q_y,
            message.q_z,
            message.roll_speed,
            message.pitch_speed,
            message.yaw_speed
        )
