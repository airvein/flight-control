"""comm_wait.py

Communication subscriber dealing with cloud `wait` command

Copyright 2020 Cervi Robotics
"""
import logging
from typing import Tuple, Optional

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from std_msgs import msg

from flight_control.commander import StateDataNames
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states import WAIT_ALLOWED_STATES

logger = logging.getLogger(__name__)


class CommWait:
    def __init__(self, node: Node, executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/wait", self.handle, qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug("Processing 'wait' command.")

        can_trigger_wait, reason = self._can_trigger_wait()
        if can_trigger_wait:
            self.state_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.WAITING] = True

            logger.info("Wait performed CORRECTLY")
        else:
            logger.error(f"Wait command trigger refused. Reason: {reason}")

        self.state_machine_controller.command_status.update(
            name="wait", success=can_trigger_wait)

    def _can_trigger_wait(self) -> Tuple[bool, Optional[str]]:
        correct_state = self.state_machine_controller.state in WAIT_ALLOWED_STATES
        waiting_flag = self.state_machine_controller.state_data[
            StateDataNames.FLAGS][StateDataNames.Flag.WAITING]
        waiting_time_exceeded = self.state_machine_controller.state_data[
            StateDataNames.FLAGS][StateDataNames.Flag.WAITING_TIME_EXCEEDED]

        reason = None
        if not correct_state:
            reason = "Drone is in incorrect state"
        elif waiting_flag:
            reason = "Drone already in waiting state"
        elif waiting_time_exceeded:
            reason = "Maximum time in wait state exceeded"

        result = correct_state and not waiting_flag and not waiting_time_exceeded

        response = (result, reason)
        return response
