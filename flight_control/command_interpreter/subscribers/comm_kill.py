"""comm_kill.py

Communication subscriber dealing with kill cloud command

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node
from transitions import MachineError
from std_msgs import msg

from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommKill:
    def __init__(self, node: Node,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/kill", self.handle, qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug(f"Processing kill message")
        try:
            self.state_machine_controller.drone_kill()

            success = True
            logger.info("Kill performed CORRECTLY.")
        except MachineError as e:
            success = False
            logger.error(f"Kill FAILED. MachineError: {e}")

        self.state_machine_controller.command_status.update(
            name="kill", success=success)
