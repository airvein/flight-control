"""comm_land_now.py

Communication subscriber dealing with land_now command

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError
from std_msgs import msg

from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommLandNow:
    def __init__(self, node: Node,
                 executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/land_now", self.handle, qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug("Processing land_now message.")
        try:
            # transition can't be triggered by self.executor.create_task()
            # because it won't catch MachineError
            self.state_machine_controller.land_now()

            success = True
            logger.info("Land_now performed CORRECTLY.")

        except MachineError:
            success = False
            logger.error(f"Land_now FAILED.")

        self.state_machine_controller.command_status.update(
            name="land_now", success=success)
