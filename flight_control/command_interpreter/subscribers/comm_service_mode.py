"""comm_service_mode.py

Communication subscriber dealing with service_mode. The subscription is
supposed to handle both initiliaizing and leaving `service_mode` state

Copyright 2020 Cervi Robotics
"""

from rclpy.node import Node
from transitions import MachineError
from std_msgs import msg

from flight_control.commander.state_machine_controller import \
    StateMachineController


class CommServiceMode:
    def __init__(self, node: Node,
                 state_machine_controller: StateMachineController):
        self.logger = node.get_logger()
        self.node = node
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/service_mode", self.handle)

    def handle(self, message: msg.Bool):
        self.logger.debug("Processing ServiceMode message.")
        transition = self.state_machine_controller.service_mode if message.data\
            else self.state_machine_controller.exit_service_mode
        try:
            transition()
            success = True
        except MachineError as e:
            success = False
            self.logger.error(f"ServiceMode transition FAILED. Error: {e}")

        self.state_machine_controller.command_status.update(
            name="service_mode", success=success)
