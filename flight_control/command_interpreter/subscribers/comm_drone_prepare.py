"""comm_drone_prepare.py

Communication subscriber dealing with DronePrepare

Copyright 2019 Cervi Robotics
"""
import logging
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError
from std_msgs import msg

from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommDronePrepare:
    def __init__(self, node: Node, executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/drone_prepare", self.handle,
            qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug(f"Processing DronePrepare message. Message: {message}")
        try:
            self.executor.create_task(
                self.state_machine_controller.drone_prepared)

            logger.info("DronePrepare performed CORRECTLY.")

        except MachineError:
            logger.error("DronePrepare FAILED.")
