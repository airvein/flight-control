"""px_control_status.py

PxControl subscriber dealing with status

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander.telemetry import Telemetry


class PxControlStatus:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.PxControlStatus, "/px_control/status", self.handle,
            qos_profile=10)

    def handle(self, message: msg.PxControlStatus):
        self.telemetry.px_control_status = message
