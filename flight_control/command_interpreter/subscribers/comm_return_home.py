"""comm_return_home.py

Communication subscriber dealing with return_home command

Copyright 2019 Cervi Robotics
"""
import logging

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError
from std_msgs import msg

from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommReturnHome:
    def __init__(self, node: Node, executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/return_home", self.handle,
            qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug(f"Processing ReturnHome message. Message: {message}")
        try:
            self.state_machine_controller.return_home()

            success = True
            logger.info("ReturnHome performed CORRECTLY.")

        except MachineError:
            success = False
            logger.error("ReturnHome FAILED.")

        self.state_machine_controller.command_status.update(
            name="return_home", success=success)
