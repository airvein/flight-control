"""comm_cancel_flight.py

Communication subscriber dealing with `cancel_flight` command

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError
from std_msgs import msg

from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommFlightCancel:
    def __init__(self, node: Node, executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/flight_cancel", self.handle,
            qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug(f"Processing FlightCancel message. Message: {message}")
        try:
            self.state_machine_controller.flight_cancel()

            success = True
            logger.info("FlightCancel performed CORRECTLY.")
        except MachineError as e:
            success = False
            logger.error(f"FlightCancel FAILED. Reason: {e}")

        self.state_machine_controller.command_status.update(
            name="flight_cancel", success=success)
