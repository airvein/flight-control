"""comm_logs_upload_status.py

Communication subscriber that informs flight control module about progress
with uploading logs to cloud

Copyright 2020 Cervi Robotics
"""

from rclpy.node import Node

from flight_control.commander.telemetry import Telemetry
from drone_types import msg


class CommunicationLogsUploadStatus:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.CommLogsUploadStatus, "/communication/logs_upload_status",
            self.handle, qos_profile=10)

    def handle(self, message: msg.CommLogsUploadStatus):
        self.telemetry.logs_upload_status.upload_status = message.upload_status
