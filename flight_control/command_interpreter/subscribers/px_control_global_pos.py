"""px_control_global_pos.py

PxControl subscriber dealing with GlobalPos

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander import Telemetry


class PxControlGlobalPos:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.PxControlGlobalPos, "/px_control/global_pos", self.handle,
            qos_profile=10)

    def handle(self, message: msg.PxControlGlobalPos):
        self.telemetry.global_pos = message
