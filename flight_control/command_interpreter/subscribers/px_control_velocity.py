"""px_control_velocity.py

PxControl subscriber dealing with Velocity

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander.telemetry import Telemetry, Velocity


class PxControlVelocity:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.PxControlVelocity, "/px_control/velocity", self.handle,
            qos_profile=10)

    def handle(self, message: msg.PxControlVelocity):
        self.telemetry.velocity = Velocity(
            message.timestamp, message.vx, message.vy, message.vz)
