"""comm_finish.py

Communication subscriber dealing with Finish

Copyright 2019 Cervi Robotics
"""
import logging

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError
from std_msgs import msg

from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class CommFinish:
    def __init__(self, node: Node, executor: SingleThreadedExecutor,
                 state_machine_controller: StateMachineController):
        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self.node.create_subscription(
            msg.Bool, "/communication/finish", self.handle, qos_profile=10)

    def handle(self, message: msg.Bool):
        logger.debug(f"Processing Finish message. Message: {message}")
        try:
            self.executor.create_task(
                self.state_machine_controller.flight_finish)

            logger.info("Finish performed CORRECTLY.")

        except MachineError:
            logger.error(f"Finish FAILED.")
