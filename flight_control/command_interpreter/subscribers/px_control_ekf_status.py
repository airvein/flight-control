"""px_control_ekf_status.py

PxControl subscriber dealing with EKF Status

Copyright 2019 Cervi Robotics
"""

from rclpy.node import Node

from drone_types import msg
from flight_control.commander import Telemetry


class PxControlEKFStatus:
    def __init__(self, node: Node, telemetry: Telemetry):
        self.node = node
        self.telemetry = telemetry

        self.node.create_subscription(
            msg.PxControlEKFStatus, "/px_control/ekf_status", self.handle,
            qos_profile=10)

    def handle(self, message: msg.PxControlEKFStatus):
        self.telemetry.ekf_status = message
