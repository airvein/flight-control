"""services_manager.py

Services Manager which manage ROS services

Copyright 2019 Cervi Robotics
"""
import logging

from rclpy.executors import Executor
from rclpy.node import Node

from flight_control.command_interpreter.services import get_command_status
from flight_control.commander.state_machine_controller import \
    StateMachineController

logger = logging.getLogger(__name__)


class ServicesManager:
    def __init__(self, node: Node, executor: Executor,
                 state_machine_controller: StateMachineController):
        logger.debug("Initializing ServicesManager")

        self.node = node
        self.executor = executor
        self.state_machine_controller = state_machine_controller

        self._services = {
            "command_status": get_command_status.GetCommandStatus(
                node, executor, state_machine_controller)
        }

    @property
    def services(self) -> dict:
        return self._services

    def _is_service_registered(self, service_name: str):
        return service_name in self._services.keys()
