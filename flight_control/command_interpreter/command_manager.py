"""command_manager.py

Class which contains commands that should be executed by other modules that
are connected with FlightControl module. This class has been created to
handle commands, that are going to be send over ROS publish/subscribe
interface.

Copyright 2020 Cervi Robotics
"""
import logging
import json
from typing import Optional, Dict, Callable, Any

from rclpy.node import Node
import std_msgs.msg as std_msg

from flight_control.command_interpreter._constants import AlertCodes
from flight_control.command_interpreter.publishers import Publishers
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.common.utils import get_timestamp
from flight_control.navigator.navigator import Navigator
from drone_types import msg

logger = logging.getLogger(__name__)


class CommandManager:
    def __init__(self, node: Node,
                 publishers: Publishers,
                 state_machine_controller: StateMachineController,
                 navigator: Navigator):
        logger.debug("Initializing CommandManager")

        self._node = node
        self.publishers = publishers
        self.state_machine_controller = state_machine_controller
        self.navigator = navigator

        self._commands: Dict[str, Callable] = {
            "send_device_state": self._send_device_state,
            "send_drone_state": self._send_drone_state,
            "send_drone_stream": self._send_drone_stream,
            "send_route_progress": self._send_route_progress,
            "send_drone_status": self._send_drone_status,
            "send_heartbeat": self._send_heartbeat,
            "set_mqtt_client_state": self._set_mqtt_client_state,
            "send_mission_data": self._send_mission_data,
            "start_mission": self._start_mission,
            "wait": self._wait,
            "continue": self._continue,
            "get_route": self._get_route,
            "land_now": self._land_now,
            "kill": self._kill,
            "trigger_logs_upload": self._trigger_logs_upload,
            "send_route_parse_error_alert": self._send_route_parse_error_alert,

        }

    def is_command_available(self, command: str) -> bool:
        return command in self._commands

    def execute(self, command: str, *args, **kwargs) -> Any:
        if self.is_command_available(command):
            return self._commands[command](*args, **kwargs)
        else:
            logger.error(f"Requested command not found. Command name: {command}")

    # region COMMANDS

    def _send_mission_data(self, mission: str):
        logger.debug('Executing: send_mission_data')
        mission_data = json.dumps(self.navigator.prepare_mission(mission))

        message = std_msg.String(data=mission_data)

        if mission in self.publishers.route_publishers:
            self.publishers.route_publishers[mission].publish(message)

    def _send_drone_state(self, state: str):
        message = msg.FlightControlSendDroneState(timestamp=get_timestamp(),
                                                  state=state)

        logger.debug(f'Publishing DroneState message...: state={message.state}')

        self.publishers.flight_control_drone_state.publish(message)

    def _send_drone_stream(self, stream_type: str):
        message = msg.FlightControlSendDroneStream(timestamp=get_timestamp(),
                                                   stream_type=stream_type)

        logger.debug('Publishing DroneStream message...: '
                     f'stream_type={message.stream_type}')

        self.publishers.flight_control_drone_stream.publish(message)

    def _send_device_state(self, new_state: str,
                           old_state: Optional[str] = None):
        if old_state is None:
            old_state = self.state_machine_controller.current_device_state

        message = msg.FlightControlSendDeviceState(timestamp=get_timestamp(),
                                                   old_state=old_state,
                                                   new_state=new_state)

        logger.debug(
            'Publishing DeviceState message...: '
            f'old_state={message.old_state} | new_state={message.new_state}')

        self.state_machine_controller.current_device_state = new_state
        self.publishers.flight_control_device_state.publish(message)

    def _send_route_progress(self):
        message = msg.FlightControlProgress(
            timestamp=get_timestamp(),
            next_point=self.navigator.progress.next_point_id,
            next_point_route_id=self.navigator.progress.next_point_route_id,
            progress=self.navigator.progress.progress
        )

        self.publishers.flight_control_progress.publish(message)

    def _send_drone_status(self,
                           previous_status: Optional[
                               msg.FlightControlStatus] = None,
                           current_flag_state: bool = False
                           ) -> msg.FlightControlStatus:
        message = msg.FlightControlStatus(
            timestamp=get_timestamp(),
            flight_state=self.state_machine_controller.state,
            waiting=current_flag_state
        )
        if previous_status is not None:
            if (previous_status.flight_state == message.flight_state
                    and previous_status.waiting == message.waiting):
                return previous_status

        logger.debug(
            'Publishing DroneStatus ROS message...: '
            f'flight_state={message.flight_state} | waiting={message.waiting}')
        self.publishers.flight_control_status.publish(message)
        return message

    def _set_mqtt_client_state(self, connected: bool):
        message = std_msg.Bool(data=connected)

        self.publishers.flight_control_hangar_connection.publish(message)

    def _get_route(self, route_id: str):
        message = msg.FlightControlRequestRoute(timestamp=get_timestamp(),
                                                route_id=route_id)

        self.publishers.flight_control_request_route.publish(message)

    def _send_heartbeat(self):
        message = std_msg.UInt64(data=get_timestamp())

        self.publishers.flight_control_heartbeat.publish(message)

    def _start_mission(self):
        message = std_msg.Empty()

        self.publishers.flight_control_start_mission.publish(message)

    def _wait(self):
        message = std_msg.Empty()

        self.publishers.flight_control_wait.publish(message)

    def _continue(self):
        message = std_msg.Empty()

        self.publishers.flight_control_continue.publish(message)

    def _land_now(self):
        message = std_msg.Empty()

        self.publishers.flight_control_land_now.publish(message)

    def _kill(self):
        message = std_msg.Empty()

        self.publishers.flight_control_emergency_kill.publish(message)

    def _trigger_logs_upload(self):
        message = std_msg.Empty()

        self.publishers.flight_control_trigger_log_upload.publish(message)

    def _send_route_parse_error_alert(self):
        alert = AlertCodes.ROUTE_PARSE_ERROR
        logger.warning(
            f'Publishing Alert: {alert.name} with code: {alert.value}'
        )
        message = msg.SendAlert(
            timestamp=get_timestamp(), code=alert.value
        )
        self.publishers.flight_control_send_alert.publish(message)

    # endregion
