"""publishers.py

ROS Publishers for internal communication with other ROS modules in drone
in module Flight Control

Copyright 2019 Cervi Robotics
"""
import logging

from std_msgs import msg as std_msg
from rclpy.node import Node

from flight_control.command_interpreter import MissionTypes
from flight_control.settings import PUBLISHERS_QOS_PROFILE
from drone_types import msg

logger = logging.getLogger(__name__)


class Publishers:
    def __init__(self, node: Node):
        logger.debug("Initializing Publishers")

        self.node = node

        # region DATA STREAMS
        self.flight_control_drone_state = self.node.create_publisher(
            msg.FlightControlSendDroneState, "/flight_control/drone_state",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_drone_stream = self.node.create_publisher(
            msg.FlightControlSendDroneStream, "/flight_control/drone_stream",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_device_state = self.node.create_publisher(
            msg.FlightControlSendDeviceState, "/flight_control/device_state",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_status = self.node.create_publisher(
            msg.FlightControlStatus, "/flight_control/status",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_progress = self.node.create_publisher(
            msg.FlightControlProgress, "/flight_control/progress",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )

        self.flight_control_heartbeat = self.node.create_publisher(
            std_msg.UInt64, "/flight_control/heartbeat",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )

        self.flight_control_send_alert = self.node.create_publisher(
            msg.SendAlert, "/send_alert",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        # endregion

        # region COMMAND PUBLISHERS

        # communication
        self.flight_control_request_route = self.node.create_publisher(
            msg.FlightControlRequestRoute, "/flight_control/request_route",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_hangar_connection = self.node.create_publisher(
            std_msg.Bool, "/flight_control/hangar_connection_control",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_trigger_log_upload = self.node.create_publisher(
            std_msg.Empty, "/flight_control/trigger_logs_upload",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        # px control
        self.flight_control_land_now = self.node.create_publisher(
            std_msg.Empty, "/flight_control/land_now",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_emergency_kill = self.node.create_publisher(
            std_msg.Empty, "/flight_control/emergency_kill",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_wait = self.node.create_publisher(
            std_msg.Empty, "/flight_control/wait",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_continue = self.node.create_publisher(
            std_msg.Empty, "/flight_control/continue",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_start_mission = self.node.create_publisher(
            std_msg.Empty, "/flight_control/start_mission",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        # endregion

        # region NAVIGATOR ROUTE PUBLISHERS
        self.flight_control_route_normal = self.node.create_publisher(
            std_msg.String, "/flight_control/route_normal",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_route_emergency = self.node.create_publisher(
            std_msg.String, "/flight_control/route_emergency",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )
        self.flight_control_route_rth = self.node.create_publisher(
            std_msg.String, "/flight_control/route_rth",
            qos_profile=PUBLISHERS_QOS_PROFILE
        )

        self.route_publishers = {
            MissionTypes.NORMAL: self.flight_control_route_normal,
            MissionTypes.RTH: self.flight_control_route_rth,
            MissionTypes.EMERGENCY: self.flight_control_route_emergency
        }
        # endregion
