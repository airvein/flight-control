import os

PUBLISHERS_QOS_PROFILE: int = 10

MAX_WAITING_TIME: int = 5 * 60 * 1000  # milliseconds


class MaxEkfVariance:
    VELOCITY = 0.2
    POS_HORIZ = 0.2
    POS_VERT = 0.2
    COMPASS = 0.2


class Power:
    USE_MANUAL_SHUTDOWN = os.environ.get('FC_USE_MANUAL_SHUTDOWN') == '1'
    MANUAL_SHUTDOWN_PIN = 22

    SHUTDOWN_STREAM = os.environ.get('FC_SHUTDOWN_STREAM') == '1'
    STREAM_SHUTDOWN_PIN = 23

    NO_SHUTDOWN = os.environ.get('FC_NO_SHUTDOWN') == '1'


class LowBattery:
    LAND = 5.0  # percentage
    SHUTDOWN = 3.0  # percentage
