FROM git.cervirobotics.com:5005/airvein/tools/ros2-build:dashing

RUN mkdir -p /workspace/src

WORKDIR /workspace

COPY drone-types src/drone-types
COPY flight-control src/flight-control

ENV APTS=src/flight-control/apts.txt

RUN mkdir -p ~/.config/pip/ \
    && python3 -m pip install -r src/flight-control/requirements.txt \
    && apt-get update \
    && bash -c "xargs -a <(awk '! /^ *(#|$)/' \"$APTS\") -r -- apt-get install -y -q" \
    && rm -rf /var/lib/apt/lists/*

RUN /bin/bash -c "source /opt/ros/dashing/setup.bash && colcon build"
