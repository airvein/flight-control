import os
import logging
from datetime import datetime

import rclpy
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError

from flight_control import PACKAGE_NAME
from flight_control.command_interpreter.services_manager import ServicesManager
from flight_control.command_interpreter.subscribers_manager import SubscribersManager
from flight_control.command_interpreter.publishers import Publishers
from flight_control.commander.commander import Commander
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_manager import StatesManager
from flight_control.commander.telemetry import Telemetry
from flight_control.commander.state_data import get_state_data
from flight_control.navigator.navigator import Navigator
from flight_control.command_interpreter.command_manager import CommandManager
from flight_control.common.exceptions import MachineShutdown

logger = logging.getLogger(PACKAGE_NAME)


def initialize_flight_control(node: Node, executor):
    telemetry = Telemetry()
    state_data = get_state_data()
    navigator = Navigator(telemetry=telemetry)

    publishers = Publishers(node=node)

    state_machine = StateMachineController(state_data=state_data,
                                           executor=executor)

    subscribers = SubscribersManager(node=node,
                                     telemetry=telemetry,
                                     executor=executor,
                                     state_machine_controller=state_machine)

    services = ServicesManager(node=node,
                               executor=executor,
                               state_machine_controller=state_machine)

    command_manager = CommandManager(node=node,
                                     publishers=publishers,
                                     state_machine_controller=state_machine,
                                     navigator=navigator)

    states_manager = StatesManager(state_machine=state_machine,
                                   telemetry=telemetry,
                                   command_manager=command_manager)

    commander = Commander(node=node,
                          executor=executor,
                          states_manager=states_manager,
                          state_machine_controller=state_machine,
                          navigator=navigator,
                          publishers=publishers,
                          command_manager=command_manager)

    return node, executor


def initialize_logger():
    log_dir = os.environ.get("AIRVEIN_LOGS_PATH") or \
              os.path.join(os.path.expanduser("~"), "airvein_ws", "flight_logs")
    date = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    log_filename = f"{PACKAGE_NAME}-{date}.log"
    log_path = os.path.join(log_dir, log_filename)

    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s [%(levelname)s] %(name)s: %(message)s",
        handlers=[
            logging.FileHandler(log_path),
            logging.StreamHandler()
        ]
    )
    logging.getLogger("transitions").setLevel(logging.WARNING)


def main(args=None):
    initialize_logger()
    logger.debug("Logging started")

    rclpy.init(args=args)

    node = rclpy.create_node(PACKAGE_NAME)

    executor = SingleThreadedExecutor()
    executor.add_node(node)

    node, executor = initialize_flight_control(node, executor)

    logger.info("Flight Control module initialized")

    try:
        while rclpy.ok():
            try:
                rclpy.spin_once(node, executor=executor)
            except MachineError as e:
                logger.error(str(e))
            except MachineShutdown:
                break
    finally:
        executor.shutdown()
        rclpy.shutdown()

    logger.info("Flight Control module shut down")


if __name__ == '__main__':
    main()
