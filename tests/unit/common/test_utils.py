"""test_utils.py

Tests for utility functions

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import patch

from flight_control.common import utils
from flight_control.route.point import Point


class TestUtils(unittest.TestCase):

    @patch('flight_control.common.utils.time.time', return_value=12345.1234567)
    def test_get_timestamp_should_return_timestamp_formatted_to_ms_when_called(
            self, mock_time):
        expected_output = int(mock_time() * 1000)

        output = utils.get_timestamp()

        self.assertEqual(expected_output, output)

    def test_fix_points_id_should_fix_ID_and_return_list_of_points_when_called(
            self):
        points = [Point(id=i, latitude=i, longitude=i, altitude=i)
                  for i in range(5)]

        output = utils.fix_points_id(points, start_id=10)

        for point, out in zip(points, output):
            with self.subTest():
                self.assertEqual(point.id + 10, out.id)
                self.assertEqual(point.latitude, out.latitude)
                self.assertEqual(point.longitude, out.longitude)
                self.assertEqual(point.altitude, out.altitude)

    def test_fix_point_altitude_should_return_point_with_modified_altitude_when_called(self):
        point = Point(id=10, latitude=20, longitude=30, altitude=40)
        subtrahend = 15
        expected_output = Point(id=10, latitude=20,
                               longitude=30, altitude=40-subtrahend)

        output = utils.fix_point_altitude(point, subtrahend)

        self.assertEqual(expected_output, output)
