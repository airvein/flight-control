"""test_state.py

Tests for State class storing data of Navigator state

Copyright 2019 Cervi Robotics
"""
import unittest

from flight_control.navigator.state import (State, NavigatorState,
                                            EmergencyCruiseStateError,
                                            StateSetError, EmergencySubState)


class TestsState(unittest.TestCase):
    def setUp(self) -> None:
        self.state = State()

    def test_state_should_have_required_values_when_initialized(self):
        self.assertEqual(NavigatorState.CRUISE, self.state._state)
        self.assertEqual(0, self.state._value)

    def test_state_should_reprint_correct_property_when_called(self):
        self.assertEqual('<State.CRUISE: 0>', str(self.state))

    def test_state_should_return_value_when_called_value_property(self):
        self.assertEqual(0, self.state.value)

    def test_state_should_return_state_when_called_name_property(self):
        self.assertEqual('CRUISE', self.state.name)

    def test_state_should_return_state_when_called_state_property(self):
        self.assertEqual(NavigatorState.CRUISE, self.state.state)

    def test_set_state_should_set_state_to_CRUISE_when_called_with_correct_arguments(self):
        self.state.set_state(NavigatorState.CRUISE)

        self.assertEqual(NavigatorState.CRUISE, self.state._state)

    def test_set_state_should_set_state_to_RETRUN_HOME_when_called_with_correct_arguments(self):
        self.state.set_state(NavigatorState.RETURN_HOME)

        self.assertEqual(NavigatorState.RETURN_HOME, self.state._state)

    def test_set_state_should_set_state_to_EMERGENCY_CRUISE_when_called_with_correct_arguments(self):
        self.state.set_state(EmergencySubState.EMERGENCY_CRUISE)

        self.assertEqual(EmergencySubState.EMERGENCY_CRUISE, self.state._state)

    def test_set_state_should_set_state_to_EMERGENCY_RETURN_HOME_when_called_with_correct_arguments(self):
        self.state.set_state(EmergencySubState.EMERGENCY_RETURN_HOME)

        self.assertEqual(EmergencySubState.EMERGENCY_RETURN_HOME, self.state._state)

    def test_set_state_should_set_state_to_EMERGENCY_ROUTE_when_called_with_correct_arguments(self):
        self.state.set_state(EmergencySubState.EMERGENCY_ROUTE)

        self.assertEqual(EmergencySubState.EMERGENCY_ROUTE, self.state._state)

    def test_set_state_should_raise_error_when_there_IS_NOT_state_declared(self):
        expected_call = 'To update State state argument must be declared as first positional argument. Error: tuple index out of range'

        with self.assertRaises(StateSetError) as error:
            self.state.set_state()

        self.assertEqual(expected_call, str(error.exception))

    def test_set_emergency_route_value_should_raise_error_when_there_IS_NOT_value(self):
        state = EmergencySubState.EMERGENCY_ROUTE
        expected_call = 'In Emergency Route State value as second argument must be defined. Error: tuple index out of range'

        with self.assertRaises(EmergencyCruiseStateError) as error:
            self.state._set_emergency_cruise_value(state)

        self.assertEqual(expected_call, str(error.exception))

    def test_set_emergency_route_value_should_raise_error_when_value_is_NOT_integer(self):
        state = EmergencySubState.EMERGENCY_ROUTE
        value = None
        expected_call = 'In Emergency Route State value as second argument must be integer. Error: tuple index out of range'

        with self.assertRaises(EmergencyCruiseStateError) as error:
            self.state._set_emergency_cruise_value(state, value)

        self.assertEqual(expected_call, str(error.exception))

    def test_set_emergency_route_value_should_raise_error_when_value_is_NOT_in_range_1_to_n(self):
        state = EmergencySubState.EMERGENCY_ROUTE
        value = -1
        expected_call = 'In Emergency Route State value as second argument must be in range [1...n]. Error: tuple index out of range'

        with self.assertRaises(EmergencyCruiseStateError) as error:
            self.state._set_emergency_cruise_value(state, value)

        self.assertEqual(expected_call, str(error.exception))

    def test_set_emergency_route_value_should_set_value_when_all_OK(self):
        state = EmergencySubState.EMERGENCY_ROUTE
        value = 5

        self.state._set_emergency_cruise_value(state, value)

        self.assertEqual(5, self.state._value)

    def test_update_should_raise_StateSetError_when_no_arguments(self):
        expected_call = 'To update State state argument must be declared as first positional argument. Error: tuple index out of range'

        with self.assertRaises(StateSetError) as error:
            self.state._update()

        self.assertEqual(expected_call, str(error.exception))

    def test_update_should_perform_correctly_when_normal_cruise(self):
        self.state._update(NavigatorState.CRUISE)

        self.assertEqual(NavigatorState.CRUISE, self.state._state)
        self.assertEqual(0, self.state._value)

    def test_update_should_set_EMERGENCY_ROUTE_with_5_when_called_as_emergency(self):
        self.state._update(EmergencySubState.EMERGENCY_ROUTE, 5)

        self.assertEqual(EmergencySubState.EMERGENCY_ROUTE, self.state._state)
        self.assertEqual(5, self.state._value)

    def test_update_should_set_RETURN_HOME_with_minus_1_when_called_return_home(self):
        self.state._update(NavigatorState.RETURN_HOME)

        self.assertEqual(NavigatorState.RETURN_HOME, self.state._state)
        self.assertEqual(-1, self.state._value)


if __name__ == '__main__':
    unittest.main()
