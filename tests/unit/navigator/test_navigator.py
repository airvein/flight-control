"""test_navigator.py

Tests for Navigator class which manage points send to target position

Copyright 2019 Cervi Robotics
"""
import unittest
from unittest.mock import Mock, MagicMock, patch, PropertyMock

from flight_control.command_interpreter import MissionTypes
from flight_control.commander.telemetry import Telemetry
from flight_control.navigator.mission_manager import MissionManager
from flight_control.navigator.navigator import Navigator
from flight_control.navigator.progress import AbsoluteDistanceError
from flight_control.navigator.state import NavigatorState
from flight_control.route.point import Point
from tests.unit.fixtures.route_data import RouteData


class TestNavigator(unittest.TestCase):
    def setUp(self) -> None:
        patcher_logger = patch('flight_control.navigator.navigator.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.telemetry = Telemetry()
        self.telemetry.route = RouteData()

        self.navigator = Navigator(self.telemetry)

    def test_wait_should_change_waiting_to_True_when_called(self):
        self.navigator._waiting = False

        self.navigator.wait()

        self.assertTrue(self.navigator.waiting)

    def test_continue_flight_should_change_waiting_to_False_when_called(self):
        self.navigator._waiting = True

        self.navigator.continue_flight()

        self.assertFalse(self.navigator.waiting)

    def test_update_should_calculate_progress_and_check_for_point_completion(self):
        self.navigator.progress.calculate_progress = Mock()
        self.navigator._evaluate_target_point = Mock()

        self.navigator.update()

        self.navigator.progress.calculate_progress.assert_called_once()
        self.navigator._evaluate_target_point.assert_called_once()

    def test_update_should_log_warning_on_AbsoluteDistanceError(self):
        self.navigator.progress.calculate_progress = Mock(
            side_effect=[AbsoluteDistanceError])
        self.navigator._evaluate_target_point = Mock()
        self.navigator._update_telemetry_points = Mock()

        self.navigator.update()

        self.mock_logger.warning.assert_called()
        self.navigator._update_telemetry_points.assert_called_once()

    def test_prepare_mission_should_call_mission_manager_when_called(self):
        self.telemetry.emergency_route_id = 0

        cases = (MissionTypes.NORMAL,
                 MissionTypes.RTH,
                 MissionTypes.EMERGENCY)

        for case in cases:
            self.navigator.mission_manager = MagicMock(spec=MissionManager)
            self.navigator._update_mission_points = Mock()
            self.navigator.state.set_state = Mock()
            with self.subTest():
                self.navigator.prepare_mission(case)

                self.navigator.mission_manager.prepare_mission_data.assert_called_with(
                    case)
                self.navigator._update_mission_points.assert_called_with(case)
                self.navigator.state.set_state.assert_called_with(
                    self.navigator.mission_state_map[case])

    def test_evaluate_target_point_should_not_change_points_when_specific_conditions_occur(self):
        cases = [
            {"position_achieved": False, "route_done": False},
            {"position_achieved": True, "route_done": True},
            {"position_achieved": False, "route_done": True},
        ]

        self.telemetry.previous_point = self.telemetry.route.start_point
        self.telemetry.next_point = self.telemetry.route.route_points[0]

        for case in cases:
            with self.subTest():
                self.navigator.progress.target_point_achieved = case[
                    "position_achieved"]
                self.telemetry.route_done = case["route_done"]

                self.navigator._evaluate_target_point()

                self.assertEqual(self.telemetry.previous_point,
                                 self.telemetry.route.start_point)
                self.assertEqual(self.telemetry.next_point,
                                 self.telemetry.route.route_points[0])

    def test_evaluate_target_point_should_change_points_when_specific_condition_occur(self):
        self.navigator.progress.target_point_achieved = True
        self.navigator._is_route_done = Mock(return_value=False)
        self.telemetry.route_done = False

        previous_point = Point(id=5, latitude=20.0, longitude=20.0, altitude=5)
        next_point = Point(id=6, latitude=40.0, longitude=40.0, altitude=10.0)
        next_point_rth = previous_point

        next_point_id = next_point.id

        cases = [
            {"state": NavigatorState.RETURN_HOME, "next_id": next_point_id - 1,
             "return": next_point_rth},
            {"state": NavigatorState.CRUISE, "next_id": next_point_id + 1,
             "return": next_point},
            {"state": NavigatorState.EMERGENCY_CRUISE, "next_id": next_point_id + 1,
             "return": next_point},
        ]

        for case in cases:
            with self.subTest():
                self.navigator._get_next_route_point = Mock(
                    return_value=case["return"])
                self.telemetry.previous_point = previous_point
                self.telemetry.next_point = next_point
                self.navigator.state.set_state(case["state"])

                self.navigator._evaluate_target_point()

                self.assertEqual(self.telemetry.previous_point, next_point)
                self.navigator._get_next_route_point.assert_called()
                self.assertEqual(self.telemetry.next_point, case["return"])

    def test_get_landing_point_should_return_route_end_point_with_fixed_altitude_when_called(
            self):
        self.telemetry.emergency_end_point = \
            self.telemetry.route.emergency_routes[0].route_points[-1]
        self.telemetry.route.start_point = Point(id=0, latitude=0, longitude=0,
                                                 altitude=1234)

        cases = [
            {"state": NavigatorState.CRUISE,
             "end_point": self.telemetry.route.end_point},
            {"state": NavigatorState.EMERGENCY_CRUISE,
             "end_point": self.telemetry.emergency_end_point},
            {"state": NavigatorState.RETURN_HOME,
             "end_point": self.telemetry.route.start_point}
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.navigator.state.set_state(case["state"])

                output = self.navigator._get_landing_point()

                self.assertEqual(output.id, case["end_point"].id)
                self.assertEqual(output.longitude, case["end_point"].longitude)
                self.assertEqual(output.latitude, case["end_point"].latitude)

                if case["state"] == NavigatorState.EMERGENCY_CRUISE:
                    self.assertEqual(output.altitude,
                                     case["end_point"].altitude)
                else:
                    expected_value = (case["end_point"].altitude
                                      - self.telemetry.route.start_point.altitude)

                    self.assertEqual(output.altitude, expected_value)

    def test_update_mission_points_should_get_points_from_handler_and_set_them_in_telemetry_when_called(
            self):
        expected_previous_point = Point(id=2, longitude=2, latitude=2,
                                        altitude=2)
        expected_next_point = Point(id=3, longitude=3, latitude=3, altitude=3)

        mock_prepare_points_handler = MagicMock()
        mock_prepare_points_handler.__getitem__.return_value = Mock(
            return_value=(expected_previous_point, expected_next_point))

        self.navigator._prepare_points_handler = mock_prepare_points_handler

        self.navigator._update_mission_points("mission_type")

        mock_prepare_points_handler.__getitem__.assert_called_with(
            "mission_type")
        self.assertEqual(expected_previous_point,
                         self.navigator.telemetry.previous_point)
        self.assertEqual(expected_next_point,
                         self.navigator.telemetry.next_point)

    def test_prepare_points_normal_mission_should_return_proper_points_when_called(
            self):
        expected_previous_point = Point(
            id=0,
            latitude=self.telemetry.route.start_point.latitude,
            longitude=self.telemetry.route.start_point.longitude,
            altitude=0
        )
        expected_next_point = Point(id=1, latitude=1, longitude=1, altitude=1)
        mock_actual_points = MagicMock()
        mock_actual_points.__next__.return_value = expected_next_point
        self.telemetry.actual_route_points = mock_actual_points

        output = self.navigator._prepare_points_normal_mission()

        self.assertEqual(expected_previous_point, output[0])
        self.assertEqual(expected_next_point, output[1])

    def test_prepare_points_return_home_mission_should_return_proper_points_when_called(
            self):
        self.telemetry.next_point = Point(id=7, latitude=7, longitude=7,
                                          altitude=7)
        expected_previous_point = self.telemetry.next_point
        expected_next_point = Point(id=6, latitude=6, longitude=6, altitude=6)
        mock_actual_points = MagicMock()
        mock_actual_points.__next__.return_value = expected_next_point
        self.telemetry.actual_route_points = mock_actual_points

        output = self.navigator._prepare_points_return_home_mission()

        self.assertEqual(expected_previous_point, output[0])
        self.assertEqual(expected_next_point, output[1])

    def test_prepare_points_emergency_mission_should_reverse_actual_points_when_called_from_ahead_emergency_point(
            self):
        self.telemetry.emergency_route_id = 5
        previous_point = Point(id=6, latitude=6, longitude=6, altitude=6)
        next_point = Point(id=7, latitude=7, longitude=7, altitude=7)
        expected_next_point = Point(id=5, latitude=5, longitude=5, altitude=5)
        self.telemetry.previous_point = previous_point
        self.telemetry.next_point = next_point
        mock_actual_points = MagicMock()
        mock_actual_points.__next__.return_value = expected_next_point
        self.telemetry.actual_route_points = mock_actual_points

        output = self.navigator._prepare_points_emergency_mission()

        self.assertEqual(output[0], next_point)
        self.assertEqual(output[1], expected_next_point)

    def test_prepare_points_emergency_mission_should_update_actual_points_depending_on_drone_position(
            self):
        cases = [
            {"comment": "drone behind emergency point, moving forward",
             "previous_point": Mock(spec=Point, id=2),
             "next_point": Mock(spec=Point, id=3),
             "expected_previous_point": Mock(spec=Point, id=2),
             "expected_next_point": Mock(spec=Point, id=3),
             "emergency_route_id": 5},
            {"comment": "drone behind emergency point, moving backward",
             "previous_point": Mock(spec=Point, id=3),
             "next_point": Mock(spec=Point, id=2),
             "expected_previous_point": Mock(spec=Point, id=2),
             "expected_next_point": Mock(spec=Point, id=3),
             "emergency_route_id": 5},
            {"comment": "drone ahead emergency point, moving forward",
             "previous_point": Mock(spec=Point, id=10),
             "next_point": Mock(spec=Point, id=11),
             "expected_previous_point": Mock(spec=Point, id=11),
             "expected_next_point": Mock(spec=Point, id=10),
             "emergency_route_id": 5},
            {"comment": "drone ahead emergency point, moving backward",
             "previous_point": Mock(spec=Point, id=11),
             "next_point": Mock(spec=Point, id=10),
             "expected_previous_point": Mock(spec=Point, id=11),
             "expected_next_point": Mock(spec=Point, id=10),
             "emergency_route_id": 5},
        ]

        for case in cases:
            with self.subTest(msg=case["comment"]):
                self.telemetry.emergency_route_id = case["emergency_route_id"]
                self.telemetry.previous_point = case["previous_point"]
                self.telemetry.next_point = case["next_point"]
                mock_actual_points = MagicMock()
                mock_actual_points.__next__.return_value = case[
                    "expected_next_point"]
                self.telemetry.actual_route_points = mock_actual_points

                output = self.navigator._prepare_points_emergency_mission()

                self.assertEqual(output[0].id,
                                 case["expected_previous_point"].id)
                self.assertEqual(output[1].id, case["expected_next_point"].id)

    def test_get_next_route_point_should_return_next_point_from_route_when_it_is_not_last_point(
            self):
        self.telemetry.actual_route_points = (p for p in
                                              self.telemetry.route.route_points)
        next_route_point = self.telemetry.route.route_points[0]

        output = self.navigator._get_next_route_point()

        self.assertEqual(output, next_route_point)

    def test_get_next_route_point_should_return_route_end_point_when_next_target_point_is_end_point(
            self):
        self.navigator._get_landing_point = Mock(return_value=Mock(id=24))

        with patch.object(Navigator, '_next_target_point_id',
                          new_callable=PropertyMock) \
                as mock_next_point_id:
            mock_next_point_id.return_value = 24

            output = self.navigator._get_next_route_point()

        self.assertEqual(output,
                         self.navigator._get_landing_point.return_value)

    def test_is_route_done_should_return_bool_output_depend_on_progress(self):
        self.telemetry.emergency_end_point = \
            self.telemetry.route.emergency_routes[0].route_points[-1]

        cases = [
            {"state": NavigatorState.CRUISE,
             "end_point": self.telemetry.route.end_point,
             "next_point_id": self.telemetry.route.end_point.id,
             "output": True},
            {"state": NavigatorState.CRUISE,
             "end_point": self.telemetry.route.end_point,
             "next_point_id": self.telemetry.route.end_point.id - 1,
             "output": False},
            {"state": NavigatorState.RETURN_HOME,
             "end_point": self.telemetry.route.start_point,
             "next_point_id": self.telemetry.route.start_point.id,
             "output": True},
            {"state": NavigatorState.RETURN_HOME,
             "end_point": self.telemetry.route.start_point,
             "next_point_id": self.telemetry.route.start_point.id + 1,
             "output": False},
            {"state": NavigatorState.EMERGENCY_CRUISE, "end_point": self.telemetry.emergency_end_point,
             "next_point_id": self.telemetry.emergency_end_point.id, "output": True},
            {"state": NavigatorState.EMERGENCY_CRUISE, "end_point": self.telemetry.emergency_end_point,
             "next_point_id": self.telemetry.emergency_end_point.id - 1, "output": False},
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.telemetry.next_point = Mock(spec=Point)
                self.telemetry.next_point.id = case['next_point_id']
                self.navigator.state.set_state(case['state'])

                output = self.navigator._is_route_done()

                self.assertEqual(output, case['output'])

    def test_next_target_point_id_should_return_proper_id_depending_on_drone_track(
            self):
        cases = [
            {"previous_point": Point(id=4, latitude=0, longitude=0,
                                     altitude=0),
             "next_point": Point(id=5, latitude=0, longitude=0, altitude=0),
             "expected_output": 6},
            {"previous_point": Point(id=5, latitude=0, longitude=0,
                                     altitude=0),
             "next_point": Point(id=4, latitude=0, longitude=0, altitude=0),
             "expected_output": 3},
        ]

        for case in cases:
            self.telemetry.previous_point = case["previous_point"]
            self.telemetry.next_point = case["next_point"]

            with self.subTest():
                output = self.navigator._next_target_point_id

                self.assertEqual(case["expected_output"], output)
