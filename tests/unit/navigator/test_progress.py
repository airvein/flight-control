"""test_progress.py

Tests for Progress class calculating progress of waypoint while drone is flight

Copyright 2019 Cervi Robotics
"""
import math
import unittest
from unittest.mock import MagicMock, patch, call, Mock

from drone_types import msg

from flight_control.commander.telemetry import Telemetry
from flight_control.navigator.progress import Progress, AbsoluteDistanceError
from flight_control.navigator.state import State
from flight_control.route.point import Point
from tests.unit.fixtures.route_data import RouteData


class TestProgress(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.route = RouteData()
        self.telemetry.previous_point = Mock(spec=Point)
        self.telemetry.next_point = Mock(spec=Point)
        self.telemetry.next_point_id = 0
        self.state = State()

        self.progress = Progress(self.telemetry, self.state)

    def test_progress_should_have_correct_arguments_when_initialized(self):
        self.assertEqual(self.progress.next_point_id,
                         self.telemetry.next_point.id)
        self.assertEqual(self.progress.next_point_route_id, self.state.value)
        self.assertEqual(self.progress.progress, 0.0)

    def test_is_target_position_achieved_should_return_bool_value_depends_on_progress(
            self):
        self.progress.absolute_distance = 10

        cases = [{"dist_from_previous_point": 4,
                  'dist_to_next_point': 6,
                  "expected_output": False},
                 {"dist_from_previous_point": 9.6,
                  'dist_to_next_point': 0.4,
                  "expected_output": True},
                 {"dist_from_previous_point": 5,
                  "dist_to_next_point": 15,  # edge-case: drone got behind
                  "expected_output": False},  # previous-next points
                 {"dist_from_previous_point": 14,
                  "dist_to_next_point": 4,  # edge-case: drone got ahead
                  "expected_output": True},  # previous-next points
                 ]

        for case in cases:
            with self.subTest(msg=case):
                self.progress.distance_drone_from_previous_point = case[
                    'dist_from_previous_point']
                self.progress.distance_drone_to_next_point = case[
                    'dist_to_next_point']

                output = self.progress.is_target_position_achieved()

                self.assertEqual(case['expected_output'], output)

    def test_calculate_distance_haversine_should_return_value_when_two_points_are_given(
            self):
        """Measurements checked on:
        gps-coordiantes.org/distance-between-coordinates.php
        """
        previous_point = Point(id=5, latitude=20.0, longitude=20.0,
                               altitude=10.0)
        next_point = Point(id=6, latitude=40.0, longitude=40.0, altitude=10.0)

        output = self.progress._calculate_distance_haversine(previous_point,
                                                             next_point)
        output_reversed_points = self.progress._calculate_distance_haversine(
            next_point, previous_point)

        self.assertAlmostEqual(output, 2927385.19, places=2)

        self.assertEqual(output, output_reversed_points)

    def test_get_distance_should_return_value_when_next_points_are_given(self):
        previous_point = Point(id=5, latitude=20.0, longitude=20.0, altitude=5)
        next_point = Point(id=6, latitude=40.0, longitude=40.0, altitude=10.0)

        distance_2d = self.progress._calculate_distance_haversine(previous_point, next_point)
        altitude_diff = next_point.altitude - previous_point.altitude

        distance_3d = math.sqrt(distance_2d**2 + altitude_diff**2)

        output = self.progress._get_distance(previous_point, next_point)

        self.assertAlmostEqual(output, distance_3d)

    def test_get_distance_should_fix_altitude_when_called_with_proper_argument(self):
        previous_point = Point(id=5, latitude=20.0, longitude=20.0, altitude=5)
        next_point = Point(id=6, latitude=40.0, longitude=40.0, altitude=100)

        distance_2d = self.progress._calculate_distance_haversine(previous_point, next_point)

        output = self.progress._get_distance(previous_point, next_point,
                                             fix_relative_height=95)

        self.assertAlmostEqual(output, distance_2d)

    def test_get_progress_should_raise_AbsoluteDistanceError_when_absolute_distance_is_0(self):
        with self.assertRaises(AbsoluteDistanceError):
            self.progress._get_progress(distance_reached=123,
                                        absolute_distance=0)

    def test_calculate_progress_should_return_progress_0_5__when_called(self):
        self.telemetry.route.start_point = Point(id=0, latitude=20.0, longitude=20.0, altitude=5.0)
        self.telemetry.global_pos = msg.PxControlGlobalPos(latitude=20.0, longitude=20.0, altitude=5.0)

        self.progress.telemetry.previous_point = Point(id=5, latitude=20.0, longitude=10.0, altitude=10.0)
        self.progress.telemetry.next_point = Point(id=6, latitude=20.0, longitude=30.0, altitude=10.0)

        output = self.progress.calculate_progress()

        self.assertAlmostEqual(output, 0.50, places=2)

    def test_calculate_progress_should_return_progress_0_75__when_called(self):
        self.telemetry.route.start_point = Point(id=0, latitude=20.0, longitude=20.0, altitude=5.0)
        self.telemetry.global_pos = msg.PxControlGlobalPos(latitude=20.0, longitude=25.0, altitude=5.0)

        self.progress.telemetry.previous_point = Point(id=5, latitude=20.0, longitude=10.0, altitude=10.0)
        self.progress.telemetry.next_point = Point(id=6, latitude=20.0, longitude=30.0, altitude=10.0)

        output = self.progress.calculate_progress()

        self.assertAlmostEqual(output, 0.75, places=2)

    def test_calculate_progress_should_return_progress_0_75__when_landing(self):
        self.telemetry.route.start_point = Point(id=0, latitude=20.0, longitude=20.0, altitude=0.0)
        self.telemetry.global_pos = msg.PxControlGlobalPos(latitude=30.0, longitude=25.0, altitude=2.5)

        self.progress.telemetry.previous_point = Point(id=5, latitude=30.0, longitude=25.0, altitude=10.0)
        self.progress.telemetry.next_point = Point(id=6, latitude=30.0, longitude=25.0, altitude=0.0)

        output = self.progress.calculate_progress()
        self.assertAlmostEqual(output, 0.75, places=2)

    def test_calculate_progress_should_return_progress_0_99__when_landing(self):
        self.telemetry.route.start_point = Point(id=0, latitude=20.0, longitude=20.0, altitude=0.0)
        self.telemetry.global_pos = msg.PxControlGlobalPos(latitude=30.0, longitude=25.0, altitude=0.1)

        self.progress.telemetry.previous_point = Point(id=5, latitude=30.0, longitude=25.0, altitude=10.0)
        self.progress.telemetry.next_point = Point(id=6, latitude=30.0, longitude=25.0, altitude=0.0)

        output = self.progress.calculate_progress()

        self.assertAlmostEqual(output, 0.99, places=2)

    def test_update_should_set_all_values_of_progress_when_is_called(self):
        self.progress.is_target_position_achieved = Mock(return_value=True)
        self.telemetry.next_point = Point(id=4, latitude=10.0, longitude=110.0,
                                          altitude=-10.0)

        self.progress.state = MagicMock(autospec=State)
        self.progress.state.value = -1

        self.progress._update(progress=0.35)

        self.assertEqual(self.progress.next_point_id, 4)
        self.assertEqual(self.progress.next_point_route_id, -1)
        self.assertEqual(self.progress.progress, 0.35)
        self.assertEqual(
            self.progress.is_target_position_achieved.return_value,
            self.progress.target_point_achieved)

    @patch.object(Progress, "_get_distance", side_effect=[10, 4, 6])
    def test_update_points_distance_should_update_distance_from_previous_point_and_to_next_point_when_called(
            self, mock_get_distance):
        self.telemetry.global_pos = Mock(spec=msg.PxControlGlobalPos)

        expected_call_absolute_distance = call(self.telemetry.previous_point,
                                               self.telemetry.next_point)
        expected_call_distance_from_previous_point = call(
            self.telemetry.previous_point,
            self.telemetry.global_pos,
            fix_relative_height=self.telemetry.route.start_point.altitude)
        expected_call_distance_to_next_point = call(
            self.telemetry.next_point,
            self.telemetry.global_pos,
            fix_relative_height=self.telemetry.route.start_point.altitude)

        expected_calls = (expected_call_absolute_distance,
                          expected_call_distance_from_previous_point,
                          expected_call_distance_to_next_point)

        self.progress._update_points_distance()

        mock_get_distance.assert_has_calls(expected_calls)
        self.assertEqual(10, self.progress.absolute_distance)
        self.assertEqual(4, self.progress.distance_drone_from_previous_point)
        self.assertEqual(6, self.progress.distance_drone_to_next_point)
