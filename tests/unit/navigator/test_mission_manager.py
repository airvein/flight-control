import unittest
from unittest.mock import MagicMock, patch

from flight_control.command_interpreter import MissionTypes
from flight_control.commander import Telemetry
from flight_control.navigator.mission_preparation.abstract_mission import \
    AbstractMission
from flight_control.navigator.mission_preparation.normal_mission import \
    NormalMission
from flight_control.navigator.mission_preparation.rth_mission import RthMission
from flight_control.navigator.mission_preparation.emergency_mission import \
    EmergencyMission
from flight_control.navigator.mission_manager import MissionManager
from flight_control.route.point import Point


class TestMissionManager(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)

        self.mission_manager = MissionManager(self.telemetry)

    def test_class_should_create_mission_objects_when_initialized(self):
        self.assertIsInstance(self.mission_manager._normal_mission,
                              NormalMission)
        self.assertIsInstance(self.mission_manager._emergency_mission,
                              EmergencyMission)
        self.assertIsInstance(self.mission_manager._rth_mission,
                              RthMission)

    @patch.object(AbstractMission, 'get_mission_data')
    def test_prepare_mission_data_should_update_emergency_end_point_on_emergency_mission_with_last_route_point(
            self, mock_get_mission_data):
        mock_get_mission_data.return_value = {
            "route_points": [[1, 1.1, 1.2, 1.3],
                             [2, 2.1, 2.2, 2.3]],
            "zones": {}}
        expected_output = {"route_points": [[1, 1.1, 1.2, 1.3]],
                           "zones": {}}
        expected_emergency_end_point = Point(id=2, latitude=2.1, longitude=2.2,
                                             altitude=2.3)

        output = self.mission_manager.prepare_mission_data(
            MissionTypes.EMERGENCY)

        self.assertEqual(expected_emergency_end_point,
                         self.telemetry.emergency_end_point)
        self.assertDictEqual(expected_output, output)
