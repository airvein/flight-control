import unittest
from unittest.mock import MagicMock

from flight_control.commander import Telemetry
from flight_control.navigator.mission_preparation import NormalMission
from flight_control.navigator.mission_preparation.abstract_mission import \
    AbstractMission
from tests.unit.fixtures.route_data import RouteData


class TestNormalMission(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.route = RouteData()

        self.normal_mission = NormalMission(self.telemetry)

    def test_class_inherits_from_abstract_mission(self):
        self.assertTrue(issubclass(NormalMission, AbstractMission))

    def test_get_mission_route_points_should_return_sliced_list_of_points_when_called(
            self):
        expected_points = self.telemetry.route.route_points[1:-1]

        output = self.normal_mission._get_mission_route_points()

        self.assertListEqual(expected_points, output)

    def test_get_mission_zones_should_return_dict_with_zones_when_called(self):
        expected_dict = {
            "home_maneuver": self.telemetry.route.zones.home_maneuver,
            "home_approach": self.telemetry.route.zones.home_approach,
            "landing_approach": self.telemetry.route.zones.landing_approach,
            "landing_maneuver": self.telemetry.route.zones.landing_maneuver
        }

        output = self.normal_mission._get_mission_zones()

        self.assertDictEqual(expected_dict, output)

    def test_get_landing_yaw_should_return_end_yaw(self):
        actual_yaw = self.normal_mission._get_landing_yaw()

        self.assertEqual(37.21, actual_yaw)
