import unittest
from unittest.mock import MagicMock, patch, Mock

from flight_control.commander import Telemetry
from flight_control.navigator.mission_preparation import EmergencyMission
from flight_control.navigator.mission_preparation.abstract_mission import \
    AbstractMission
from flight_control.route.point import Point
from tests.unit.fixtures.route_data import RouteData


class TestEmergencyMission(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.route = RouteData()

        self.emergency_mission = EmergencyMission(self.telemetry)

    def test_class_inherits_from_abstract_mission(self):
        self.assertTrue(issubclass(EmergencyMission, AbstractMission))

    @patch(
        'flight_control.navigator.mission_preparation.emergency_mission.fix_points_id')
    @patch.object(EmergencyMission, '_get_emergency_route')
    @patch.object(EmergencyMission,
                  '_get_normal_route_points_to_emergency_point')
    def test_get_mission_route_points_should_return_proper_list_of_points(
            self, mock_get_normal_route_points, mock_get_emergency_route,
            mock_fix_points_id):
        mock_get_normal_route_points.return_value = [1, 2, 3]
        mock_get_emergency_route.return_value = [4, 5]
        self.telemetry.next_point = MagicMock(spec=Point, id=6)
        self.telemetry.emergency_route_id = 5

        expected_points = [1, 2, 3, 4, 5]
        mock_fix_points_id.return_value = expected_points

        output = self.emergency_mission._get_mission_route_points()

        self.assertListEqual(expected_points, output)
        mock_fix_points_id.assert_called_once_with(expected_points,
                                                   self.telemetry.next_point.id + 1)

    def test_get_mission_zones_should_return_empty_dict_when_called(self):
        expected_dict = {}

        output = self.emergency_mission._get_mission_zones()

        self.assertDictEqual(expected_dict, output)

    def test_get_emergency_route_should_return_route_points_for_proper_emergency_route_id(
            self):
        self.telemetry.emergency_route_id = 16
        expected_points = self.telemetry.route.emergency_routes[1].route_points

        output = self.emergency_mission._get_emergency_route(
            emergency_route_id=16)

        self.assertEqual(output, expected_points)

    def test_get_normal_route_points_to_emergency_point_should_return_proper_points_depending_on_case(
            self):
        cases = [
            {"comment": "drone behind emergency point, moving forward",
             "previous_point": Mock(spec=Point, id=10),
             "next_point": Mock(spec=Point, id=11),
             "emergency_route_id": 16,
             "expected_point_ids": [11, 12, 13, 14, 15]},
            {"comment": "drone behind emergency point, moving backward",
             "previous_point": Mock(spec=Point, id=11),
             "next_point": Mock(spec=Point, id=10),
             "emergency_route_id": 16,
             "expected_point_ids": [11, 12, 13, 14, 15]},
            {"comment": "drone ahead emergency point, moving forward",
             "previous_point": Mock(spec=Point, id=10),
             "next_point": Mock(spec=Point, id=11),
             "emergency_route_id": 5,
             "expected_point_ids": [10, 9, 8, 7, 6]},
            {"comment": "drone ahead emergency point, moving backward",
             "previous_point": Mock(spec=Point, id=11),
             "next_point": Mock(spec=Point, id=10),
             "emergency_route_id": 5,
             "expected_point_ids": [10, 9, 8, 7, 6]},
            {"comment": "drone next target point is emergency point",
             "previous_point": Mock(spec=Point, id=4),
             "next_point": Mock(spec=Point, id=5),
             "emergency_route_id": 5,
             "expected_point_ids": []},
            {"comment": "drone just passed emergency point",
             "previous_point": Mock(spec=Point, id=5),
             "next_point": Mock(spec=Point, id=6),
             "emergency_route_id": 5,
             "expected_point_ids": []},
        ]

        for case in cases:
            with self.subTest(msg=case["comment"]):
                self.telemetry.previous_point = case["previous_point"]
                self.telemetry.next_point = case["next_point"]
                self.telemetry.emergency_route_id = case["emergency_route_id"]

                output = self.emergency_mission._get_normal_route_points_to_emergency_point()
                output_ids = [p.id for p in output]

                self.assertListEqual(case["expected_point_ids"], output_ids)

    @patch.object(EmergencyMission, '_get_emergency_route')
    @patch.object(EmergencyMission,
                  '_get_normal_route_points_to_emergency_point')
    def test_get_mission_route_points_should_add_emergency_points_and_fix_points_id_when_called(
            self, mock_get_normal_route_points, mock_get_emergency_route):
        mock_get_emergency_route.side_effect = [
            [Point(id=i, latitude=0.0, longitude=0.0, altitude=0.0) for i in
             range(5)]]
        mock_get_normal_route_points.side_effect = [
            [Point(id=i, latitude=0.0, longitude=0.0, altitude=0.0) for i in
             range(12, 16)]]

        self.telemetry.previous_point = Point(id=10, latitude=0.0,
                                              longitude=0.0, altitude=0.0)
        self.telemetry.next_point = Point(id=11, latitude=0.0, longitude=0.0,
                                          altitude=0.0)
        self.telemetry.emergency_route_id = 16

        expected_route_point_ids = [12, 13, 14, 15, 16, 17, 18, 19, 20]

        output = self.emergency_mission._get_mission_route_points()
        output_ids = [p.id for p in output]

        self.assertListEqual(output_ids, expected_route_point_ids)

    def test_get_landing_yaw_should_return_0(self):
        actual_yaw = self.emergency_mission._get_landing_yaw()

        self.assertEqual(0.0, actual_yaw)
