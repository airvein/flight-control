import unittest
from unittest.mock import MagicMock

from flight_control.commander import Telemetry
from flight_control.navigator.mission_preparation import RthMission
from flight_control.navigator.mission_preparation.abstract_mission import \
    AbstractMission
from flight_control.route.point import Point
from tests.unit.fixtures.route_data import RouteData


class TestRthMission(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.route = RouteData()

        self.rth_mission = RthMission(self.telemetry)

    def test_class_inherits_from_abstract_mission(self):
        self.assertTrue(issubclass(RthMission, AbstractMission))

    def test_get_mission_route_points_should_return_proper_list_of_points(
            self):
        self.telemetry.next_point = MagicMock(spec=Point, id=6)
        next_id = self.telemetry.next_point.id
        expected_points = list(
            reversed(self.telemetry.route.route_points[1:next_id]))

        output = self.rth_mission._get_mission_route_points()

        self.assertListEqual(expected_points, output)

    def test_get_mission_zones_should_return_dict_with_zones_when_called(self):
        expected_dict = {
            "home_maneuver": self.telemetry.route.zones.home_maneuver,
            "home_approach": self.telemetry.route.zones.home_approach,
        }

        output = self.rth_mission._get_mission_zones()

        self.assertDictEqual(expected_dict, output)

    def test_get_landing_yaw_should_return_end_yaw(self):
        actual_yaw = self.rth_mission._get_landing_yaw()

        self.assertEqual(21.37, actual_yaw)
