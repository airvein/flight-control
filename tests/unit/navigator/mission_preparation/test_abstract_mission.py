import unittest
from unittest.mock import MagicMock, patch, Mock
from collections.abc import Iterable

from flight_control.commander import Telemetry
from flight_control.navigator.mission_preparation.abstract_mission import \
    AbstractMission
from flight_control.route.point import Point


class TestAbstractMission(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)

    def test_cannot_instantiate_abstract_class(self):
        with self.assertRaises(TypeError):
            AbstractMission(self.telemetry)

    @patch.multiple(AbstractMission, __abstractmethods__=set())
    def test_get_mission_data_should_return_dict_with_proper_structure(self):
        mock_point = MagicMock(spec=Point)

        abstract_mission = AbstractMission(self.telemetry)
        abstract_mission._get_mission_route_points = Mock(
            return_value=[mock_point(id=i) for i in range(5)])
        abstract_mission._get_mission_zones = Mock(return_value={})
        abstract_mission._get_landing_yaw = Mock(return_value=0.0)

        expected_output = {
            "route_points": [p.list() for p in
                             abstract_mission._get_mission_route_points()],
            "zones": abstract_mission._get_mission_zones(),
            "landing_yaw": abstract_mission._get_landing_yaw()}

        output = abstract_mission.get_mission_data()

        self.assertDictEqual(expected_output, output)
        self.assertIsInstance(self.telemetry.actual_route_points, Iterable)
