import pathlib
from typing import Text


def read_track_data() -> Text:
    track_path = pathlib.Path(__file__).parent.joinpath("track.json")
    with open(track_path) as f:
        data = f.read()
        return data
