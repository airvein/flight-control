import functools
from unittest.mock import MagicMock, call, patch, PropertyMock, Mock

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, UploadData
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase
from drone_types.msg import CommLogsUploadStatus


class TestUploadData(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestUploadData, self).setUp()

        self.upload_data_state = UploadData(self.mock_machine_controller,
                                            self.telemetry,
                                            self.mock_command_manager)
        self.mock_machine_controller.done_data_upload = MagicMock(spec=functools.partial)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(UploadData, AbstractStateHandler))

    @patch.object(UploadData, '_get_cloud_state_name', new_callable=PropertyMock)
    def test_handle_should_trigger_uploading_and_update_cloud_state_when_upload_status_is_waiting_for_trigger(
            self, mock_get_rth_flag):
        self.mock_machine_controller.wait_data_upload = Mock()
        self.telemetry.logs_upload_status = MagicMock(spec=CommLogsUploadStatus)
        self.telemetry.logs_upload_status.upload_status = CommLogsUploadStatus.WAITING_FOR_TRIGGER
        mock_get_rth_flag.return_value = None

        self.upload_data_state.handle()

        expected_command_manager_calls = [
            call("send_device_state", new_state=mock_get_rth_flag()),
            call("trigger_logs_upload")
        ]

        self.mock_command_manager.execute.assert_has_calls(
            expected_command_manager_calls)

        self.assertEqual(self.telemetry.logs_upload_status.upload_status,
                         CommLogsUploadStatus.IN_PROGRESS)

        self.mock_machine_controller.add_pending_transition.assert_called_with(
            self.mock_machine_controller.wait_data_upload
        )

    def test_handle_should_reenter_upload_data_state_when_logs_upload_status_is_in_progress(self):
        self.telemetry.logs_upload_status.upload_status = CommLogsUploadStatus.IN_PROGRESS
        self.mock_machine_controller.wait_data_upload = Mock()

        self.upload_data_state.handle()

        self.mock_command_manager.execute.assert_not_called()
        self.mock_machine_controller.add_pending_transition.assert_called_with(
            self.mock_machine_controller.wait_data_upload
        )

    def test_handle_should_trigger_done_data_upload_when_logs_upload_status_is_done(self):
        self.telemetry.logs_upload_status.upload_status = CommLogsUploadStatus.DONE
        self.mock_machine_controller.done_data_upload = Mock()

        self.upload_data_state.handle()

        self.mock_command_manager.execute.assert_not_called()
        self.mock_machine_controller.add_pending_transition.assert_called_with(
            self.mock_machine_controller.done_data_upload
        )

    def test_get_cloud_state_name_should_return_proper_state_depending_on_RTH_flag(self):
        cases = [
            {"RTH_flag": False, "expected_output": CloudStateNames.FLIGHT_ENDING},
            {"RTH_flag": True, "expected_output": CloudStateNames.rth(CloudStateNames.FLIGHT_ENDING)},
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.mock_machine_controller.state_data[StateDataNames.FLAGS][
                    StateDataNames.Flag.RTH] = case["RTH_flag"]

                self.assertEqual(case["expected_output"],
                                 self.upload_data_state._get_cloud_state_name)
