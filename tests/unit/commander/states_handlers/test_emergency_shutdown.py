from unittest.mock import patch, Mock

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import \
    EmergencyShutdown, Shutdown
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestEmergencyShutdown(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestEmergencyShutdown, self).setUp()

        patcher = patch(
            'flight_control.commander.states_handlers._shutdown.GPIO'
        )
        patcher.start()
        self.addCleanup(patcher.stop)

        self.emergency_shutdown_state = EmergencyShutdown(
            self.mock_machine_controller,
            self.telemetry, self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertIsInstance(self.emergency_shutdown_state, Shutdown)

    @patch.object(Shutdown, "shutdown_system")
    def test_handle_should_update_device_state_to_shutdown(self,
                                                           mock_shutdown_system):
        self.mock_command_manager.execute = Mock()
        self.emergency_shutdown_state.handle()

        self.mock_command_manager.execute.assert_called_with(
            "send_device_state", new_state=CloudStateNames.SHUTDOWN
        )

    @patch.object(Shutdown, "shutdown_system")
    def test_handle_should_call_shutdown_system(self, mock_shutdown_system):
        self.emergency_shutdown_state.handle()

        mock_shutdown_system.assert_called_once()

    @patch.object(Shutdown, "shutdown_system")
    def test_handle_should_set_stream_type_to_position(self, mock_shutdown_system):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.POSITION
        }

        self.emergency_shutdown_state.handle()

        self.assertSubsetInDict(
            expected_state_data, self.mock_machine_controller.state_data)
