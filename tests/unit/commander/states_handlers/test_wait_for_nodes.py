import functools
from unittest.mock import MagicMock

from drone_types.msg import PxControlStatus

from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    WaitForNodes
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestWaitForNodes(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super().setUp()

        self.wait_for_nodes = WaitForNodes(self.mock_machine_controller,
                                           self.telemetry,
                                           self.mock_command_manager)

        self.mock_machine_controller.nodes_ready = MagicMock(
            spec=functools.partial)
        self.mock_machine_controller.repeat_wait_for_nodes = MagicMock(
            spec=functools.partial)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(WaitForNodes, AbstractStateHandler))

    def test_handle_should_add_transition_nodes_ready_when_received_proper_status(
            self):
        self.telemetry.px_control_status.connection_state \
            = PxControlStatus.CONNECTION_STATE_CONNECTED

        self.wait_for_nodes.handle()

        self.mock_machine_controller.add_pending_transition.assert_called_with(
            self.mock_machine_controller.nodes_ready
        )

    def test_handle_should_add_transition_repeat_wait_for_nodes_when_status_is_not_ready(
            self):
        self.telemetry.px_control_status.connection_state \
            = PxControlStatus.CONNECTION_STATE_CONNECTING

        self.wait_for_nodes.handle()

        self.mock_machine_controller.add_pending_transition.assert_called_with(
            self.mock_machine_controller.repeat_wait_for_nodes
        )

    def test_handle_should_set_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.wait_for_nodes.handle()

        self.assertSubsetInDict(expected_state_data,
                                self.mock_machine_controller.state_data)
