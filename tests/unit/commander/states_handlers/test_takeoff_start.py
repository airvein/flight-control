from unittest.mock import Mock, call

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    TakeoffStart
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestTakeoffStart(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestTakeoffStart, self).setUp()

        self.takeoff_start_state = TakeoffStart(self.mock_machine_controller,
                                                self.telemetry,
                                                self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(TakeoffStart, AbstractStateHandler))

    def test_handle_should_execute_proper_commands_when_called(self):
        self.mock_command_manager.execute = Mock()

        self.takeoff_start_state.handle()

        calls = [
            call("start_mission"),
            call("send_device_state", new_state=CloudStateNames.TAKEOFF),
            call("set_mqtt_client_state", connected=False),
        ]

        self.mock_command_manager.execute.assert_has_calls(calls)

    def test_enables_navigator(self):
        self.takeoff_start_state.handle()

        self.assertTrue(
            self.mock_machine_controller.state_data[
                StateDataNames.FLAGS][StateDataNames.Flag.NAVIGATOR_ENABLE]
        )

    def test_sets_stream_type_to_in_flight(self):
        self.takeoff_start_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_FLIGHT,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )
