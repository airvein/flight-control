from unittest.mock import Mock, patch

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateNames, StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    GetRoute
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase
from drone_types import msg


class TestGetRoute(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestGetRoute, self).setUp()

        self.mock_machine_controller.state = StateNames.GET_ROUTE
        self.mock_machine_controller.state_data[StateDataNames.INIT_FLIGHT] = {
            'route_id': "2137", 'flight_id': '', 'cargo': False
        }

        self.get_route_state = GetRoute(self.mock_machine_controller,
                                        self.telemetry,
                                        self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(GetRoute, AbstractStateHandler))

    def test_handle_should_call_send_device_state_to_initializing_when_called(
            self):
        self.get_route_state.handle()

        self.mock_machine_controller.add_pending_task.assert_called_with(
            self.mock_command_manager.execute, "send_device_state",
            new_state=CloudStateNames.INITIALIZING)

    def test_handle_should_publish_request_route_message_when_called(
            self):
        self.get_route_state._command_manager.execute = Mock()
        route_id = self.mock_machine_controller.state_data[
            StateDataNames.INIT_FLIGHT]["route_id"]

        self.get_route_state.handle()

        self.get_route_state._command_manager.execute.assert_called_with(
            "get_route", route_id=route_id)

    def test_sets_stream_type_to_in_hangar(self):
        self.get_route_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_HANGAR,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )
