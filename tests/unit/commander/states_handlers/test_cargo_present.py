from flight_control.command_interpreter import DroneHangarMessageNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, CargoPresent
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestCargoPresent(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestCargoPresent, self).setUp()
        self.cargo_present_state = CargoPresent(self.mock_machine_controller,
                                                self.telemetry,
                                                self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(CargoPresent, AbstractStateHandler))

    def test_sends_cargo_ready_message_to_hangar(self):
        self.cargo_present_state.handle()

        self.mock_machine_controller.add_pending_task.assert_called_with(
            self.mock_command_manager.execute, "send_drone_state",
            state=DroneHangarMessageNames.CARGO_READY)

    def test_sets_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.cargo_present_state.handle()

        self.assertSubsetInDict(expected_state_data, self.mock_machine_controller.state_data)
