"""test_land_now_landed.py

Tests for "ln_landed" state handler

"""

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    LandNowLanded
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestLandNowLanded(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super().setUp()

        self.ln_landed = LandNowLanded(
            self.mock_machine_controller, self.telemetry,
            self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(LandNowLanded, AbstractStateHandler))

    def test_handle_should_set_stream_type_to_position(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.POSITION
        }

        self.ln_landed.handle()

        self.assertSubsetInDict(
            expected_state_data, self.mock_machine_controller.state_data)

    def test_handle_should_update_device_state_when_called(self):
        self.ln_landed.handle()

        self.ln_landed._state_machine.add_pending_task.assert_called_with(
            self.ln_landed._command_manager.execute, "send_device_state",
            new_state=CloudStateNames.LN_LANDED
        )

    def test_handle_should_set_navigator_flag_to_disable_when_called(self):
        self.ln_landed.handle()

        self.assertFalse(
            self.mock_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.NAVIGATOR_ENABLE])
