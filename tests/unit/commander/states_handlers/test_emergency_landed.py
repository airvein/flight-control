from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, EmergencyLanded
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestEmergencyLanded(BaseStateHandlerTestCase):

    def setUp(self) -> None:
        super(TestEmergencyLanded, self).setUp()

        self.emergency_landed_state = EmergencyLanded(
            self.mock_machine_controller,
            self.telemetry, self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(EmergencyLanded, AbstractStateHandler))

    def test_updates_device_state_to_emergency_landed(self):
        self.emergency_landed_state.handle()

        self.mock_command_manager.execute.assert_called_with(
            "send_device_state", new_state=CloudStateNames.EL_LANDED
        )

    def test_enable_navigator_is_disabled_in_state_data(self):
        self.mock_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.NAVIGATOR_ENABLE] = True

        self.emergency_landed_state.handle()

        self.assertFalse(
            self.mock_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.NAVIGATOR_ENABLE]
        )

    def test_sets_stream_type_to_position(self):
        self.emergency_landed_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.POSITION,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )
