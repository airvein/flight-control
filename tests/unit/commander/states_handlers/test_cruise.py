from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, Cruise
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestCruise(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestCruise, self).setUp()

        self.cruise_state = Cruise(self.mock_machine_controller,
                                   self.telemetry, self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(Cruise, AbstractStateHandler))

    def test_updates_device_state_to_cruise(self):
        self.cruise_state.handle()

        self.mock_machine_controller.add_pending_task.assert_called_with(
            self.mock_command_manager.execute, "send_device_state",
            new_state=CloudStateNames.CRUISE
        )

    def test_sets_stream_type_to_in_flight(self):
        self.cruise_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_FLIGHT,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )
