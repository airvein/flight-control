import json
from typing import Dict, Optional, Text
from unittest import TestCase
from unittest.mock import MagicMock

from flight_control.commander.state_data import get_state_data
from rclpy.executors import Executor

from flight_control.command_interpreter.publishers import Publishers
from flight_control.commander import Telemetry
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.command_interpreter.command_manager import CommandManager


class BaseStateHandlerTestCase(TestCase):
    def setUp(self) -> None:
        self.mock_machine_controller = MagicMock(
            spec=StateMachineController, publishers=MagicMock(spec=Publishers),
            executor=MagicMock(spec=Executor), state_data=get_state_data()
        )
        self.telemetry = Telemetry()
        self.mock_command_manager = MagicMock(spec=CommandManager)

    def assertSubsetInDict(self, subset: Dict, target_set: Dict, msg: Optional[Text] = None):
        if not subset.items() <= target_set.items():
            if msg is None:
                msg = 'Target Dict does not contain subset Dict.'
                self.fail(msg)

    def assertProperMissionDataStructure(self, output: str, expected: dict):
        self.assertIsInstance(output, str)

        output_dict = json.loads(output)

        self.assertDictEqual(output_dict, expected)
