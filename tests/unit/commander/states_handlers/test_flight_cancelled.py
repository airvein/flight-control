import functools
from unittest.mock import MagicMock, Mock

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, FlightCanceled
from flight_control.route import Route
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase
from tests.unit.fixtures import read_track_data


class TestFlightCancelled(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestFlightCancelled, self).setUp()

        self.flight_canceled_state = FlightCanceled(
            self.mock_machine_controller,
            self.telemetry, self.mock_command_manager)
        self.telemetry.route = Route.from_json(read_track_data())
        self.mock_machine_controller.flight_canceled = MagicMock(spec=functools.partial)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(FlightCanceled, AbstractStateHandler))

    def test_updates_device_state_to_flight_canceling(self):
        self.mock_command_manager.execute = Mock()

        self.flight_canceled_state.handle()

        self.mock_command_manager.execute.assert_called_with(
            "send_device_state", new_state=CloudStateNames.FLIGHT_CANCELING
        )

    def test_clears_route_data(self):
        self.flight_canceled_state.handle()

        self.assertIsNone(self.telemetry.route)

    def test_sets_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.flight_canceled_state.handle()

        self.assertSubsetInDict(expected_state_data, self.mock_machine_controller.state_data)
