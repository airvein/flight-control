from unittest.mock import call

from flight_control.command_interpreter import CloudStateNames, DroneHangarMessageNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, Ready
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestReady(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestReady, self).setUp()
        self.ready_state = Ready(self.mock_machine_controller,
                                 self.telemetry, self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(Ready, AbstractStateHandler))

    def test_updates_device_state_to_ready_from_initializing_and_sends_drone_ready_message_to_hangar(self):
        self.ready_state.handle()

        calls = [call(self.mock_command_manager.execute, "send_device_state",
                      new_state=CloudStateNames.READY),
                 call(self.mock_command_manager.execute, "send_drone_state",
                      state=DroneHangarMessageNames.DRONE_READY),
                 ]
        self.mock_machine_controller.add_pending_task.assert_has_calls(calls)

    def test_sets_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.ready_state.handle()

        self.assertSubsetInDict(expected_state_data, self.mock_machine_controller.state_data)
