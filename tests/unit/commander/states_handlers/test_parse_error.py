from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import ParseError, AbstractStateHandler
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestParseError(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestParseError, self).setUp()

        self.parse_error_state = ParseError(self.mock_machine_controller,
                                            self.telemetry,
                                            self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(ParseError, AbstractStateHandler))

    def test_sends_route_parse_error_alert(self):
        self.skipTest("Not yet implemented")

    def test_sets_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.parse_error_state.handle()

        self.assertSubsetInDict(expected_state_data, self.mock_machine_controller.state_data)

    def test_handle_should_execute_send_route_parse_error_alert(self):
        self.parse_error_state.handle()

        self.mock_command_manager.execute.assert_called_once_with(
            'send_route_parse_error_alert'
        )
