from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, CruiseLand
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestCruiseLand(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestCruiseLand, self).setUp()

        self.cruise_land_state = CruiseLand(self.mock_machine_controller,
                                            self.telemetry,
                                            self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(CruiseLand, AbstractStateHandler))

    def test_updates_device_state_to_landing_in_normal_flight(self):
        self.cruise_land_state.handle()

        self.mock_command_manager.execute.assert_called_with(
            "send_device_state", new_state=CloudStateNames.LANDING
        )

    def test_updates_device_state_to_landing_in_rth_flight(self):
        self.mock_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.RTH] = True

        self.cruise_land_state.handle()

        self.mock_command_manager.execute.assert_called_with(
            "send_device_state",
            new_state=CloudStateNames.rth(CloudStateNames.LANDING)
        )

    def test_sets_stream_type_to_in_flight(self):
        self.cruise_land_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_FLIGHT,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )

    def test_removes_waiting_flag_if_is_set(self):
        self.mock_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = True

        self.cruise_land_state.handle()

        self.assertFalse(
            self.mock_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.WAITING])
