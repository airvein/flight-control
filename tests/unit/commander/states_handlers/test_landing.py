from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, Landing
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestLanding(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestLanding, self).setUp()

        self.landing_state = Landing(self.mock_machine_controller,
                                     self.telemetry, self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(Landing, AbstractStateHandler))

    def test_sets_stream_type_to_in_flight(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_FLIGHT
        }

        self.landing_state.handle()

        self.assertSubsetInDict(expected_state_data, self.mock_machine_controller.state_data)
