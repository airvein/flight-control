import functools
from unittest.mock import MagicMock

from flight_control.command_interpreter import DroneHangarMessageNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    CancelEndFlight
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestCancelEndFlight(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestCancelEndFlight, self).setUp()

        self.mock_machine_controller.system_shutdown = MagicMock(
            spec=functools.partial)

        self.cancel_end_flight_state = CancelEndFlight(
            self.mock_machine_controller, self.telemetry,
            self.mock_command_manager)

    def test_class_should_implement_abstract_state_handler(self):
        self.assertTrue(issubclass(CancelEndFlight, AbstractStateHandler))

    def test_handle_should_set_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.cancel_end_flight_state.handle()

        self.assertSubsetInDict(
            expected_state_data, self.mock_machine_controller.state_data)

    def test_handle_should_execute_send_drone_state(self):
        self.cancel_end_flight_state.handle()

        self.mock_command_manager.execute.assert_called_once_with(
            "send_drone_state",
            state=DroneHangarMessageNames.FLIGHT_CANCELED
        )
