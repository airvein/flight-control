from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, Approach
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestApproach(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestApproach, self).setUp()

        self.approach_state = Approach(self.mock_machine_controller,
                                       self.telemetry,
                                       self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(Approach, AbstractStateHandler))

    def test_updates_device_state_to_approach_in_normal_flight(self):
        self.approach_state.handle()

        self.mock_machine_controller.add_pending_task.assert_called_with(
            self.mock_command_manager.execute, "send_device_state",
            new_state=CloudStateNames.APPROACH)

    def test_updates_device_state_to_approach_in_rth_flight(self):
        self.mock_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.RTH] = True

        self.approach_state.handle()

        self.mock_machine_controller.add_pending_task.assert_called_with(
            self.mock_command_manager.execute, "send_device_state",
            new_state=CloudStateNames.rth(CloudStateNames.APPROACH))

    def test_sets_stream_type_to_in_flight(self):
        self.approach_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_FLIGHT,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )
