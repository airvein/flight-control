from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import WaitCargo, AbstractStateHandler
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestWaitCargo(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestWaitCargo, self).setUp()

        self.wait_cargo_state = WaitCargo(self.mock_machine_controller,
                                          self.telemetry,
                                          self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(WaitCargo, AbstractStateHandler))

    def test_updates_device_state_from_initializing_to_cargo_prepare(self):
        self.wait_cargo_state.handle()

        self.mock_machine_controller.add_pending_task.assert_called_with(
            self.mock_command_manager.execute, "send_device_state",
            new_state=CloudStateNames.CARGO_PREPARE
        )

    def test_sets_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.wait_cargo_state.handle()

        self.assertSubsetInDict(expected_state_data, self.mock_machine_controller.state_data)
