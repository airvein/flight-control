from unittest.mock import call, Mock

from std_msgs import msg
from rclpy.publisher import Publisher

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    Landed
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestLanded(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestLanded, self).setUp()

        self.landed_state = Landed(self.mock_machine_controller,
                                   self.telemetry, self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(Landed, AbstractStateHandler))

    def test_updates_device_state_to_landed_in_normal_flight_and_enables_communication_with_hangar(
            self):
        self.landed_state.handle()

        calls = [call(self.mock_command_manager.execute, "send_device_state",
                      new_state=CloudStateNames.LANDED),
                 ]
        self.mock_machine_controller.add_pending_task.assert_has_calls(calls)

    def test_handle_should_send_command_to_enable_mqtt_client_when_called(self):
        self.landed_state._command_manager.execute = Mock()

        self.landed_state.handle()

        self.landed_state._command_manager.execute.assert_called_with(
            "set_mqtt_client_state", connected=True
        )

    def test_updates_device_state_to_landed_in_rth_flight_and_enables_communication_with_hangar(
            self):
        self.mock_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.RTH] = True

        self.landed_state.handle()

        calls = [call(self.mock_command_manager.execute, "send_device_state",
                      new_state=CloudStateNames.rth(CloudStateNames.LANDED)),
                 ]
        self.mock_machine_controller.add_pending_task.assert_has_calls(calls)

    def test_enables_navigator_is_disabled_in_state_data(self):
        self.landed_state.handle()

        self.assertFalse(
            self.mock_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.NAVIGATOR_ENABLE]
        )

    def test_sets_stream_type_to_in_hangar(self):
        self.landed_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_HANGAR,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )
