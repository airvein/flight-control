from unittest.mock import call

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, EmergencyLanding
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestEmergencyLanding(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestEmergencyLanding, self).setUp()

        self.emergency_landing_state = EmergencyLanding(
            self.mock_machine_controller,
            self.telemetry, self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(EmergencyLanding, AbstractStateHandler))

    def test_updates_device_state_to_emergency_landing_and_calls_land_service(self):
        self.emergency_landing_state.handle()

        self.mock_command_manager.execute.assert_called_with(
            "send_device_state", new_state=CloudStateNames.EL_LANDING)

    def test_sets_stream_type_to_in_flight(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_FLIGHT
        }

        self.emergency_landing_state.handle()

        self.assertSubsetInDict(expected_state_data, self.mock_machine_controller.state_data)
