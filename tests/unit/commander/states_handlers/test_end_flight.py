from flight_control.command_interpreter import DroneHangarMessageNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, EndFlight
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestEndFlight(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestEndFlight, self).setUp()
        self.end_flight_state = EndFlight(self.mock_machine_controller,
                                          self.telemetry,
                                          self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(EndFlight, AbstractStateHandler))

    def test_send_flight_ended_message_to_hangar(self):
        self.end_flight_state.handle()

        self.mock_machine_controller.add_pending_task.assert_called_with(
            self.mock_command_manager.execute, "send_drone_state",
            state=DroneHangarMessageNames.FLIGHT_ENDED
        )

    def test_sets_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.end_flight_state.handle()

        self.assertSubsetInDict(expected_state_data, self.mock_machine_controller.state_data)
