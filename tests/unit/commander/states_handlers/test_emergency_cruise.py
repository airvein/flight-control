from unittest.mock import call

from flight_control.command_interpreter import CloudStateNames, MissionTypes
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, EmergencyCruise
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestEmergencyCruise(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestEmergencyCruise, self).setUp()

        self.emergency_cruise_state = EmergencyCruise(
            self.mock_machine_controller,
            self.telemetry, self.mock_command_manager)
        self.mock_machine_controller.state_data[
            StateDataNames.EMERGENCY_LAND] = {'emergency_route_id': 12}

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(EmergencyCruise, AbstractStateHandler))

    def test_handle_should_call_command_manager_with_proper_commands(self):
        self.emergency_cruise_state.handle()

        calls = [
            call("send_mission_data", mission=MissionTypes.EMERGENCY),
            call("send_device_state", new_state=CloudStateNames.EL_CRUISE),
        ]
        self.mock_command_manager.execute.assert_has_calls(calls)

    def test_handle_should_set_emergency_route_id(self):
        self.emergency_cruise_state.handle()

        self.assertEqual(12, self.telemetry.emergency_route_id)

    def test_sets_stream_type_to_in_flight(self):
        self.emergency_cruise_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_FLIGHT,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )
