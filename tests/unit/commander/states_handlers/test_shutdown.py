from unittest.mock import Mock, call, patch

from periphery import GPIO

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    Shutdown
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase
from flight_control.common.exceptions import MachineShutdown


class TestShutdown(BaseStateHandlerTestCase):
    @patch("flight_control.commander.states_handlers._shutdown.GPIO")
    def setUp(self, mock_gpio) -> None:
        super(TestShutdown, self).setUp()
        patch_time = patch(
            'flight_control.commander.states_handlers._shutdown.time')
        self.mock_time = patch_time.start()
        self.addCleanup(patch_time.stop)

        patcher_logger = patch(
            'flight_control.commander.states_handlers._shutdown.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.shutdown_state = Shutdown(self.mock_machine_controller,
                                       self.telemetry,
                                       self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(Shutdown, AbstractStateHandler))

    @patch.object(Shutdown, "shutdown_system")
    def test_updates_device_state_to_shutdown_and_disconnect_mqtt_client_when_called(
            self, mock_shutdown_system):
        self.mock_command_manager.execute = Mock()
        calls = [
            call("send_device_state", new_state=CloudStateNames.SHUTDOWN),
            call("set_mqtt_client_state", connected=False),
        ]

        self.shutdown_state.handle()

        self.mock_command_manager.execute.has_calls(calls)

    @patch.object(Shutdown, "shutdown_system")
    def test_handle_should_set_stream_type_to_in_hangar(
            self, mock_shutdown_system):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.shutdown_state.handle()

        self.assertSubsetInDict(expected_state_data,
                                self.mock_machine_controller.state_data)

    @patch.object(Shutdown, '_shutdown_stream')
    @patch('flight_control.commander.states_handlers._shutdown.Power')
    @patch("flight_control.commander.states_handlers._shutdown.os.system")
    def test_shutdown_system_should_shutdown_system_when_FC_NO_SHUTDOWN_not_set(
            self, mock_os_system, mock_power_settings, mock_shutdown_stream):
        mock_power_settings.NO_SHUTDOWN = False

        self.shutdown_state.shutdown_system()

        self.mock_command_manager.execute.assert_called_once_with(
            "send_drone_status", previous_status=None, current_flag_state=False
        )
        self.mock_time.sleep.assert_called_once()
        mock_shutdown_stream.assert_called_once()
        mock_os_system.assert_called_once_with('shutdown -h now')

    @patch.object(Shutdown, '_shutdown_stream')
    @patch('flight_control.commander.states_handlers._shutdown.Power')
    @patch("flight_control.commander.states_handlers._shutdown.os.system")
    def test_handle_should_raise_MachineShutdown_when_FC_NO_SHUTDOWN_is_set(
            self, mock_os_system, mock_power_settings, mock_shutdown_stream):
        mock_power_settings.NO_SHUTDOWN = True

        with self.assertRaises(MachineShutdown):
            self.shutdown_state.shutdown_system()

        self.mock_command_manager.execute.assert_called_once_with(
            "send_drone_status", previous_status=None, current_flag_state=False
        )
        self.mock_time.sleep.assert_called_once()
        mock_shutdown_stream.assert_called_once()
        mock_os_system.assert_not_called()

    @patch('flight_control.commander.states_handlers._shutdown.Power')
    def test_shutdown_stream_should_write_True_only_if_SHUTDOWN_STREAM_is_set(
            self, mock_power_settings):
        self.shutdown_state._shutdown_pin = Mock(spec=GPIO)
        shutdown_pin_mock = self.shutdown_state._shutdown_pin

        for case in [True, False]:
            with self.subTest():
                mock_power_settings.SHUTDOWN_STREAM = case

                self.shutdown_state._shutdown_stream()

                if case:
                    shutdown_pin_mock.write.assert_called_once_with(True)
                else:
                    shutdown_pin_mock.write.assert_not_called()
                shutdown_pin_mock.reset_mock()
