from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, ServiceMode
from tests.unit.commander.states_handlers.base_state_handler_test import BaseStateHandlerTestCase


class TestServiceMode(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.service_mode = ServiceMode(self.mock_machine_controller,
                                        self.telemetry,
                                        self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(ServiceMode, AbstractStateHandler))

    def test_handle_should_set_stream_type_to_in_hangar_when_called(self):
        self.service_mode.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_HANGAR,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )
