"""test_killed.py

Tests for "killed" state handler

"""
from unittest.mock import call

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    Killed
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestKilled(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super().setUp()

        self.killed = Killed(
            self.mock_machine_controller, self.telemetry,
            self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(Killed, AbstractStateHandler))

    def test_handle_should_set_stream_type_to_position(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.POSITION
        }

        self.killed.handle()

        self.assertSubsetInDict(
            expected_state_data, self.mock_machine_controller.state_data)

    def test_handle_should_call_proper_execute_commands(self):
        expected_calls = [
            call("send_device_state", new_state=CloudStateNames.KILLED),
            call("kill"),
        ]

        self.killed.handle()

        self.mock_command_manager.execute.assert_has_calls(expected_calls)
