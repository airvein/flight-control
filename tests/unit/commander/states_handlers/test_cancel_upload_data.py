import functools
from unittest.mock import MagicMock

from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    CancelUploadData
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestCancelUploadData(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestCancelUploadData, self).setUp()

        self.mock_machine_controller.system_shutdown = MagicMock(
            spec=functools.partial)

        self.cancel_upload_data_state = CancelUploadData(
            self.mock_machine_controller, self.telemetry,
            self.mock_command_manager)

    def test_class_should_implement_abstract_state_handler(self):
        self.assertTrue(issubclass(CancelUploadData, AbstractStateHandler))

    def test_handle_should_set_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.cancel_upload_data_state.handle()

        self.assertSubsetInDict(
            expected_state_data, self.mock_machine_controller.state_data)
