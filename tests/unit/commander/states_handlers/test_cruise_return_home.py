from unittest.mock import Mock, call, MagicMock, patch

from flight_control.command_interpreter import CloudStateNames, MissionTypes
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import (
    AbstractStateHandler,
    CruiseReturnHome,
)
from tests.unit.commander.states_handlers.base_state_handler_test import (
    BaseStateHandlerTestCase,
)


class TestCruiseReturnHome(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestCruiseReturnHome, self).setUp()

        self.cruise_return_home_state = CruiseReturnHome(
            self.mock_machine_controller, self.telemetry, self.mock_command_manager
        )

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(CruiseReturnHome, AbstractStateHandler))

    def test_sets_rth_flag(self):
        self.cruise_return_home_state.handle()

        self.assertTrue(self.mock_machine_controller.state_data[
                            StateDataNames.FLAGS][StateDataNames.Flag.RTH])

    def test_handle_should_set_stream_type_to_in_flight(self):
        self.cruise_return_home_state.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_FLIGHT,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )

    def test_handle_should_execute_proper_commands_when_called(self):
        self.cruise_return_home_state.handle()

        calls = [
            call("send_mission_data", mission=MissionTypes.RTH),
            call("send_device_state",
                 new_state=CloudStateNames.rth(CloudStateNames.CRUISE)),
        ]
        self.cruise_return_home_state._command_manager.execute.assert_has_calls(
            calls)
