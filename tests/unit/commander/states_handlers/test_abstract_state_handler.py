from unittest import TestCase
from unittest.mock import MagicMock

from flight_control.commander import Telemetry
from flight_control.commander.state_machine_controller import StateMachineController
from flight_control.commander.states_handlers import AbstractStateHandler
from flight_control.command_interpreter.command_manager import CommandManager


class HandlerImplementation(AbstractStateHandler):
    def __init__(self, state_machine: StateMachineController,
                 telemetry: Telemetry, command_manager: CommandManager):
        super().__init__(state_machine, telemetry, command_manager)
        self.handle_called = False

    def handle(self):
        self.handle_called = True


class TestAbstractStateHandler(TestCase):
    def setUp(self) -> None:
        self.mock_state_machine = MagicMock(spec=StateMachineController)
        self.mock_telemetry = MagicMock(spec=Telemetry)
        self.mock_command_manager = MagicMock(spec=CommandManager)

    def test_can_not_create_abstract_class_instance(self):
        with self.assertRaises(TypeError):
            AbstractStateHandler(self.mock_state_machine, self.mock_telemetry,
                                 self.mock_command_manager)

    def test_can_instantiate_subclass_of_abstract_class(self):
        obj = HandlerImplementation(self.mock_state_machine,
                                    self.mock_telemetry,
                                    self.mock_command_manager)
        self.assertIsInstance(obj, AbstractStateHandler)

    def test_requires_implementation_of_abstract_methods(self):
        obj = HandlerImplementation(self.mock_state_machine,
                                    self.mock_telemetry,
                                    self.mock_command_manager)

        obj.handle()

        self.assertTrue(obj.handle_called)

    def test_state_machine_controller_and_telemetry_are_saved_in_base_class(self):
        obj = HandlerImplementation(self.mock_state_machine,
                                    self.mock_telemetry,
                                    self.mock_command_manager)

        self.assertIs(obj._state_machine, self.mock_state_machine)
        self.assertIs(obj._telemetry, self.mock_telemetry)
