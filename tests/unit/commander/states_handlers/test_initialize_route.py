import functools
import logging
from unittest.mock import Mock, MagicMock, call, patch

from flight_control.command_interpreter import DroneHangarMessageNames, \
    MissionTypes
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    InitializeRoute
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase
from tests.unit.fixtures import read_track_data
from flight_control.route.route import RouteError
from flight_control.route import Route


class TestInitializeRoute(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestInitializeRoute, self).setUp()

        self.track_data = read_track_data()
        self.mock_machine_controller.parse_error = MagicMock(
            spec=functools.partial)
        self.mock_machine_controller.state_data[StateDataNames.DOWNLOAD_ROUTE] = \
            {'route_data': self.track_data}

        patch_logger = patch('flight_control.commander.states_handlers._initialize_route.logger',
                             spec=logging.Logger)
        self.logger = patch_logger.start()
        self.addCleanup(patch_logger.stop)

        self.initialize_route_state = InitializeRoute(
            self.mock_machine_controller,
            self.telemetry, self.mock_command_manager)

    def test_class_should_inherit_from_abstract_state_handler(self):
        self.assertTrue(issubclass(InitializeRoute, AbstractStateHandler))

    @patch.object(Route, 'from_json', return_value='route_data')
    def test_handle_should_parse_route(self, mock_from_json):
        self.initialize_route_state.handle()

        mock_from_json.assert_called_once_with(
            self.mock_machine_controller.state_data[
                StateDataNames.DOWNLOAD_ROUTE]['route_data']
        )
        self.assertEqual('route_data', self.telemetry.route)

    def test_handle_should_trigger_parse_error_when_got_route_error(self):
        with patch.object(Route, 'from_json', side_effect=[RouteError()]):
            self.initialize_route_state.handle()

            self.mock_machine_controller.add_pending_transition.assert_called_with(
                self.mock_machine_controller.parse_error
            )

    @patch.object(Route, 'from_json', side_effect=[RouteError("RouteError")])
    def test_handle_should_NOT_update_drone_state_if_parse_error(self, mock_fn):
        self.mock_machine_controller.parse_error = Mock()

        self.initialize_route_state.handle()

        self.mock_command_manager.execute.assert_not_called()
        self.mock_machine_controller.add_pending_transition.assert_called_with(
            self.mock_machine_controller.parse_error)
        self.logger.error.assert_called_once()

    def test_handle_should_execute_proper_tasks_when_called(self):
        self.initialize_route_state._prepare_mission_data = Mock()
        calls = [
            call("send_mission_data", mission=MissionTypes.NORMAL),
            call("send_drone_state",
                 state=DroneHangarMessageNames.DRONE_INITIALIZED),
        ]

        self.initialize_route_state.handle()

        self.mock_command_manager.execute.assert_has_calls(calls)

    def test_handle_should_set_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.initialize_route_state.handle()

        self.assertSubsetInDict(
            expected_state_data, self.mock_machine_controller.state_data
        )
