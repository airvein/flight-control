import functools
from unittest.mock import MagicMock

from drone_types.msg import PxControlStatus

from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    CheckReady
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestCheckReady(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super(TestCheckReady, self).setUp()

        self.check_ready_state = CheckReady(self.mock_machine_controller,
                                            self.telemetry,
                                            self.mock_command_manager)

        self.mock_machine_controller.ready = MagicMock(spec=functools.partial)
        self.mock_machine_controller.repeat_check = MagicMock(
            spec=functools.partial
        )

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(CheckReady, AbstractStateHandler))

    def test_handle_should_add_transition_ready_if_all_checks_passed(self):
        status = self.telemetry.px_control_status
        status.arm_state = PxControlStatus.ARM_STATE_READY
        status.mission_state = PxControlStatus.MISSION_STATE_NORMAL
        self.telemetry.sensors.presence.battery = True

        self.check_ready_state.handle()

        self.mock_machine_controller.add_pending_transition.assert_called_with(
            self.mock_machine_controller.ready
        )

    def test_handle_should_add_transition_repeat_check_if_not_all_checks_passed(
            self):
        self.telemetry.sensors.presence.battery = False

        self.check_ready_state.handle()

        self.mock_machine_controller.add_pending_transition.assert_called_with(
            self.mock_machine_controller.repeat_check
        )

    def test_handle_should_set_stream_type_to_in_hangar(self):
        expected_state_data = {
            StateDataNames.STREAM_TYPE: StateDataNames.StreamType.IN_HANGAR
        }

        self.check_ready_state.handle()

        self.assertSubsetInDict(
            expected_state_data, self.mock_machine_controller.state_data
        )
