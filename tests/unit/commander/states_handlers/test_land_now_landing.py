"""test_land_now_landing.py

Tests for "ln_landing" state handler

"""
from unittest.mock import call

from flight_control.command_interpreter import CloudStateNames
from flight_control.commander import StateDataNames
from flight_control.commander.states_handlers import AbstractStateHandler, \
    LandNowLanding
from tests.unit.commander.states_handlers.base_state_handler_test import \
    BaseStateHandlerTestCase


class TestLandNowLanding(BaseStateHandlerTestCase):
    def setUp(self) -> None:
        super().setUp()

        self.ln_landing = LandNowLanding(
            self.mock_machine_controller, self.telemetry,
            self.mock_command_manager)

    def test_implements_abstract_state_handler(self):
        self.assertTrue(issubclass(LandNowLanding, AbstractStateHandler))

    def test_sets_stream_type_to_in_flight(self):
        self.ln_landing.handle()

        self.assertEqual(
            StateDataNames.StreamType.IN_FLIGHT,
            self.mock_machine_controller.state_data[StateDataNames.STREAM_TYPE]
        )

    def test_handle_should_send_device_state_and_call_land_now_command_when_called(self):
        expected_calls = [
            call("send_device_state", new_state=CloudStateNames.LN_LANDING),
            call("land_now")
            ]

        self.ln_landing.handle()

        self.ln_landing._command_manager.execute.assert_has_calls(expected_calls)
