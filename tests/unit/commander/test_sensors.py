"""test_sensors.py

Tests for Sensors class storing data from sensors

Copyright 2019 Cervi Robotics
"""
import unittest

from drone_types import msg

from flight_control.commander.sensors import Sensors


class TestSensors(unittest.TestCase):

    def setUp(self) -> None:
        self.sensors = Sensors()

    def test_sensors_should_have_correct_class_instances_when_initialized(
            self):
        self.assertIsInstance(self.sensors.presence, msg.SensorsPresence)
        self.assertIsInstance(self.sensors.button_press,
                              msg.SensorsButtonPress)
