"""test_telemetry.py

Test for Telemetry class storing data of telemetry

Copyright 2019 Cervi Robotics
"""
import unittest

from drone_types import msg

from flight_control.commander.sensors import Sensors
from flight_control.commander.telemetry import (NextPoint, PointNED,
                                                PreviousPoint, Telemetry)
from flight_control.route import Route


class TestPointNED(unittest.TestCase):
    def test_point_ned_should_raise_error_when_initialized_without_arguments(self):
        self.assertRaises(TypeError, PointNED)

    def test_point_ned_should_have_specified_arguments_when_initialized(self):
        point = PointNED(id=1, x=50.1, y=20.2, z=-30.3, yaw=90.9)

        self.assertEqual(point.id, 1)
        self.assertEqual(point.x, 50.1)
        self.assertEqual(point.y, 20.2)
        self.assertEqual(point.z, -30.3)
        self.assertEqual(point.yaw, 90.9)

    def test_point_ned_should_raise_TypeError_when_called_with_wrong_type_of_id(self):
        self.assertRaises(TypeError, PointNED, id="1",
                          x=50.1, y=20.2, z=30.3, yaw=90.1)

    def test_point_ned_should_raise_TypeError_when_called_with_wrong_type_of_x(self):
        self.assertRaises(TypeError, PointNED, id=1,
                          x="50.1", y=20.2, z=30.3, yaw=90.1)

    def test_point_ned_should_raise_TypeError_when_called_with_wrong_type_of_y(self):
        self.assertRaises(TypeError, PointNED, id=1, x=50.1,
                          y="20.2", z=30.3, yaw=90.1)

    def test_point_ned_should_raise_TypeError_when_called_with_wrong_type_of_z(self):
        self.assertRaises(TypeError, PointNED, id=1, x=50.1,
                          y=20.2, z="30.3", yaw=90.1)

    def test_point_ned_should_raise_TypeError_when_called_with_wrong_type_of_yaw(self):
        self.assertRaises(TypeError, PointNED, id=1, x=50.1,
                          y=20.2, z=30.3, yaw="90.1")

    def test_equality_should_return_True_when_x_y_z_yaw_are_the_same(self):
        point1 = PointNED(id=1, x=50.123, y=20.123, z=-30.123, yaw=90.912)
        point2 = PointNED(id=2, x=50.124, y=20.124, z=-30.124, yaw=90.914)
        self.assertEqual(point1, point2)

    def test_equality_should_return_False_when_x_y_z_yaw_are_have_different_decimal_places(self):
        point1 = PointNED(id=1, x=50.123, y=20.123, z=-30.123, yaw=90.912)
        point2 = PointNED(id=2, x=50.13, y=20.124, z=-30.124, yaw=90.914)
        self.assertNotEqual(point1, point2)


class TestPreviousPointAndNextPoint(unittest.TestCase):
    def setUp(self) -> None:
        self.previous_point = PreviousPoint(
            id=1, x=50.1, y=20.2, z=-30.3, yaw=90.9)
        self.next_point = NextPoint(id=1, x=50.1, y=20.2, z=-30.3, yaw=90.9)

    def test_PreviousPoint_should_be_subclass_of_PointNED_when_initialized(self):
        self.assertTrue(issubclass(PreviousPoint, PointNED))

    def test_NextPoint_should_be_subclass_of_PointNED_when_initialized(self):
        self.assertTrue(issubclass(NextPoint, PointNED))

    def test_PreviousPoint_and_NextPoint_should_have_declared_arguments_when_initialized(self):
        points = [self.previous_point, self.next_point]
        for point in points:
            with self.subTest(msg=point):
                self.assertEqual(point.id, 1)
                self.assertEqual(point.x, 50.1)
                self.assertEqual(point.y, 20.2)
                self.assertEqual(point.z, -30.3)
                self.assertEqual(point.yaw, 90.9)


class TestTelemetry(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = Telemetry()

    def test_telemetry_should_have_specified_arguments_when_initialized(self):

        self.assertIsInstance(self.telemetry.local_pos, msg.PxControlLocalPos)
        self.assertIsInstance(self.telemetry.global_pos,
                              msg.PxControlGlobalPos)
        self.assertIsInstance(self.telemetry.velocity, msg.PxControlVelocity)
        self.assertIsInstance(self.telemetry.attitude, msg.PxControlAttitude)
        self.assertIsInstance(self.telemetry.route, Route)
        self.assertIsInstance(self.telemetry.sensors, Sensors)
        self.assertIsNone(self.telemetry.emergency_route_id)
        self.assertIsNone(self.telemetry.emergency_end_point)

    def test_telemetry_should_update_local_pos_when_called(self):
        test_message = msg.PxControlLocalPos(x=10.1, y=20.2, z=5.5)

        self.telemetry.local_pos = test_message

        self.assertEqual(self.telemetry.local_pos.x, 10.1)
        self.assertEqual(self.telemetry.local_pos.y, 20.2)
        self.assertEqual(self.telemetry.local_pos.z, 5.5)

    def test_telemetry_should_update_global_pos_when_called(self):
        test_message = msg.PxControlGlobalPos(
            latitude=1010.1, longitude=2020.2, altitude=20.5)

        self.telemetry.global_pos = test_message
        self.assertEqual(self.telemetry.global_pos.latitude, 1010.1)
        self.assertEqual(self.telemetry.global_pos.longitude, 2020.2)
        self.assertEqual(self.telemetry.global_pos.altitude, 20.5)

    def test_telemetry_should_update_velocity_when_called(self):
        test_message = msg.PxControlVelocity(vx=1.0, vy=2.0, vz=3.0)

        self.telemetry.velocity = test_message
        self.assertEqual(self.telemetry.velocity.vx, 1.0)
        self.assertEqual(self.telemetry.velocity.vy, 2.0)
        self.assertEqual(self.telemetry.velocity.vz, 3.0)

    def test_telemetry_should_update_attitude_when_called(self):
        test_message = msg.PxControlAttitude(
            q_w=1.0, q_x=2.0, q_y=3.0, q_z=4.0)

        self.telemetry.attitude = test_message
        self.assertEqual(self.telemetry.attitude.q_w, 1.0)
        self.assertEqual(self.telemetry.attitude.q_x, 2.0)
        self.assertEqual(self.telemetry.attitude.q_y, 3.0)
        self.assertEqual(self.telemetry.attitude.q_z, 4.0)

    def test_telemetry_should_update_route_when_called(self):
        test_route = Route()
        test_route.route_id = "123"

        self.telemetry.route = test_route

        self.assertEqual(self.telemetry.route.route_id, "123")

    def test_telemetry_should_update_battery_when_called(self):
        test_message = msg.PxControlBattery(
            voltage=1.0, current=2.0, remaining=3.0)

        self.telemetry.battery = test_message
        self.assertEqual(self.telemetry.battery.voltage, 1.0)
        self.assertEqual(self.telemetry.battery.current, 2.0)
        self.assertEqual(self.telemetry.battery.remaining, 3.0)


if __name__ == '__main__':
    unittest.main()
