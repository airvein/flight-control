from unittest import TestCase
from unittest.mock import MagicMock

from rclpy.executors import Executor
from transitions import MachineError

from flight_control.commander import StateNames
from flight_control.commander.state_machine_controller import \
    StateMachineControllerError, StateMachineController
from tests.unit.fixtures import read_track_data


class StateMachineControllerTest(TestCase):
    def setUp(self) -> None:
        self.state_data = {}
        self.mock_executor = MagicMock(spec=Executor)

        self.state_machine = StateMachineController(self.state_data,
                                                    self.mock_executor)
        self.state_machine.set_state(StateNames.POWERED_ON)

    def test_initial_state_is_powered_on(self):
        self.assertTrue(self.state_machine.is_powered_on())

    def test_state_us_updated_after_transition(self):
        self.state_machine.init_flight(flight_id="f123", route_id="r123", cargo=False)
        self.assertTrue(self.state_machine.is_get_route())

    def test_refuses_transition_from_wrong_state(self):
        with self.assertRaises(MachineError):
            self.state_machine.parse_error()

    def test_init_flight_should_pass_data_to_state_data(self):
        expected_state_data = {
            'init_flight': {
                'flight_id': 'f134',
                'route_id': 'r223',
                'cargo': False
            }
        }

        self.state_machine.init_flight(flight_id="f134", route_id="r223", cargo=False)

        self.assertTrue(self.state_machine.is_get_route())
        self.assertDictEqual(expected_state_data, self.state_data)

    def test_refuses_to_change_state_with_incomplete_init_flight_data(self):
        with self.assertRaises(StateMachineControllerError):
            self.state_machine.init_flight()

        self.assertTrue(self.state_machine.is_powered_on())

    def test_download_route_should_pass_data_to_state_data(self):
        route_data = read_track_data()
        expected_state_data = {
            'download_route': {
                'route_data': route_data
            }
        }
        self.state_machine.state = 'get_route'

        self.state_machine.download_route(route_data=route_data)

        self.assertTrue(self.state_machine.is_initialize_route())
        self.assertDictEqual(expected_state_data, self.state_data)

    def test_should_refuse_to_change_state_with_incomplete_download_route_data(self):
        self.state_machine.state = 'get_route'

        with self.assertRaises(StateMachineControllerError):
            self.state_machine.download_route()

        self.assertTrue(self.state_machine.is_get_route())

    def test_emergency_land_should_pass_data_to_state_data(self):
        expected_state_data = {
            'emergency_land': {
                'emergency_route_id': 4
            }
        }
        self.state_machine.state = 'cruise'

        self.state_machine.emergency_land(emergency_route_id=4)

        self.assertTrue(self.state_machine.is_emergency_cruise())
        self.assertDictEqual(expected_state_data, self.state_data)

    def test_should_refuse_to_change_state_with_incomplete_emergency_land_data(self):
        self.state_machine.state = 'cruise'

        with self.assertRaises(StateMachineControllerError):
            self.state_machine.emergency_land()

        self.assertTrue(self.state_machine.is_cruise())

    def test_can_add_pending_transition_and_execute_it_during_update(self):
        test_transition = MagicMock()

        self.state_machine.add_pending_transition(test_transition)

        self.state_machine.update()

        test_transition.assert_called_once()

    def test_can_add_pending_transition_with_args_and_execute_it(self):
        test_transition = MagicMock()

        self.state_machine.add_pending_transition(test_transition, 1, 2, 3)

        self.state_machine.update()

        test_transition.assert_called_with(1, 2, 3)

    def test_can_add_pending_transition_with_kwargs_and_execute_id(self):
        test_transition = MagicMock()

        self.state_machine.add_pending_transition(test_transition, arg1="test", arg2=123)

        self.state_machine.update()

        test_transition.assert_called_with(arg1="test", arg2=123)

    def test_add_pending_task_should_add_task_to_pending_task_and_execute_it_during_update(self):
        test_task = MagicMock()

        self.state_machine.add_pending_task(test_task)

        self.state_machine.update()

        self.mock_executor.create_task.assert_called_once()

    def test_clear_pending_transitions_should_clear_pending_transitions(self):
        test_transition1 = MagicMock()
        test_transition2 = MagicMock()

        self.state_machine._pending_transitions.put_nowait(test_transition1)
        self.state_machine._pending_transitions.put_nowait(test_transition2)

        self.state_machine.clear_pending_transitions()

        self.assertEqual(self.state_machine._pending_transitions.empty(), True)
