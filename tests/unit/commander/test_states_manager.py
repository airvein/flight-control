import unittest
from unittest.mock import MagicMock, patch

from rclpy.executors import Executor

from flight_control.command_interpreter.publishers import Publishers
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states import STATES
from flight_control.commander.states_handlers import (
    InitializeRoute, PoweredOn, GetRoute, ParseError, WaitCargo,
    CargoPresent, Ready, FlightCanceled, TakeoffStart, TakeoffMonitor, Cruise,
    CheckReady, CruiseReturnHome, ApproachCruise, ApproachCruiseWait, Approach,
    CruiseLand, EmergencyCruise, Landing, Landed, EmergencyLanded,
    CancelUploadData, UploadData, EmergencyShutdown, EndFlight, BatteryShutdown,
    Shutdown, EmergencyLanding, LandNowLanded, LandNowLanding, Killed, ServiceMode
)
from flight_control.commander.states_manager import StatesManager
from flight_control.commander.telemetry import Telemetry
from flight_control.command_interpreter.command_manager import CommandManager


class StatesManagerTest(unittest.TestCase):
    def setUp(self) -> None:
        patcher = patch(
            'flight_control.commander.states_handlers._shutdown.GPIO'
        )
        patcher.start()
        self.addCleanup(patcher.stop)

        self.telemetry = Telemetry()
        self.state_data = {}
        self.mock_publishers = MagicMock(spec=Publishers)
        self.mock_executor = MagicMock(spec=Executor)
        self.mock_command_manager = MagicMock(spec=CommandManager)

        self.state_machine = StateMachineController(self.state_data,
                                                    self.mock_executor)

        self.manager = StatesManager(self.telemetry,
                                     self.state_machine,
                                     self.mock_command_manager)

    def test_manager_is_configured_to_handle_all_defined_states(self):
        self.assertListEqual(STATES, self.manager.handled_states)

    @patch.object(ServiceMode, 'handle')
    def test_handles_service_mode_state(self, mock_handle):
        self.manager.handle_state_change('service_mode')
        mock_handle.assert_called_once()

    @patch.object(Killed, 'handle')
    def test_handles_killed_state(self, mock_handle):
        self.manager.handle_state_change('killed')
        mock_handle.assert_called_once()

    @patch.object(LandNowLanding, 'handle')
    def test_handles_ln_landing_state(self, mock_handle):
        self.manager.handle_state_change('ln_landing')
        mock_handle.assert_called_once()

    @patch.object(LandNowLanded, 'handle')
    def test_handles_ln_landed_state(self, mock_handle):
        self.manager.handle_state_change('ln_landed')
        mock_handle.assert_called_once()

    @patch.object(PoweredOn, 'handle')
    def test_handles_powered_on_state(self, mock_handle):
        self.manager.handle_state_change('powered_on')
        mock_handle.assert_called_once()

    @patch.object(GetRoute, 'handle')
    def test_handles_get_route_state(self, mock_handle):
        self.manager.handle_state_change('get_route')
        mock_handle.assert_called_once()

    @patch.object(InitializeRoute, 'handle')
    def test_handles_initialize_route_state(self, mock_handle):
        self.manager.handle_state_change('initialize_route')
        mock_handle.assert_called_once()

    @patch.object(ParseError, 'handle')
    def test_handles_parse_error_state(self, mock_handle):
        self.manager.handle_state_change('parse_error')
        mock_handle.assert_called_once()

    @patch.object(WaitCargo, 'handle')
    def test_handles_wait_cargo_state(self, mock_handle):
        self.manager.handle_state_change('wait_cargo')
        mock_handle.assert_called_once()

    @patch.object(CargoPresent, 'handle')
    def test_handles_cargo_present_state(self, mock_handle):
        self.manager.handle_state_change('cargo_present')
        mock_handle.assert_called_once()

    @patch.object(CheckReady, 'handle')
    def test_handles_check_ready_state(self, mock_handle):
        self.manager.handle_state_change('check_ready')
        mock_handle.assert_called_once()

    @patch.object(Ready, 'handle')
    def test_handles_ready_state(self, mock_handle):
        self.manager.handle_state_change('ready')
        mock_handle.assert_called_once()

    @patch.object(FlightCanceled, 'handle')
    def test_handles_flight_canceled_state(self, mock_handle):
        self.manager.handle_state_change('flight_canceled')
        mock_handle.assert_called_once()

    @patch.object(TakeoffStart, 'handle')
    def test_handles_takeoff_start_state(self, mock_handle):
        self.manager.handle_state_change('takeoff_start')
        mock_handle.assert_called_once()

    @patch.object(TakeoffMonitor, 'handle')
    def test_handles_takeoff_monitor_state(self, mock_handle):
        self.manager.handle_state_change('takeoff_monitor')
        mock_handle.assert_called_once()

    @patch.object(Cruise, 'handle')
    def test_handles_cruise_state(self, mock_handle):
        self.manager.handle_state_change('cruise')
        mock_handle.assert_called_once()

    @patch.object(CruiseReturnHome, 'handle')
    def test_handles_cruise_return_home_state(self, mock_handle):
        self.manager.handle_state_change('cruise_return_home')
        mock_handle.assert_called_once()

    @patch.object(ApproachCruise, 'handle')
    def test_handles_approach_cruise_state(self, mock_handle):
        self.manager.handle_state_change('approach_cruise')
        mock_handle.assert_called_once()

    @patch.object(ApproachCruiseWait, 'handle')
    def test_handles_approach_cruise_wait_state(self, mock_handle):
        self.manager.handle_state_change('approach_cruise_wait')
        mock_handle.assert_called_once()

    @patch.object(Approach, 'handle')
    def test_handles_approach_state(self, mock_handle):
        self.manager.handle_state_change('approach')
        mock_handle.assert_called_once()

    @patch.object(CruiseLand, 'handle')
    def test_handles_cruise_land_state(self, mock_handle):
        self.manager.handle_state_change('cruise_land')
        mock_handle.assert_called_once()

    @patch.object(EmergencyCruise, 'handle')
    def test_handles_emergency_cruise_state(self, mock_handle):
        self.manager.handle_state_change('emergency_cruise')
        mock_handle.assert_called_once()

    @patch.object(Landing, 'handle')
    def test_handles_landing_state(self, mock_handle):
        self.manager.handle_state_change('landing')
        mock_handle.assert_called_once()

    @patch.object(EmergencyLanding, 'handle')
    def test_handles_emergency_landing_state(self, mock_handle):
        self.manager.handle_state_change('emergency_landing')
        mock_handle.assert_called_once()

    @patch.object(Landed, 'handle')
    def test_handles_landed_state(self, mock_handle):
        self.manager.handle_state_change('landed')
        mock_handle.assert_called_once()

    @patch.object(EmergencyLanded, 'handle')
    def test_handles_emergency_landed_state(self, mock_handle):
        self.manager.handle_state_change('emergency_landed')
        mock_handle.assert_called_once()

    @patch.object(CancelUploadData, 'handle')
    def test_handles_cancel_upload_data_state(self, mock_handle):
        self.manager.handle_state_change('cancel_upload_data')
        mock_handle.assert_called_once()

    @patch.object(UploadData, 'handle')
    def test_handles_upload_data_state(self, mock_handle):
        self.manager.handle_state_change('upload_data')
        mock_handle.assert_called_once()

    @patch.object(EmergencyShutdown, 'handle')
    def test_handles_emergency_shutdown_state(self, mock_handle):
        self.manager.handle_state_change('emergency_shutdown')
        mock_handle.assert_called_once()

    @patch.object(EndFlight, 'handle')
    def test_handles_end_flight_state(self, mock_handle):
        self.manager.handle_state_change('end_flight')
        mock_handle.assert_called_once()

    @patch.object(Shutdown, 'handle')
    def test_handles_shutdown_state(self, mock_handle):
        self.manager.handle_state_change('shutdown')
        mock_handle.assert_called_once()

    @patch.object(BatteryShutdown, 'handle')
    def test_handles_battery_shutdown_state(self, mock_handle):
        self.manager.handle_state_change('battery_shutdown')
        mock_handle.assert_called_once()
