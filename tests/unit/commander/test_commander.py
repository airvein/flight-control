import functools
from unittest import TestCase
from unittest.mock import MagicMock, patch, Mock, call

from rclpy.node import Node
from rclpy.executors import Executor
from rclpy.timer import WallTimer

from flight_control.command_interpreter import CloudStateNames
from flight_control.command_interpreter.publishers import Publishers
from flight_control.commander import StateNames, StateDataNames, Telemetry
from flight_control.commander.state_data import get_state_data
from flight_control.commander.commander import Commander
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.states_manager import StatesManager
from flight_control.navigator.navigator import Navigator
from flight_control.navigator.state import State
from flight_control.command_interpreter.command_manager import CommandManager
from flight_control.state_updater.state_updater import StateUpdater


class TestCommander(TestCase):
    @patch('flight_control.commander.commander.StateUpdater', spec=StateUpdater)
    def setUp(self, mock_state_updater) -> None:
        self.mock_state_updater = mock_state_updater()
        self._init_mocks()

        self.commander = Commander(self.mock_node,
                                   self.mock_executor,
                                   self.mock_states_manager,
                                   self.mock_state_machine_controller,
                                   self.mock_navigator,
                                   self.mock_publishers,
                                   self.mock_command_manager)

    def test_can_create_commander_object(self):
        self.assertIsInstance(self.commander, Commander)

    def test_commander_should_set_default_stream_type_when_initialized(self):
        self.assertEqual(
            StateDataNames.StreamType.IN_HANGAR,
            self.mock_state_machine_controller.state_data[
                StateDataNames.STREAM_TYPE])

    def test_commander_should_call_state_changed_once_when_initialized(self):
        self.mock_states_manager.handle_state_change.assert_called_once()

    def test_commander_should_initialize_ros_timers_when_initialized(self):
        timer_calls = [
            call(1.0, self.commander._ticker_1s_cb),
            call(0.1, self.commander._ticker_100ms_cb)
        ]

        self.mock_node.create_timer.assert_has_calls(timer_calls,
                                                     any_order=True)
        self.assertIsInstance(self.commander._ticker_1s, WallTimer)
        self.assertIsInstance(self.commander._ticker_100ms, WallTimer)

    @patch.object(Commander, "_get_flag_state")
    def test_state_changed_should_call_handle_state_change_and_publish_status(
            self, mock_flag_state):
        mock_flag_state.return_value = "flag_state"
        expected_command = self.commander._command_manager.execute(
            "send_drone_status",
            previous_status=self.commander._prev_status,
            current_flag_state="flag_state"
        )

        self.commander.state_changed()

        self.mock_states_manager.handle_state_change.assert_called()
        self.assertEqual(self.commander._prev_status, expected_command)
        mock_flag_state.assert_called_with(StateDataNames.Flag.WAITING)

    def test_get_flag_state_should_return_flag(self):
        self.mock_state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.NAVIGATOR_INIT] = False
        self.mock_state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.NAVIGATOR_ENABLE] = True

        flag_init = self.commander._get_flag_state(
            StateDataNames.Flag.NAVIGATOR_INIT)
        flag_enable = self.commander._get_flag_state(
            StateDataNames.Flag.NAVIGATOR_ENABLE)

        self.assertEqual(flag_init, False)
        self.assertEqual(flag_enable, True)

    def test_ticker_1s_cb_should_calL_send_heartbeat_AND_NOT_send_drone_stream_when_machine_state_IS_wait_for_nodes(
            self):
        self.mock_state_machine_controller.state = StateNames.WAIT_FOR_NODES
        self.commander._send_drone_stream = Mock()
        self.commander._command_manager.execute = Mock()

        self.commander._ticker_1s_cb()

        self.commander._send_drone_stream.assert_not_called()
        self.commander._command_manager.execute.assert_called_once()

    def test_ticker_1s_cb_should_calL_send_drone_stream_and_send_heartbeat_when_called_and_machine_state_IS_NOT_wait_for_nodes(
            self):
        self.mock_state_machine_controller.state = StateNames.POWERED_ON
        self.commander._send_drone_stream = Mock()
        self.commander._command_manager.execute = Mock()

        self.commander._ticker_1s_cb()

        self.commander._send_drone_stream.assert_called_once()
        self.commander._command_manager.execute.assert_called_once()

    def test_send_drone_stream_should_call_executor_when_time_match_frequency(
            self):
        self.commander._command_manager.execute = Mock()
        self.commander._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR
        self.commander._tick1_loops = 10

        self.commander._send_drone_stream()

        self.commander._command_manager.execute.assert_called_with(
            "send_drone_stream",
            stream_type=StateDataNames.StreamType.IN_HANGAR
        )
        self.assertEqual(self.commander._tick1_loops, 1)  # reset counter

    def test_send_drone_stream_should_not_call_executor_when_time_doesnt_match_frequency(
            self):
        self.commander._command_manager.execute = Mock()
        self.commander._state_machine.state_data[StateDataNames.STREAM_TYPE] = \
            StateDataNames.StreamType.IN_HANGAR
        self.commander._tick1_loops = 9

        self.commander._send_drone_stream()

        self.commander._command_manager.execute.assert_not_called()
        self.assertEqual(self.commander._tick1_loops, 10)

    def test_update_timer_callback_should_update_state_updater_and_state_machine(
            self):
        self.mock_state_machine_controller.state = MagicMock(spec=State)
        self.mock_navigator.state = MagicMock(spec=State)

        self.commander._timer_update_callback()

        self.mock_state_updater.update.assert_called_once()
        self.mock_state_machine_controller.update.assert_called_once()

    def test_update_timer_callback_should_add_download_route_transition_when_state_machine_is_in_get_route_state_and_route_data_is_not_none(
            self):
        self.mock_state_machine_controller.state = StateNames.GET_ROUTE
        self.mock_state_machine_controller.download_route = MagicMock(
            spec=functools.partial)
        self.mock_navigator.telemetry.route_data = "test_value"

        self.commander._timer_update_callback()

        self.mock_state_updater.update.assert_called_once()
        self.mock_state_machine_controller.add_pending_transition.assert_called_once_with(
            self.mock_state_machine_controller.download_route,
            route_data=self.mock_navigator.telemetry.route_data)

    def test_update_timer_callback_should_initialize_navigator_and_set_navigator_init_flag_to_FALSE_when_initialized(
            self):
        self.mock_state_machine_controller.state = MagicMock(spec=State)
        self.mock_state_machine_controller.state_data[
            StateDataNames.FLAGS][StateDataNames.Flag.NAVIGATOR_INIT] = True

        self.commander._timer_update_callback()

        self.assertFalse(
            self.mock_state_machine_controller.state_data[
                StateDataNames.FLAGS][StateDataNames.Flag.NAVIGATOR_INIT])

    @patch.object(StateUpdater, "update", Mock())
    def test_update_timer_callback_should_call_navigator_wait_when_entering_waiting_state_and_in_approach_cruise_wait(
            self):
        self.mock_state_machine_controller.state = StateNames.APPROACH_CRUISE_WAIT
        self.mock_navigator.state = MagicMock(spec=State)
        self.mock_state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = True
        self.mock_navigator.waiting = False

        self.commander._timer_update_callback()

        self.mock_navigator.wait.assert_called_once()
        self.commander._command_manager.execute.assert_any_call("wait")

    @patch.object(StateUpdater, "update", Mock())
    def test_update_timer_callback_should_call_navigator_wait_when_entering_waiting_state_and_not_in_approach_cruise_wait(
            self):
        self.mock_state_machine_controller.state = StateNames.CRUISE
        self.mock_navigator.state = MagicMock(spec=State)
        self.mock_state_machine_controller.current_device_state = CloudStateNames.CRUISE

        self.mock_state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = True
        self.mock_navigator.waiting = False

        self.commander._timer_update_callback()

        self.mock_navigator.wait.assert_called_once()

        expected_calls = [
            call("wait"),
            call("send_device_state",
                 new_state=CloudStateNames.waiting(CloudStateNames.CRUISE))
        ]

        self.commander._command_manager.execute.assert_has_calls(expected_calls, any_order=True)

    @patch.object(StateUpdater, "update", Mock())
    def test_update_timer_callback_should_call_navigator_wait_when_exiting_waiting_state_and_not_in_cruise_land(
            self):
        self.mock_state_machine_controller.state = StateNames.CRUISE
        self.mock_navigator.state = MagicMock(spec=State)
        self.mock_state_machine_controller.current_device_state = CloudStateNames.CRUISE

        self.mock_state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = False
        self.mock_navigator.waiting = True

        self.commander._timer_update_callback()

        self.mock_navigator.continue_flight.assert_called_once()

        expected_calls = [
            call("continue"),
            call("send_device_state",
                 new_state=CloudStateNames.del_waiting(CloudStateNames.CRUISE))
        ]
        self.commander._command_manager.execute.assert_has_calls(expected_calls, any_order=True)

    @patch.object(StateUpdater, "update", Mock())
    def test_update_timer_callback_should_call_navigator_wait_when_exiting_waiting_state_and_in_cruise_land(
            self):
        self.mock_state_machine_controller.state = StateNames.CRUISE_LAND
        self.mock_navigator.state = MagicMock(spec=State)

        self.mock_state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = False
        self.mock_navigator.waiting = True

        self.commander._timer_update_callback()

        self.mock_navigator.continue_flight.assert_called_once()
        self.mock_state_machine_controller.add_pending_task.assert_not_called()
        self.commander._command_manager.execute.assert_any_call("continue")

    def test_timer_update_callback_should_update_navigator_and_streams_when_flag_is_navigator_enable(
            self):
        self.mock_state_machine_controller.state_data[
            StateDataNames.FLAGS][StateDataNames.Flag.NAVIGATOR_ENABLE] = True

        self.commander._timer_update_callback()

        self.commander._navigator.update.assert_called_once()
        self.commander._command_manager.execute.assert_any_call(
            "send_route_progress")

    def test_timer_update_callback_should_NOT_update_navigator_and_streams_when_flag_is_NOT_navigator_enable(
            self):
        self.commander._command_manager.execute = Mock()
        self.mock_state_machine_controller.state_data[
            StateDataNames.FLAGS][StateDataNames.Flag.NAVIGATOR_ENABLE] = False

        self.commander._timer_update_callback()

        self.commander._navigator.update.assert_not_called()

        executor_calls = self.commander._command_manager.execute.call_args_list
        for call in executor_calls:
            self.assertNotIn("send_route_progress", call[0])

    def _init_mocks(self):
        patch_sleep = patch('flight_control.commander.commander.time.sleep')
        self.mock_sleep = patch_sleep.start()
        self.addCleanup(patch_sleep.stop)

        self.mock_node = MagicMock(spec=Node)
        self.mock_node.create_timer.return_value = MagicMock(spec=WallTimer)

        self.mock_states_manager = MagicMock(spec=StatesManager)
        self.mock_navigator = MagicMock(spec=Navigator)
        self.mock_navigator.telemetry = MagicMock(spec=Telemetry)
        self.mock_publishers = MagicMock(spec=Publishers)
        self.mock_executor = MagicMock(spec=Executor)
        self.mock_state_machine_controller = MagicMock(
            spec=StateMachineController, state_data=get_state_data(),
            state=Mock(), current_device_state=CloudStateNames.SHUTDOWN)
        self.mock_command_manager = MagicMock(spec=CommandManager)
