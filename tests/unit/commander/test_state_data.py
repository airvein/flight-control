"""test_state_data.py

Tests for StateData object.

Copyright 2020 Cervi Robotics
"""
from collections import UserDict
import unittest

from flight_control.commander import StateDataNames
from flight_control.commander.state_data import get_state_data, StateData


class TestStateData(unittest.TestCase):

    def setUp(self) -> None:
        self.state_data: StateData = get_state_data()

    def test_class_should_be_subclass_of_UserDict(self):
        self.assertTrue(issubclass(StateData, UserDict))

    def test_fields_other_than_flags_should_be_None_type(self):
        for key in self.state_data:
            if key != StateDataNames.FLAGS:
                with self.subTest():
                    self.assertIsNone(self.state_data[key])

    def test_flags_should_be_a_dict_type(self):
        self.assertIsInstance(self.state_data[StateDataNames.FLAGS], dict)

    def test_all_flags_should_be_set_to_False_when_initialized(self):
        for key in self.state_data[StateDataNames.FLAGS]:
            with self.subTest():
                self.assertFalse(self.state_data[StateDataNames.FLAGS][key])
