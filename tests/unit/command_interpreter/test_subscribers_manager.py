"""test_subscribers_manager.py

Tests for ROS Subscribers Manager which manage subscribers

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node

from flight_control.command_interpreter.subscribers_manager import \
    SubscribersManager
from flight_control.commander import Telemetry
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestSubscribersManager(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = Telemetry()
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)

        self.subscribers_manager = SubscribersManager(
            self.mock_node, self.telemetry, self.executor,
            self.state_machine_controller)

        self.available_subscribers = [
            'comm_init', 'comm_cargo_prepare', 'comm_drone_prepare',
            'comm_start_flight', 'comm_land', 'comm_finish',
            'comm_shutdown_drone', 'comm_return_home', 'comm_route_data',
            'comm_emergency_land', 'comm_land_now', 'comm_wait',
            'comm_continue', 'comm_flight_cancel', 'comm_kill',
            'comm_logs_upload_status',
            'px_control_attitude', 'px_control_local_pos',
            'px_control_global_pos', 'px_control_status',
            'px_control_velocity', 'px_control_ekf_status',
            'px_control_battery', 'sensors_button_press',
            'sensors_presence', 'comm_service_mode'
        ]

    def test_subscribers_manager_should_have_available_subscribers_when_initialized(
            self):
        self.assertEqual(len(self.available_subscribers),
                         len(self.subscribers_manager._subscribers))

    def test_method_subscribers_should_return_dict_of_subscribers_when_called(
            self):
        output = self.subscribers_manager.subscribers

        self.assertIsInstance(output, dict)
        for subscriber in self.available_subscribers:
            with self.subTest(msg=subscriber):
                self.assertIn(subscriber, output)

    def test_is_subscription_registered_should_return_false_when_there_ISNT_subscription(
            self):
        output = self.subscribers_manager._is_subscription_registered(
            'wrong_name')

        self.assertFalse(output)

    def test_is_subscription_registered_should_return_true_when_there_IS_subscription(self):
        for subscriber in self.available_subscribers:
            with self.subTest(msg=subscriber):
                output = self.subscribers_manager._is_subscription_registered(
                    subscriber)

                self.assertTrue(output)
