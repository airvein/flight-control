"""test_command_manager.py

Test for CommandManager

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from rclpy.node import Node
from rclpy.publisher import Publisher
from std_msgs import msg as std_msg

from flight_control.command_interpreter import MissionTypes
from flight_control.command_interpreter._constants import AlertCodes
from flight_control.command_interpreter.publishers import Publishers
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.command_interpreter.command_manager import CommandManager
from flight_control.navigator.navigator import Navigator
from drone_types import msg


class TestCommandManager(unittest.TestCase):
    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.command_manager.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.mock_publishers = MagicMock(spec=Publishers)
        self.mock_state_machine = MagicMock(spec=StateMachineController)
        self.mock_state_machine.current_device_state = "device_state"
        self.mock_state_machine.state = "machine_state"
        self.mock_navigator = MagicMock(spec=Navigator)

        patch_get_timestamp = patch(
            'flight_control.command_interpreter.command_manager.get_timestamp')
        self.get_timestamp = patch_get_timestamp.start()
        self.get_timestamp.return_value = 1234567890123
        self.addCleanup(patch_get_timestamp.stop)

        self.command_manager = CommandManager(self.mock_node,
                                              self.mock_publishers,
                                              self.mock_state_machine,
                                              self.mock_navigator)

        self.available_commands = [
            "send_device_state", "send_drone_state", "send_drone_stream",
            "send_route_progress", "send_drone_status",
            "send_heartbeat", "set_mqtt_client_state", "send_mission_data",
            "start_mission", "wait", "continue", "get_route", "land_now",
            "kill", "trigger_logs_upload", "send_route_parse_error_alert",
        ]

    def test_instance_should_create_class_objects_from_injected_objects_when_initialized(
            self):
        self.assertIsInstance(self.command_manager._node, Node)
        self.assertIsInstance(self.command_manager.state_machine_controller,
                              StateMachineController)
        self.assertIsInstance(self.command_manager.publishers, Publishers)

    def test_instance_should_have_all_commands_available_when_initialized(
            self):
        self.assertEqual(len(self.available_commands),
                         len(self.command_manager._commands)
                         )

        for command in self.available_commands:
            with self.subTest(msg=command):
                output = self.command_manager.is_command_available(command)

                self.assertTrue(output)

    def test_execute_should_call_specified_method_when_available(self):
        for command in self.available_commands:
            with self.subTest(msg=command):
                self.command_manager._commands[command] = Mock()

                self.command_manager.execute(command,
                                             'some_arg', 'another_arg',
                                             some_kwarg=1, another_kwarg=2)

                self.command_manager._commands[command].assert_called_with(
                    'some_arg', 'another_arg', some_kwarg=1, another_kwarg=2)

    def test_execute_should_log_error_when_wrong_command_called(self):
        command = "wrong_command"

        self.command_manager.execute(command)

        self.mock_logger.error.assert_called_once()

    def test_is_command_available_should_return_True_when_command_exists(self):
        command = "send_device_state"

        output = self.command_manager.is_command_available(command)

        self.assertTrue(output)

    def test_is_command_available_should_return_False_when_command_DOESNT_exist(
            self):
        command = "wrong_command"

        output = self.command_manager.is_command_available(command)

        self.assertFalse(output)

    def test_send_drone_state_should_prepare_and_publish_message_when_called(
            self):
        self.command_manager.publishers.flight_control_drone_state = Mock(
            spec=Publisher)
        state = "drone_state"
        expected_message = msg.FlightControlSendDroneState(
            timestamp=self.get_timestamp(),
            state=state
        )

        self.command_manager._send_drone_state(state)

        self.command_manager.publishers.flight_control_drone_state.publish.assert_called_once_with(
            expected_message)

    def test_send_drone_stream_should_prepare_and_publish_message_when_called(
            self):
        self.command_manager.publishers.flight_control_drone_stream = Mock(
            spec=Publisher)
        stream_type = "stream_type"
        expected_message = msg.FlightControlSendDroneStream(
            timestamp=self.get_timestamp(),
            stream_type=stream_type
        )

        self.command_manager._send_drone_stream(stream_type)

        self.command_manager.publishers.flight_control_drone_stream.publish.assert_called_once_with(
            expected_message)

    def test_send_device_state_should_prepare_and_publish_message_and_update_device_state_when_called(
            self):
        self.command_manager.publishers.flight_control_device_state = Mock(
            spec=Publisher)
        new_state = "new_state"
        expected_message = msg.FlightControlSendDeviceState(
            timestamp=self.get_timestamp(),
            old_state=self.command_manager.state_machine_controller.current_device_state,
            new_state=new_state
        )

        self.command_manager._send_device_state(new_state, old_state=None)

        self.command_manager.publishers.flight_control_device_state.publish.assert_called_once_with(
            expected_message)
        self.assertEqual(
            self.command_manager.state_machine_controller.current_device_state,
            new_state)

    def test_send_route_progress_should_prepare_and_publish_message_when_called(
            self):
        self.command_manager.publishers.flight_control_progress = Mock(
            spec=Publisher)
        self.mock_navigator.progress = Mock()
        self.mock_navigator.progress.next_point_id = 2
        self.mock_navigator.progress.next_point_route_id = 0
        self.mock_navigator.progress.progress = 0.12

        expected_message = msg.FlightControlProgress(
            timestamp=self.get_timestamp(),
            next_point=self.mock_navigator.progress.next_point_id,
            next_point_route_id=self.mock_navigator.progress.next_point_route_id,
            progress=self.mock_navigator.progress.progress
        )

        self.command_manager._send_route_progress()

        self.command_manager.publishers.flight_control_progress.publish.assert_called_once_with(
            expected_message)

    def test_send_drone_status_should_prepare_and_publish_message_and_return_status_when_state_changed(
            self):
        self.command_manager.publishers.flight_control_status = Mock(
            spec=Publisher)
        waiting = False

        previous_status = msg.FlightControlStatus(
            timestamp=self.get_timestamp(),
            flight_state="older_state",
            waiting=waiting
        )
        expected_message = msg.FlightControlStatus(
            timestamp=self.get_timestamp(),
            flight_state=self.command_manager.state_machine_controller.state,
            waiting=waiting
        )

        output = self.command_manager._send_drone_status(
            previous_status=previous_status,
            current_flag_state=waiting)

        self.command_manager.publishers.flight_control_status.publish.assert_called_once_with(
            expected_message)
        self.assertEqual(expected_message, output)

    def test_send_drone_status_should_NOT_publish_message_and_return_previous_status_when_state_DIDNT_change(
            self):
        self.command_manager.publishers.flight_control_status = Mock(
            spec=Publisher)
        waiting = False

        previous_status = msg.FlightControlStatus(
            timestamp=self.get_timestamp(),
            flight_state=self.command_manager.state_machine_controller.state,
            waiting=waiting
        )

        output = self.command_manager._send_drone_status(
            previous_status=previous_status,
            current_flag_state=waiting)

        self.command_manager.publishers.flight_control_status.publish.assert_not_called()
        self.assertEqual(previous_status, output)

    def test_send_heartbeat_should_publish_message_with_timestamp_when_called(
            self):
        self.command_manager.publishers.flight_control_heartbeat = Mock(
            spec=Publisher)

        expected_message = std_msg.UInt64(data=self.get_timestamp())

        self.command_manager._send_heartbeat()

        self.command_manager.publishers.flight_control_heartbeat.publish.assert_called_once_with(
            expected_message)

    def test_start_mission_should_publish_message_when_called(self):
        self.command_manager.publishers.flight_control_start_mission = Mock()
        mock_message = Mock(spec=std_msg.Empty)

        self.command_manager._start_mission()

        self.command_manager.publishers.flight_control_start_mission.publish.assert_called_once_with(
            mock_message)

    def test_continue_should_publish_message_when_called(self):
        self.command_manager.publishers.flight_control_continue = Mock()
        mock_message = Mock(spec=std_msg.Empty)

        self.command_manager._continue()

        self.command_manager.publishers.flight_control_continue.publish.assert_called_once_with(
            mock_message)

    def test_wait_should_publish_message_when_called(self):
        self.command_manager.publishers.flight_control_wait = Mock()
        mock_message = Mock(spec=std_msg.Empty)

        self.command_manager._wait()

        self.command_manager.publishers.flight_control_wait.publish.assert_called_once_with(
            mock_message)

    def test_get_route_should_send_request_route_message_when_called(self):
        self.command_manager.publishers.flight_control_request_route = Mock()
        message = msg.FlightControlRequestRoute(timestamp=self.get_timestamp(),
                                                route_id="route_id")

        self.command_manager._get_route(route_id="route_id")

        self.command_manager.publishers.flight_control_request_route.publish.assert_called_once_with(
            message)

    def test_set_mqtt_client_state_should_send_message_with_connected_state_when_called(
            self):
        state = True
        self.command_manager.publishers.flight_control_hangar_connection = Mock()
        msg = std_msg.Bool(data=state)

        self.command_manager._set_mqtt_client_state(connected=state)

        self.command_manager.publishers.flight_control_hangar_connection.publish.assert_called_once_with(
            msg)

    @patch('flight_control.command_interpreter.command_manager.json')
    def test_send_mission_data_should_call_proper_publisher_when_called(
            self, mock_json):
        mock_json.dumps.return_value = "mission_data"
        expected_msg = std_msg.String(data="mission_data")

        self.mock_publishers.route_publishers = {
            MissionTypes.NORMAL: Mock(spec=Publisher),
            MissionTypes.RTH: Mock(spec=Publisher),
            MissionTypes.EMERGENCY: Mock(spec=Publisher),
        }

        cases = [MissionTypes.NORMAL, MissionTypes.RTH, MissionTypes.EMERGENCY]

        for mission in cases:
            with self.subTest():
                self.command_manager._send_mission_data(mission=mission)

                self.mock_publishers.route_publishers[mission].publish.assert_called_with(
                    expected_msg)

    def test_land_now_should_send_ROS_message_when_called(self):
        self.command_manager.publishers.flight_control_land_now = Mock()
        msg = std_msg.Empty()

        self.command_manager._land_now()

        self.command_manager.publishers.flight_control_land_now.publish.assert_called_once_with(
            msg)

    def test_kill_should_send_ROS_message_when_called(self):
        self.command_manager.publishers.flight_control_emergency_kill = Mock()
        msg = std_msg.Empty()

        self.command_manager._kill()

        self.command_manager.publishers.flight_control_emergency_kill.publish.assert_called_once_with(
            msg)

    def test_send_route_parse_error_alert_should_send_alert_when_called(self):
        self.command_manager.publishers.flight_control_send_alert = Mock()
        message = msg.SendAlert(timestamp=self.get_timestamp(),
                                code=AlertCodes.ROUTE_PARSE_ERROR.value)

        self.command_manager._send_route_parse_error_alert()

        self.command_manager.publishers.flight_control_send_alert.publish.assert_called_once_with(
            message)
