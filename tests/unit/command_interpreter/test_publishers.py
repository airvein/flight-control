"""test_publishers.py

Tests for ROS Publishers for internal communication with other ROS modules in drone in module
Flight Control

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, call

from rclpy.node import Node
from std_msgs import msg as std_msg

from drone_types import msg
from flight_control.command_interpreter.publishers import Publishers
from flight_control.settings import PUBLISHERS_QOS_PROFILE


class TestsPublishers(unittest.TestCase):
    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)

        self.publishers = Publishers(self.mock_node)

    def test_publishers_should_be_created_with_specific_arguments(self):
        calls = [
            # region DATA STREAMS
            call(msg.FlightControlStatus, "/flight_control/status",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(msg.FlightControlProgress, "/flight_control/progress",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(msg.FlightControlSendDeviceState,
                 "/flight_control/device_state",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(msg.FlightControlSendDroneState,
                 "/flight_control/drone_state",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(msg.FlightControlSendDroneStream,
                 "/flight_control/drone_stream",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.UInt64, "/flight_control/heartbeat",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(msg.SendAlert, "/send_alert",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            # endregion

            # region COMMAND PUBLISHERS
            call(msg.FlightControlRequestRoute,
                 "/flight_control/request_route",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.Bool, "/flight_control/hangar_connection_control",
                 qos_profile=PUBLISHERS_QOS_PROFILE),

            call(std_msg.Empty, "/flight_control/trigger_logs_upload",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.Empty, "/flight_control/land_now",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.Empty, "/flight_control/emergency_kill",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.Empty, "/flight_control/wait",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.Empty, "/flight_control/continue",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.Empty, "/flight_control/start_mission",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            # endregion

            # region NAVIGATOR ROUTE PUBLISHERS
            call(std_msg.String, "/flight_control/route_normal",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.String, "/flight_control/route_emergency",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            call(std_msg.String, "/flight_control/route_rth",
                 qos_profile=PUBLISHERS_QOS_PROFILE),
            # endregion

        ]

        self.mock_node.create_publisher.assert_has_calls(calls, any_order=True)
        self.assertEqual(len(calls), self.mock_node.create_publisher.call_count)
