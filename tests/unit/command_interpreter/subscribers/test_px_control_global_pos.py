"""test_px_control_ekf_status.py

Tests for /px_control/global_pos subscriber

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.px_control_global_pos import \
    PxControlGlobalPos
from flight_control.commander.telemetry import Telemetry


class TestPxControlGlobalPos(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.global_pos = PxControlGlobalPos(self.mock_node, self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.PxControlGlobalPos, "/px_control/global_pos",
            self.global_pos.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.PxControlGlobalPos(
            latitude=1.122,
            longitude=2.221,
            altitude=3.3,
            eph=0,
            epv=0,
            num_sats=0
        )

        self.global_pos.handle(expected_message)

        self.assertEqual(expected_message, self.telemetry.global_pos)
