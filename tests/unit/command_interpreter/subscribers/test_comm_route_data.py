"""test_comm_return_data.py

Tests for Communication subscriber dealing with route_data command

Copyright 20120 Cervi Robotics
"""
import json

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.comm_route_data import \
    CommRouteData
from flight_control.commander.telemetry import Telemetry


class TestCommRouteData(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.comm_route_data = CommRouteData(
            self.mock_node, self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.CommRouteData, "/communication/route_data",
            self.comm_route_data.handle, qos_profile=10
        )

    def test_handle_should_save_data_in_telemetry_on_callback(self):
        data = json.dumps({"data": "some_json_data"})
        input_message = msg.CommRouteData(data=data)

        self.comm_route_data.handle(input_message)

        self.assertEqual(self.telemetry.route_data, input_message.data)
