"""test_sensors_button_press.py

Tests for Sensors subscriber dealing with Button Press

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.sensors_button_press import \
    SensorsButtonPress
from flight_control.commander.telemetry import Telemetry


class TestSensorsButtonPress(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.sensors = Mock()

        self.sensors_button_press = SensorsButtonPress(
            self.mock_node, self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.SensorsButtonPress, "/sensors/button_press",
            self.sensors_button_press.handle, qos_profile=10
        )

    def test_handle_should_save_sensor_state_to_telemetry_when_called(self):
        expected_message = msg.SensorsButtonPress(
            shutdown=True
        )

        self.sensors_button_press.handle(expected_message)

        self.assertEqual(
            self.telemetry.sensors.button_press, expected_message)
