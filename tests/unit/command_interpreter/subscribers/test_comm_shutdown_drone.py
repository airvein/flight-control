"""test_comm_shutdown_drone.py

Tests for Communication subscriber dealing with ShutdownDrone

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from std_msgs import msg
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError

from flight_control.command_interpreter.subscribers.comm_shutdown_drone import \
    CommShutdownDrone
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommShutdownDrone(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_shutdown_drone.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.system_shutdown = MagicMock()

        self.comm_shutdown_drone = CommShutdownDrone(
            self.mock_node, self.executor, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/shutdown_drone",
            self.comm_shutdown_drone.handle, qos_profile=10
        )

    def test_handle_should_call_shutdown_drone_state_change_trigger(self):
        input_message = msg.Bool(data=True)

        self.comm_shutdown_drone.handle(input_message)

        self.executor.create_task.assert_called_once_with(
            self.state_machine_controller.system_shutdown)

    def test_handle_should_catch_Machine_Error_and_print_error(self):
        input_message = msg.Bool(data=True)
        self.executor.create_task.side_effect = [MachineError("Error text")]

        self.comm_shutdown_drone.handle(input_message)

        self.mock_logger.error.assert_called()
