"""test_comm_init.py

Tests for Communication subscriber dealing with Init

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, Mock, patch

from rclpy.executors import SingleThreadedExecutor
from rclpy.impl.rcutils_logger import RcutilsLogger
from rclpy.node import Node
from transitions import MachineError

from drone_types import msg
from flight_control.command_interpreter.subscribers.comm_init import CommInit
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommInit(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_init.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.init_flight = MagicMock()

        self.comm_init = CommInit(
            self.mock_node, self.executor, self.state_machine_controller)
        self.comm_init.logger = MagicMock(spec=RcutilsLogger)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.CommInit, "/communication/init", self.comm_init.handle,
            qos_profile=10
        )

    def test_handle_should_call_init_flight_state_change_trigger(self):
        input_message = msg.CommInit(
            route_id="123", flight_id="123", cargo=False)
        self.state_machine_controller.init_flight = Mock()

        self.comm_init.handle(input_message)

        self.state_machine_controller.init_flight.assert_called_with(
            route_id="123", flight_id="123", cargo=False)

    def test_handle_should_handle_Machine_Error_and_log_error(self):
        input_message = msg.CommInit(
            route_id="123", flight_id="123", cargo=False)
        self.state_machine_controller.init_flight = Mock(
            side_effect=MachineError("Error"))

        self.comm_init.handle(input_message)

        self.mock_logger.error.assert_called()
