"""test_comm_wait.py

Tests for Communication subscriber dealing with cloud "wait" command

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from std_msgs import msg
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node

from flight_control.commander.state_data import get_state_data
from flight_control.command_interpreter.subscribers.comm_wait import CommWait
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander import StateDataNames, StateNames


class TestCommWait(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_wait.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(
            spec=StateMachineController, state=StateNames.CRUISE,
            state_data=get_state_data(), command_status={},
            current_device_state="device_state")

        self.comm_wait = CommWait(
            self.mock_node, self.executor, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/wait", self.comm_wait.handle,
            qos_profile=10)

    def test_can_trigger_wait_should_return_tuple_when_called(self):
        output = self.comm_wait._can_trigger_wait()

        self.assertIsInstance(output, tuple)

    def test_can_trigger_wait_should_return_proper_value_when_conditions_are_met(self):
        cases = [
            {"state": StateNames.CRUISE,
             "waiting_flag": False,
             "waiting_time_exceeded": False,
             "result": True},
            {"state": "wrong_state",
             "waiting_flag": False,
             "waiting_time_exceeded": False,
             "result": False},
            {"state": StateNames.CRUISE,
             "waiting_flag": True,
             "waiting_time_exceeded": False,
             "result": False},
            {"state": StateNames.CRUISE,
             "waiting_flag": False,
             "waiting_time_exceeded": True,
             "result": False},
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.state_machine_controller.state = case['state']
                self.state_machine_controller.state_data[StateDataNames.FLAGS][
                    StateDataNames.Flag.WAITING] = case['waiting_flag']
                self.state_machine_controller.state_data[StateDataNames.FLAGS][
                    StateDataNames.Flag.WAITING_TIME_EXCEEDED] = case[
                    'waiting_time_exceeded']

                output = self.comm_wait._can_trigger_wait()

                self.assertEqual(output[0], case['result'])
                if output[0]:
                    self.assertIsNone(output[1])
                else:
                    self.assertIsInstance(output[1], str)

    def test_handle_should_set_WAITING_flag_when_trigger_is_possible(self):
        self.comm_wait._can_trigger_wait = Mock(return_value=(True, None))
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "wait", "success": True}

        self.comm_wait.handle(input_message)

        self.assertTrue(self.state_machine_controller.state_data[
                            StateDataNames.FLAGS][StateDataNames.Flag.WAITING])
        self.assertDictEqual(self.state_machine_controller.command_status,
                             expected_cloud_command_status)

    def test_handle_should_NOT_set_WAITING_flag_and_log_error_when_trigger_is_NOT_possible(
            self):
        self.state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = False
        self.comm_wait._can_trigger_wait = Mock(return_value=(False, "reason"))
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "wait", "success": False}

        self.comm_wait.handle(input_message)

        self.mock_logger.error.assert_called_once()
        self.assertFalse(
            self.state_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.WAITING])
        self.assertDictEqual(self.state_machine_controller.command_status,
                             expected_cloud_command_status)
