"""test_comm_kill.py

Tests for Communication subscriber dealing with `kill` cloud command

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, Mock, patch

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from std_msgs import msg
from transitions import MachineError

from flight_control.command_interpreter.subscribers.comm_kill import \
    CommKill
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommKill(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_kill.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.command_status = {}

        self.comm_kill = CommKill(
            self.mock_node, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/kill", self.comm_kill.handle,
            qos_profile=10
        )

    def test_handle_should_try_to_trigger_drone_kill_transition_and_update_cloud_status(
            self):
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "kill",
                                         "success": True}
        self.state_machine_controller.drone_kill = Mock()

        self.comm_kill.handle(input_message)

        self.state_machine_controller.drone_kill.assert_called_once()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)

    def test_handle_should_catch_MachineError_and_update_cloud_status(self):
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "kill",
                                         "success": False}
        self.state_machine_controller.drone_kill = Mock(
            side_effect=MachineError("Error"))

        self.comm_kill.handle(input_message)

        self.mock_logger.error.assert_called()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)
