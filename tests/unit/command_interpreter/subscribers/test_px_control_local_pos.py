"""test_px_control_local_pos.py

Tests for PxControl subscriber dealing with LocalPos

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.px_control_local_pos import \
    PxControlLocalPos
from flight_control.commander.telemetry import Telemetry


class TestPxControlLocalPos(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.px_control_local_pos = PxControlLocalPos(self.mock_node,
                                                      self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.PxControlLocalPos, "/px_control/local_pos",
            self.px_control_local_pos.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.PxControlLocalPos(
            timestamp=1234,
            x=2.0,
            y=3.0,
            z=4.0
        )

        self.px_control_local_pos.handle(expected_message)

        self.assertEqual(expected_message, self.telemetry.local_pos)
