"""test_comm_emergency_land.py

Tests for Communication subscriber dealing with EmergencyLand

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from rclpy.node import Node
from transitions import MachineError

from drone_types import msg
from flight_control.command_interpreter.subscribers.comm_emergency_land import \
    CommEmergencyLand
from flight_control.commander import Telemetry
from flight_control.commander.state_machine_controller import \
    StateMachineController
from tests.unit.fixtures.route_data import RouteData


class TestCommEmergencyLand(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_emergency_land.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)

        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.route = RouteData()

        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.emergency_land = Mock()
        self.state_machine_controller.command_status = {}

        self.comm_emergency_land = CommEmergencyLand(
            self.mock_node, self.state_machine_controller, self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.CommEmergencyLand, "/communication/emergency_land",
            self.comm_emergency_land.handle, qos_profile=10
        )

    def test_handle_should_trigger_emergency_land_when_route_id_is_correct(self):
        expected_cloud_command_status = {"name": "emergency_land",
                                         "success": True}
        input_message = msg.CommEmergencyLand(
            timestamp=12345, emergency_route=5)

        self.comm_emergency_land.handle(input_message)

        self.state_machine_controller.emergency_land.assert_called_once_with(
            emergency_route_id=input_message.emergency_route)
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)

    def test_handle_should_log_error_when_catched_MachineError(self):
        self.state_machine_controller.emergency_land.side_effect = [
            MachineError("Error text")]
        expected_cloud_command_status = {"name": "emergency_land",
                                         "success": False}
        input_message = msg.CommEmergencyLand(
            timestamp=12345, emergency_route=5)

        self.comm_emergency_land.handle(input_message)

        self.mock_logger.error.assert_called_once()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)

    def test_handle_should_log_error_when_route_id_is_incorrect(self):
        expected_cloud_command_status = {"name": "emergency_land",
                                         "success": False}
        input_message = msg.CommEmergencyLand(
            timestamp=12345, emergency_route=12345)

        self.comm_emergency_land.handle(input_message)

        self.mock_logger.error.assert_called_once()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)
