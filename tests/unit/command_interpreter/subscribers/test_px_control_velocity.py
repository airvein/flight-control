"""test_px_control_velocity.py

Tests for PxControl subscriber dealing with Velocity

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.px_control_velocity import \
    PxControlVelocity
from flight_control.commander.telemetry import Telemetry


class TestPxControlVelocity(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.px_control_velocity = PxControlVelocity(self.mock_node,
                                                     self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.PxControlVelocity, "/px_control/velocity",
            self.px_control_velocity.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.PxControlVelocity(
            timestamp=1234,
            vx=3.0,
            vy=4.0,
            vz=5.0
        )

        self.px_control_velocity.handle(expected_message)

        self.assertEqual(
            expected_message.timestamp, self.telemetry.velocity.timestamp)
        self.assertEqual(
            expected_message.vx, self.telemetry.velocity.vx)
        self.assertEqual(
            expected_message.vy, self.telemetry.velocity.vy)
        self.assertEqual(
            expected_message.vz, self.telemetry.velocity.vz)
