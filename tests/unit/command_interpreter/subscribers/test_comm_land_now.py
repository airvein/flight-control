"""test_comm_land_now.py

Tests for Communication subscriber dealing with land_now command

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch

from transitions import MachineError
from std_msgs import msg
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node

from flight_control.command_interpreter.subscribers.comm_land_now import \
    CommLandNow
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommLand(unittest.TestCase):
    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_land_now.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.land_now = MagicMock()
        self.state_machine_controller.command_status = {}

        self.comm_land_now = CommLandNow(
            self.mock_node, self.executor, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/land_now", self.comm_land_now.handle,
            qos_profile=10
        )

    def test_handler_should_trigger_machine_state_transition(self):
        msg_ = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "land_now",
                                         "success": True}
        self.comm_land_now.handle(msg_)

        self.state_machine_controller.land_now.assert_called_once()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)

    def test_handler_should_catch_MachineError(self):
        msg_ = msg.Bool(data=True)
        self.state_machine_controller.land_now = MagicMock(
            side_effect=[MachineError("Error text")])
        expected_cloud_command_status = {"name": "land_now",
                                         "success": False}

        self.comm_land_now.handle(msg_)

        self.mock_logger.error.assert_called()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)
