"""test_comm_service_mode.py

Tests for Communication subscriber dealing with ServiceMode

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock

from rclpy.impl.rcutils_logger import RcutilsLogger
from std_msgs import msg
from rclpy.node import Node
from transitions import MachineError

from flight_control.command_interpreter.subscribers.comm_service_mode import \
    CommServiceMode
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommServiceMode(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.command_status = {}
        self.state_machine_controller.service_mode = MagicMock()
        self.state_machine_controller.exit_service_mode = MagicMock()

        self.service_mode = CommServiceMode(
            self.mock_node, self.state_machine_controller)
        self.service_mode.logger = MagicMock(spec=RcutilsLogger, error=Mock())

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/service_mode",
            self.service_mode.handle)

    def test_handle_should_call_service_mode_transition_when_message_data_is_True(self):
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "service_mode",
                                         "success": True}

        self.service_mode.handle(input_message)

        self.state_machine_controller.service_mode.assert_called_once()
        self.assertDictEqual(self.state_machine_controller.command_status,
                             expected_cloud_command_status)

    def test_handle_should_call_exit_service_mode_transition_when_message_data_is_False(
            self):
        input_message = msg.Bool(data=False)
        expected_cloud_command_status = {"name": "service_mode",
                                         "success": True}

        self.service_mode.handle(input_message)

        self.state_machine_controller.service_mode.assert_not_called()
        self.state_machine_controller.exit_service_mode.assert_called_once()
        self.assertDictEqual(self.state_machine_controller.command_status,
                             expected_cloud_command_status)

    def test_handle_should_catch_Machine_Error_and_print_error(self):
        input_message = msg.Bool(data=True)
        self.state_machine_controller.service_mode.side_effect = [MachineError("Error text")]
        expected_cloud_command_status = {"name": "service_mode",
                                         "success": False}

        self.service_mode.handle(input_message)

        self.service_mode.logger.error.assert_called()
        self.assertDictEqual(self.state_machine_controller.command_status,
                             expected_cloud_command_status)
