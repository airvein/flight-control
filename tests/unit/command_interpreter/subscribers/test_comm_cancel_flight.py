"""test_comm_cancel_flight.py

Tests for Communication subscriber dealing with cancel_flight cloud command

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from std_msgs import msg
from transitions import MachineError

from flight_control.command_interpreter.subscribers.comm_cancel_flight import \
    CommFlightCancel
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommStartFlight(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_cancel_flight.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.flight_cancel = MagicMock()
        self.state_machine_controller.command_status = {}

        self.comm_flight_cancel = CommFlightCancel(
            self.mock_node, self.executor, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/flight_cancel",
            self.comm_flight_cancel.handle, qos_profile=10
        )

    def test_handle_should_call_flight_start_state_change_trigger_when_OK(
            self):
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "flight_cancel",
                                         "success": True}

        self.comm_flight_cancel.handle(input_message)

        self.state_machine_controller.flight_cancel.assert_called_once_with()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)

    def test_handle_should_log_error_and_set_success_to_false_when_MachineError(
            self):
        input_message = msg.Bool(data=True)
        self.state_machine_controller.flight_cancel.side_effect = [
            MachineError("Error text")]
        expected_cloud_command_status = {"name": "flight_cancel",
                                         "success": False}

        self.comm_flight_cancel.handle(input_message)

        self.mock_logger.error.assert_called()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)
