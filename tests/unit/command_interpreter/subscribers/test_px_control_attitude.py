"""test_px_control_attitude.py

Tests for PxControl subscriber dealing with attitude

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.px_control_attitude import \
    PxControlAttitude
from flight_control.commander.telemetry import Telemetry


class TestPxControlAttitude(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.px_control_attitude = PxControlAttitude(
            self.mock_node, self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.PxControlAttitude, "/px_control/attitude",
            self.px_control_attitude.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.PxControlAttitude(
            timestamp=1234,
            q_w=1.0,
            q_x=2.0,
            q_y=3.0,
            q_z=4.0,
            roll_speed=20.0,
            pitch_speed=15.0,
            yaw_speed=10.0
        )

        self.px_control_attitude.handle(expected_message)

        cases = [
            {
                "expected": expected_message.timestamp,
                "actual": self.telemetry.attitude.timestamp
            },
            {
                "expected": expected_message.q_w,
                "actual": self.telemetry.attitude.q_w
            },
            {
                "expected": expected_message.q_x,
                "actual": self.telemetry.attitude.q_x
            },
            {
                "expected": expected_message.q_y,
                "actual": self.telemetry.attitude.q_y
            },
            {
                "expected": expected_message.q_z,
                "actual": self.telemetry.attitude.q_z
            },
            {
                "expected": expected_message.roll_speed,
                "actual": self.telemetry.attitude.roll_speed
            },
            {
                "expected": expected_message.pitch_speed,
                "actual": self.telemetry.attitude.pitch_speed
            },
            {
                "expected": expected_message.yaw_speed,
                "actual": self.telemetry.attitude.yaw_speed
            },
        ]
        for case in cases:
            with self.subTest(msg=case):
                self.assertEqual(case["actual"], case["expected"])
