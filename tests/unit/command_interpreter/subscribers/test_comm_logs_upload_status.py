"""test_comm_logs_upload_status.py

Tests for Communication subscriber dealing with logs_upload_status

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.comm_logs_upload_status import \
    CommunicationLogsUploadStatus
from flight_control.commander.telemetry import Telemetry


class TestCommunicationLogsUploadStatus(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)

        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.logs_upload_status = MagicMock(spec=msg.CommLogsUploadStatus)

        self.comm_logs_upload_status = CommunicationLogsUploadStatus(
            self.mock_node, self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.CommLogsUploadStatus, "/communication/logs_upload_status",
            self.comm_logs_upload_status.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.CommLogsUploadStatus(upload_status=2)

        self.comm_logs_upload_status.handle(expected_message)

        self.assertEqual(expected_message.upload_status,
                         self.telemetry.logs_upload_status.upload_status)

