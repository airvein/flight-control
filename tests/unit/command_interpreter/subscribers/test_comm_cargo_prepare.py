"""test_comm_cargo_prepare.py

Tests for Communication subscriber dealing with CargoPrepare

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from std_msgs import msg
from transitions import MachineError

from flight_control.command_interpreter.subscribers.comm_cargo_prepare import \
    CommCargoPrepare
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommCargoPrepare(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_cargo_prepare.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.cargo_prepared = Mock()

        self.comm_cargo_prepare = CommCargoPrepare(
            self.mock_node, self.executor, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/cargo_prepare",
            self.comm_cargo_prepare.handle, qos_profile=10
        )

    def test_handle_should_create_executor_task(self):
        input_message = msg.Bool(data=True)

        self.comm_cargo_prepare.handle(input_message)

        self.executor.create_task.assert_called_once_with(
            self.state_machine_controller.cargo_prepared)

    def test_handle_should_catch_error_and_print_error_message_in_logger_when_MachineError(
            self):
        input_message = msg.Bool(data=True)
        self.executor.create_task.side_effect = MachineError("Some error text")

        self.comm_cargo_prepare.handle(input_message)

        self.mock_logger.error.assert_called_once()
