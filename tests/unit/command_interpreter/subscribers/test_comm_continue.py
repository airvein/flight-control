"""test_comm_continue.py

Tests for Communication subscriber dealing with cloud `continue` command

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from std_msgs import msg
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node

from flight_control.commander.state_data import get_state_data
from flight_control.command_interpreter.subscribers.comm_continue import \
    CommContinue
from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander import StateNames, StateDataNames


class TestCommContinue(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_continue.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(
            spec=StateMachineController, state=StateNames.CRUISE,
            state_data=get_state_data(), command_status={},
            current_device_state="device_state")

        self.comm_continue = CommContinue(
            self.mock_node, self.executor, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/continue", self.comm_continue.handle,
            qos_profile=10)

    def test_can_trigger_continue_should_return_tuple_when_called(self):
        output = self.comm_continue._can_trigger_continue()

        self.assertIsInstance(output, tuple)

    def test_can_trigger_continue_should_return_proper_values_depend_on_case(self):
        cases = [
            {"state": "not_approach_cruise_wait",
             "waiting_flag": True,
             "result": True, "reason": None},
            {"state": "not_approach_cruise_wait",
             "waiting_flag": False,
             "result": False, "reason": str},
            {"state": StateNames.APPROACH_CRUISE_WAIT,
             "waiting_flag": True,
             "result": False, "reason": str},
            {"state": StateNames.APPROACH_CRUISE_WAIT,
             "waiting_flag": False,
             "result": False, "reason": str},
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.state_machine_controller.state = case['state']
                self.state_machine_controller.state_data[StateDataNames.FLAGS][
                    StateDataNames.Flag.WAITING] = case['waiting_flag']

                output = self.comm_continue._can_trigger_continue()

                self.assertEqual(output[0], case['result'])

    def test_handle_should_remove_WAITING_flag_when_it_is_allowed(self):
        self.comm_continue._can_trigger_continue = Mock(return_value=(True, None))
        input_message = msg.Bool(data=True)

        expected_cloud_command_status = {"name": "continue",
                                         "success": True}

        self.comm_continue.handle(input_message)

        self.assertFalse(self.state_machine_controller.state_data[
                             StateDataNames.FLAGS][StateDataNames.Flag.WAITING]
                         )
        self.assertDictEqual(self.state_machine_controller.command_status,
                             expected_cloud_command_status)

    def test_handle_should_log_error_when_it_is_not_possible_to_trigger_command(self):
        self.comm_continue._can_trigger_continue = Mock(return_value=(False, "reason"))
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "continue",
                                         "success": False}

        self.comm_continue.handle(input_message)

        self.mock_logger.error.assert_called_once()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)
