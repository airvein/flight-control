"""test_px_control_battery.py

Tests for PxControl subscriber dealing with Battery

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.px_control_battery import \
    PxControlBattery
from flight_control.commander.telemetry import Telemetry


class TestPxControlBattery(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.px_control_battery = PxControlBattery(self.mock_node,
                                                   self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.PxControlBattery, "/px_control/battery",
            self.px_control_battery.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.PxControlBattery(
            timestamp=1234,
            voltage=3.0,
            current=4.0,
            remaining=5.0
        )

        self.px_control_battery.handle(expected_message)

        self.assertEqual(
            expected_message.timestamp, self.telemetry.battery.timestamp)
        self.assertEqual(
            expected_message.voltage, self.telemetry.battery.voltage)
        self.assertEqual(
            expected_message.current, self.telemetry.battery.current)
        self.assertEqual(
            expected_message.remaining, self.telemetry.battery.remaining)
