"""test_comm_land.py

Tests for Communication subscriber dealing with Land

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch

from std_msgs import msg
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from transitions import MachineError

from flight_control.command_interpreter.subscribers.comm_land import CommLand
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommLand(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_land.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.land = MagicMock()
        self.state_machine_controller.command_status = {}

        self.comm_land = CommLand(
            self.mock_node, self.executor, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/land", self.comm_land.handle,
            qos_profile=10)

    def test_handle_should_call_land_state_change_trigger_when_called(self):
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "land",
                                         "success": True}

        self.comm_land.handle(input_message)

        self.state_machine_controller.land.assert_called_once()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)

    def test_handle_should_handle_Machine_Error_and_log_error(self):
        input_message = msg.Bool(data=True)
        self.state_machine_controller.land.side_effect = [
            MachineError("Error text")]
        expected_cloud_command_status = {"name": "land",
                                         "success": False}

        self.comm_land.handle(input_message)

        self.mock_logger.error.assert_called()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)
