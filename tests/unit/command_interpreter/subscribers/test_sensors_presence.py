"""test_sensors_presence.py

Tests for Sensors subscriber dealing with SensorsPresence

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.sensors_presence import \
    SensorsPresence
from flight_control.commander.telemetry import Telemetry


class TestSensorsPresence(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.sensors = Mock()

        self.sensors_presence = SensorsPresence(self.mock_node, self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.SensorsPresence, "/sensors/presence",
            self.sensors_presence.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.SensorsPresence(
            cargo=True,
            battery=True
        )

        self.sensors_presence.handle(expected_message)

        self.assertEqual(expected_message, self.telemetry.sensors.presence)
