"""test_comm_return_home.py

Tests for Communication subscriber dealing with Start Flight

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from rclpy.executors import SingleThreadedExecutor
from rclpy.impl.rcutils_logger import RcutilsLogger
from rclpy.node import Node
from std_msgs import msg
from transitions import MachineError

from flight_control.command_interpreter.subscribers.comm_return_home import \
    CommReturnHome
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestCommReturnHome(unittest.TestCase):

    def setUp(self) -> None:
        patcher_logger = patch(
            'flight_control.command_interpreter.subscribers.comm_return_home.logger')
        self.mock_logger = patcher_logger.start()
        self.addCleanup(patcher_logger.stop)

        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=SingleThreadedExecutor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)
        self.state_machine_controller.return_home = MagicMock()
        self.state_machine_controller.command_status = {}

        self.comm_return_home = CommReturnHome(
            self.mock_node, self.executor, self.state_machine_controller)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.Bool, "/communication/return_home",
            self.comm_return_home.handle, qos_profile=10
        )

    def test_handle_should_call_return_home_state_change_trigger(self):
        input_message = msg.Bool(data=True)
        expected_cloud_command_status = {"name": "return_home",
                                         "success": True}

        self.comm_return_home.handle(input_message)

        self.state_machine_controller.return_home.assert_called_once()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)

    def test_handle_should_catch_Machine_Error_and_print_error(self):
        self.state_machine_controller.return_home.side_effect = [
            MachineError("Error text")]
        expected_cloud_command_status = {"name": "return_home",
                                         "success": False}
        input_message = msg.Bool(data=True)

        self.comm_return_home.handle(input_message)

        self.mock_logger.error.assert_called()
        self.assertDictEqual(
            self.state_machine_controller.command_status,
            expected_cloud_command_status)
