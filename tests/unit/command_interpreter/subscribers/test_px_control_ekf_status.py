"""test_px_control_ekf_status.py

Tests for /px_control/ekf_status subscriber

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.px_control_ekf_status import \
    PxControlEKFStatus
from flight_control.commander.telemetry import Telemetry


class TestPxControlEKFStatus(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.px_ekf_status = PxControlEKFStatus(self.mock_node, self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.PxControlEKFStatus, "/px_control/ekf_status",
            self.px_ekf_status.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.PxControlEKFStatus(
            timestamp=12345,
            flags=0,
            velocity_variance=11.1,
            pos_horiz_variance=22.2,
            pos_vert_variance=33.3,
            compass_variance=44.4
        )

        self.px_ekf_status.handle(expected_message)

        self.assertEqual(expected_message, self.telemetry.ekf_status)
