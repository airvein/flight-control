"""test_px_control_status.py

Tests for PxControl subscriber dealing with status

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from drone_types import msg
from flight_control.command_interpreter.subscribers.px_control_status import \
    PxControlStatus
from flight_control.commander.telemetry import Telemetry


class TestPxControlStatus(unittest.TestCase):
    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.px_control_status = PxControlStatus(self.mock_node,
                                                 self.telemetry)

    def test_create_subscription_should_be_called_when_initialized(self):
        self.mock_node.create_subscription.assert_called_once_with(
            msg.PxControlStatus, "/px_control/status",
            self.px_control_status.handle, qos_profile=10
        )

    def test_handle_should_save_message_to_telemetry_when_called(self):
        expected_message = msg.PxControlStatus(
            timestamp=1234,
            flight_state=1,
            connection_state=1,
            arm_state=3,
            flight_mode="AUTO"
        )

        self.px_control_status.handle(expected_message)

        self.assertEqual(expected_message, self.telemetry.px_control_status)
