"""test_abstract_service.py

Tests for Abstract Service for services in flight control module

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node
from rclpy.executors import Executor

from flight_control.command_interpreter.services.abstract_service import \
    AbstractService
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestsAbstractService(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.mock_state_machine_controller = MagicMock(
            spec=StateMachineController)
        self.executor = MagicMock(spec=Executor)

        self.abstract_service = AbstractService(
            self.mock_node, self.executor, self.mock_state_machine_controller)

    def test_abstract_service_should_create_class_objects_from_injected_objects_when_initialized(
            self):
        self.assertIsInstance(self.abstract_service.node, Node)
        self.assertIsInstance(
            self.abstract_service.state_machine_controller,
            StateMachineController)
        self.assertIsInstance(self.abstract_service.executor, Executor)

    def test_abstract_service_should_have_name_variable_when_initialized(self):
        self.assertIsNone(self.abstract_service.name)
