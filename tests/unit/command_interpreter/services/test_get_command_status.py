"""test_get_command_status.py

Tests for GetCommandStatus ROS service that pass success/failure of cloud
command execution

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock

from rclpy.node import Node
from rclpy.executors import Executor
from std_srvs import srv

from flight_control.command_interpreter.services.get_command_status import \
    GetCommandStatus
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestGetCommandStatus(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.executor = MagicMock(spec=Executor)
        self.mock_state_machine_controller = MagicMock(
            spec=StateMachineController)

        self.command_status = GetCommandStatus(
            self.mock_node, self.executor, self.mock_state_machine_controller)

        self.request = Mock(spec=srv.Trigger.Request)
        self.response = Mock(spec=srv.Trigger.Response)

    def test_create_service_should_be_called_with_proper_arguments_when_initialized(
            self):
        self.command_status.node.create_service.assert_called_with(
            srv.Trigger, "/flight_control/command_status",
            self.command_status._command_response_callback
        )

    def test_callback_should_clear_status_when_finishes_service(self):
        self.mock_state_machine_controller.command_status = {
            "success": True, "name": "cloud_command"}
        expected_result = {"success": False, "name": ''}

        self.command_status._command_response_callback(
            self.request, self.response)

        self.assertDictEqual(
            self.mock_state_machine_controller.command_status,
            expected_result)

    def test_callback_should_return_result_when_called(self):
        self.mock_state_machine_controller.command_status = {
            "success": True, "name": "cloud_command"}

        response = self.command_status._command_response_callback(
            self.request, self.response)

        self.assertTrue(response.success)
        self.assertEqual(response.message, "cloud_command")
