"""test_services_manager.py

Tests for ROS Services Manager

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node
from rclpy.executors import Executor

from flight_control.command_interpreter.services_manager import ServicesManager
from flight_control.commander.state_machine_controller import \
    StateMachineController


class TestServicesManager(unittest.TestCase):

    def setUp(self) -> None:
        self.mock_node = MagicMock(spec=Node)
        self.mock_executor = MagicMock(spec=Executor)
        self.state_machine_controller = MagicMock(spec=StateMachineController)

        self.services_manager = ServicesManager(
            self.mock_node, self.mock_executor, self.state_machine_controller)

        self.available_services = [
            "command_status"
        ]

    def test_services_manager_should_have_all_services_when_initialized(self):
        self.assertEqual(
            len(self.available_services), len(self.services_manager._services))

    def test_method_services_should_return_dict_of_services_when_called(self):
        output = self.services_manager.services

        self.assertIsInstance(output, dict)

    def test_is_service_registered_should_return_false_when_there_ISNT_service(self):
        output = self.services_manager._is_service_registered('wrong_name')

        self.assertFalse(output)

    def test_is_service_registered_should_return_true_when_there_IS_service(self):
        for service in self.available_services:
            with self.subTest(msg=service):
                output = self.services_manager._is_service_registered(
                    service)

                self.assertTrue(output)
