"""test_state_updater.py

Tests for StateUpdater class which handle with internal changing of states depending
on Drone position on track.

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import patch, MagicMock

from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.telemetry import Telemetry
from flight_control.state_updater.checkers import *
from flight_control.state_updater.state_updater import StateUpdater


class TestStateUpdater(unittest.TestCase):
    def setUp(self) -> None:
        patcher = patch(
            'flight_control.state_updater.checkers.manual_shutdown.GPIO'
        )
        patcher.start()
        self.addCleanup(patcher.stop)
        self.telemetry = Telemetry()
        self.state_machine_controller = MagicMock(spec=StateMachineController)

        self.state_updater = StateUpdater(
            self.telemetry, self.state_machine_controller)

    def test_state_updater_should_have_dict_of_checkers_when_initialized(self):
        self.assertIsInstance(self.state_updater._checkers, dict)

    def test_state_updater_should_create_instances_of_8_checkers_when_initialized(
            self):
        self.assertIsInstance(
            self.state_updater._checkers["approach"], approach.Approach)
        self.assertIsInstance(
            self.state_updater._checkers["exit_waiting"], exit_waiting.ExitWaiting)
        self.assertIsInstance(
            self.state_updater._checkers["cargo"], cargo.Cargo)
        self.assertIsInstance(
            self.state_updater._checkers["cruise"], cruise.Cruise)
        self.assertIsInstance(
            self.state_updater._checkers["drone_landed"],
            drone_landed.DroneLanded)
        self.assertIsInstance(
            self.state_updater._checkers["maneuver"], maneuver.Maneuver)
        self.assertIsInstance(
            self.state_updater._checkers["manual_shutdown"],
            manual_shutdown.ManualShutdown)
        self.assertIsInstance(
            self.state_updater._checkers["route_done"], route_done.RouteDone)
        self.assertIsInstance(
            self.state_updater._checkers["takeoff"], takeoff.Takeoff)
        self.assertIsInstance(
            self.state_updater._checkers["battery_shutdown"],
            battery_shutdown.BatteryShutdown)
        self.assertIsInstance(
            self.state_updater._checkers["cancel_flight_data"],
            cancel_flight_data.CancelFlightData)
        self.assertIsInstance(
            self.state_updater._checkers["battery_land"],
            battery_land.BatteryLand)

    @patch.object(battery_land.BatteryLand, "check")
    @patch.object(exit_waiting.ExitWaiting, "check")
    @patch.object(battery_shutdown.BatteryShutdown, "check")
    @patch.object(takeoff.Takeoff, "check")
    @patch.object(route_done.RouteDone, "check")
    @patch.object(manual_shutdown.ManualShutdown, "check")
    @patch.object(maneuver.Maneuver, "check")
    @patch.object(drone_landed.DroneLanded, "check")
    @patch.object(cruise.Cruise, "check")
    @patch.object(cargo.Cargo, "check")
    @patch.object(approach.Approach, "check")
    @patch.object(cancel_flight_data.CancelFlightData, "check")
    def test_update_should_run_all_checkers_when_called(
            self, mock_cancel_flight_data_checker, mock_approach_checker,
            mock_cargo_checker, mock_cruise_checker, mock_drone_landed_checker,
            mock_maneuver_checker, mock_manual_shutdown_checker,
            mock_route_done_checker, mock_takeoff_checker,
            mock_battery_shutdown_checker, mock_break_waiting_checker,
            mock_battery_land_checker):

        self.state_updater.update()

        mock_approach_checker.assert_called_once()
        mock_cargo_checker.assert_called_once()
        mock_cruise_checker.assert_called_once()
        mock_drone_landed_checker.assert_called_once()
        mock_maneuver_checker.assert_called_once()
        mock_manual_shutdown_checker.assert_called_once()
        mock_route_done_checker.assert_called_once()
        mock_takeoff_checker.assert_called_once()
        mock_battery_shutdown_checker.assert_called_once()
        mock_cancel_flight_data_checker.assert_called_once()
        mock_break_waiting_checker.assert_called_once()
        mock_battery_land_checker.assert_called_once()
