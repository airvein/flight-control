"""test_cargo.py

Tests for Cargo checker class which checks if cargo is present

Copyright 2019 Cervi Robotics
"""

from unittest.mock import MagicMock

from flight_control.state_updater.checkers.cargo import Cargo
from tests.unit.state_updater.checkers.abstract_base_test import AbstractBaseTest


class CargoTest(AbstractBaseTest):
    def setUp(self) -> None:
        self.state_machine_controller.cargo_detected = MagicMock()

        self.cargo = Cargo(self.telemetry, self.state_machine_controller)

    def test_cargo_checker_should_call_cargo_detected_method_when_state_wait_cargo(self):
        self.cargo.state_machine_controller.state = "wait_cargo"
        self.cargo.telemetry.sensors.presence.cargo = True

        output = self.cargo.check()

        self.cargo.state_machine_controller.cargo_detected.assert_called_once()
        self.assertTrue(output)

    def test_cargo_checker_should_return_FALSE_when_state_is_NOT_wait_cargo(self):
        self.cargo.state_machine_controller.state = "wrong_state"

        output = self.cargo.check()

        self.cargo.state_machine_controller.cargo_detected.assert_not_called()
        self.assertFalse(output)

    def test_cargo_checker_should_return_FALSE_when_state_is_NOT_cargo_detected(self):
        self.cargo.state_machine_controller.state = "wait_cargo"
        self.cargo.telemetry.sensors.presence.cargo = False

        output = self.cargo.check()

        self.cargo.state_machine_controller.cargo_detected.assert_not_called()
        self.assertFalse(output)
