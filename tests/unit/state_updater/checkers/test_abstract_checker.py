"""test_abstract_checker.py

Tests for AbstractChecker class which is base class for other checkers

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from flight_control.commander.state_machine_controller import \
    StateMachineController
from flight_control.commander.telemetry import Telemetry
from flight_control.state_updater.checkers.abstract_checker import \
    AbstractChecker


class TestAbstractChecker(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.state_machine_controller = MagicMock(spec=StateMachineController)

        self.abstract_checker = AbstractChecker(
            self.telemetry, self.state_machine_controller)

    def test_abstract_checker_should_have_specified_arguments_when_initialized_correctly(
            self):
        self.assertIsInstance(self.abstract_checker.telemetry, Telemetry)
        self.assertIsInstance(self.abstract_checker.state_machine_controller,
                              StateMachineController)

    def test_abstract_checker_should_have_abstractmethod_check_when_initialized(
            self):
        with self.assertRaises(NotImplementedError) as error:
            self.abstract_checker.check()

            self.assertEqual('Not implemented', str(error.exception))
