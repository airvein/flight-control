"""test_cancel_flight_data.py

Tests for CancelFlightData checker

Copyright 2020 Cervi Robotics
"""
import functools
from unittest.mock import MagicMock

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.cancel_flight_data import CancelFlightData
from tests.unit.state_updater.checkers.abstract_base_test import AbstractBaseTest


class TestCancelFlightData(AbstractBaseTest):
    def setUp(self) -> None:
        self.state_machine_controller.done_flight_cancel = MagicMock(
            spec=functools.partial)

        self.cancel_flight_data = CancelFlightData(
            self.telemetry, self.state_machine_controller)

    def test_checker_should_call_system_shutdown_method_when_in_proper_state(self):
        self.cancel_flight_data.state_machine_controller.state = StateNames.CANCEL_UPLOAD_DATA

        output = self.cancel_flight_data.check()

        self.cancel_flight_data.state_machine_controller.done_flight_cancel.assert_called_once()
        self.assertTrue(output)

    def test_checker_should_NOT_call_system_shutdown_method_when_state_is_invalid(self):
        self.cancel_flight_data.state_machine_controller.state = "wrong_state"

        output = self.cancel_flight_data.check()

        self.cancel_flight_data.state_machine_controller.done_flight_cancel.assert_not_called()
        self.assertFalse(output)
