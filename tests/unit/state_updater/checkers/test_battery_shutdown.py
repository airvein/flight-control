from unittest.mock import Mock, patch

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.battery_shutdown import \
    BatteryShutdown
from tests.unit.state_updater.checkers.abstract_base_test import \
    AbstractBaseTest


class TestBatteryShutdown(AbstractBaseTest):
    def setUp(self) -> None:
        self.battery_shutdown = BatteryShutdown(
            self.telemetry, self.state_machine_controller)

    @patch('flight_control.state_updater.checkers.battery_shutdown.LowBattery')
    def test_manual_shutdown_checker_should_return_bool_when_pressed_or_not(
            self, mock_low_battery):
        low_battery_setting = 5.0
        mock_low_battery.SHUTDOWN = low_battery_setting
        cases = [
            {
                "state": StateNames.EMERGENCY_LANDED,
                "battery_value": 100.0,
                "expected_output": False
            },
            {
                "state": StateNames.EMERGENCY_LANDED,
                "battery_value": 0.0,
                "expected_output": True
            },
            {
                "state": StateNames.LN_LANDED,
                "battery_value": 100.0,
                "expected_output": False
            },
            {
                "state": StateNames.LN_LANDED,
                "battery_value": 0.0,
                "expected_output": True
            },
            {
                "state": StateNames.KILLED,
                "battery_value": 100.0,
                "expected_output": False
            },
            {
                "state": StateNames.KILLED,
                "battery_value": 0.0,
                "expected_output": True
            },
            {
                "state": "wrong_state",
                "battery_value": 100.0,
                "expected_output": False
            },
            {
                "state": "wrong_state",
                "battery_value": 0.0,
                "expected_output": False
            }
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.battery_shutdown.telemetry.sensors.button_press = Mock()
                self.battery_shutdown.telemetry.battery.remaining = \
                    case["battery_value"]
                self.battery_shutdown.state_machine_controller.state = case[
                    "state"]
                self.battery_shutdown.state_machine_controller.battery_shutdown = Mock()

                output = self.battery_shutdown.check()
                if (case['battery_value'] < low_battery_setting
                        and case["state"] != "wrong_state"):
                    self.battery_shutdown.state_machine_controller.battery_shutdown.assert_called_once()
                else:
                    self.battery_shutdown.state_machine_controller.battery_shutdown.assert_not_called()
                self.assertEqual(case['expected_output'], output)
