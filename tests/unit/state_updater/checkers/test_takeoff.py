"""takeoff.py

Tests for Takeoff checker class which checks if UAV start flying - is above ground

Copyright 2019 Cervi Robotics
"""

from unittest.mock import MagicMock

from drone_types import msg

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.takeoff import Takeoff
from tests.unit.state_updater.checkers.abstract_base_test import AbstractBaseTest


class TakeoffTest(AbstractBaseTest):
    def setUp(self) -> None:
        self.takeoff = Takeoff(self.telemetry, self.state_machine_controller)

    def test_drone_in_takeoff_should_return_TRUE_when_altitude_difference_is_ABOVE_0_5(self):
        start_point_alt = self.telemetry.route.start_point.altitude
        self.takeoff.telemetry.global_pos = msg.PxControlGlobalPos(
            timestamp=123, latitude=10.0, longitude=20.0, altitude=start_point_alt+0.51)

        output = self.takeoff._drone_in_takeoff()

        self.assertTrue(output)

    def test_drone_in_takeoff_should_return_FALSE_when_altitude_difference_is_BELOW_0_5(self):
        start_point_alt = self.telemetry.route.start_point.altitude

        self.takeoff.telemetry.global_pos = msg.PxControlGlobalPos(
            timestamp=123, latitude=10.0, longitude=20.0, altitude=start_point_alt-0.5)

        output = self.takeoff._drone_in_takeoff()

        self.assertFalse(output)

    def test_takeoff_checker_should_change_state_when_state_takeoff_start(self):
        self.takeoff.state_machine_controller.state = StateNames.TAKEOFF_START
        self.takeoff._drone_in_takeoff = MagicMock(return_value=True)
        self.takeoff.state_machine_controller.takeoff = MagicMock()

        output = self.takeoff.check()

        self.takeoff.state_machine_controller.takeoff.assert_called_once()
        self.assertTrue(output)

    def test_takeoff_checker_return_FALSE_when_state_is_NOT_takeoff_start(self):
        self.takeoff.state_machine_controller.state = "wrong_state"

        output = self.takeoff.check()

        self.assertFalse(output)

    def test_takeoff_checker_return_FALSE_when_drone_is_NOT_in_air(self):
        self.takeoff.state_machine_controller.state = StateNames.TAKEOFF_START
        self.takeoff._drone_in_takeoff = MagicMock(return_value=False)

        output = self.takeoff.check()

        self.assertFalse(output)
