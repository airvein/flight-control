"""abstract_base_test.py

The TestAbstractBase class which defines a setup to inherit for other classes

Copyright 2019 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from flight_control.commander.state_data import get_state_data
from flight_control.commander.telemetry import Telemetry
from flight_control.route.point import Point
from flight_control.route.zones import Zones
from tests.unit.fixtures.route_data import RouteData
from flight_control.commander.state_machine_controller import StateMachineController


class AbstractBaseTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        if cls is not AbstractBaseTest and cls.setUp is not AbstractBaseTest.setUp:
            original_setup = cls.setUp

            def setup_overwrite(self, *args, **kwargs):
                AbstractBaseTest.setUp(self)
                # noinspection PyArgumentList
                return original_setup(self, *args, **kwargs)

            cls.setUp = setup_overwrite

    def setUp(self) -> None:
        self.state_machine_controller = MagicMock(spec=StateMachineController,
                                                  state_data=get_state_data())
        self.setup_telemetry()

    def setup_telemetry(self):
        self.telemetry = Telemetry()
        self.telemetry.route = RouteData()
        self.telemetry.route.end_point = Point(
            id=24, latitude=50.01828, longitude=21.98508, altitude=210.0)
        self.telemetry.route.zones = Zones(
            home_maneuver=2, home_approach=3, landing_approach=10,
            landing_maneuver=11)
