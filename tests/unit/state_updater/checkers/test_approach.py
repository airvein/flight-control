"""test_approach.py

Tests for Approach checker class which checks if UAV come into approach zone

Copyright 2019 Cervi Robotics
"""

from unittest.mock import MagicMock, patch, PropertyMock, Mock

from flight_control.commander import StateNames
from flight_control.route.point import Point
from flight_control.state_updater.checkers.approach import Approach
from tests.unit.state_updater.checkers.abstract_base_test import AbstractBaseTest


class ApproachTest(AbstractBaseTest):
    def setUp(self) -> None:
        self.approach = Approach(self.telemetry, self.state_machine_controller)

    def test_approach_zone_achieved_should_return_TRUE_when_drone_come_to_zone_in_cruise(self):
        self.state_machine_controller.state = StateNames.CRUISE
        self.approach.telemetry.previous_point = Mock(spec=Point, id=10)

        output = self.approach._approach_zone_achieved

        self.assertTrue(output)

    def test_approach_zone_achieved_should_return_FALSE_when_uav_NOT_in_zone_in_cruise(self):
        self.state_machine_controller.state = StateNames.CRUISE
        self.approach.telemetry.previous_point = Mock(spec=Point, id=9)

        output = self.approach._approach_zone_achieved

        self.assertFalse(output)

    def test_approach_zone_achieved_in_cruise_RETURN_HOME_should_return_TRUE_when_uav_come_to_zone(
            self):
        self.state_machine_controller.state = StateNames.CRUISE_RETURN_HOME

        self.approach.telemetry.previous_point = Mock(spec=Point, id=3)

        output = self.approach._approach_zone_achieved

        self.assertTrue(output)

    def test_approach_zone_achieved_in_cruise_RETURN_HOME_should_return_FALSE_when_uav_NOT_in_zone(
            self):
        self.state_machine_controller.state = StateNames.CRUISE_RETURN_HOME
        self.approach.telemetry.previous_point = Mock(spec=Point, id=4)

        output = self.approach._approach_zone_achieved

        self.assertFalse(output)

    def test_approach_zone_achieved_should_return_FALSE_when_uav_state_is_wrong(self):
        self.approach.state_machine_controller.state = "wrong_state"

        output = self.approach.check()

        self.assertFalse(output)

    @patch.object(Approach, '_approach_zone_achieved', new_callable=PropertyMock, return_value=True)
    def test_check_should_trigger_transition_and_return_TRUE_when_condition_is_met(self, mock_checker):
        self.approach.state_machine_controller.state = "cruise"
        self.approach.state_machine_controller.in_approach = MagicMock()

        output = self.approach.check()

        self.approach.state_machine_controller.in_approach.assert_called_once()
        self.assertTrue(output)

    @patch.object(Approach, '_approach_zone_achieved', new_callable=PropertyMock, return_value=False)
    def test_check_should_NOT_trigger_transition_and_return_FALSE_when_condition_ISNT_met(self, mock_checker):
        self.approach.state_machine_controller.state = "any wrong state"
        self.approach.state_machine_controller.in_approach = MagicMock()

        output = self.approach.check()

        self.assertFalse(output)
        self.approach.state_machine_controller.in_approach.assert_not_called()
