"""manual_shutdown.py

Tests for ManualShutdown checker class which checks if UAV is turn off by using button

Copyright 2019 Cervi Robotics
"""

from unittest.mock import Mock, patch, PropertyMock

from periphery import SysfsGPIO

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.manual_shutdown import \
    ManualShutdown
from tests.unit.state_updater.checkers.abstract_base_test import \
    AbstractBaseTest


class ManualShutdownTest(AbstractBaseTest):
    def setUp(self) -> None:
        patcher = patch(
            'flight_control.state_updater.checkers.manual_shutdown.GPIO'
        )
        patcher.start()
        self.addCleanup(patcher.stop)
        self.manual_shutdown = ManualShutdown(
            self.telemetry, self.state_machine_controller)

    @patch.object(ManualShutdown, '_button_pressed', new_callable=PropertyMock)
    def test_manual_shutdown_checker_should_return_bool_when_pressed_or_not(
            self, mock_button_pressed):
        cases = [
            {
                "state": StateNames.EMERGENCY_LANDED,
                "button_pressed": True,
                "expected_output": True
            },
            {
                "state": StateNames.EMERGENCY_LANDED,
                "button_pressed": False,
                "expected_output": False
            },
            {
                "state": StateNames.KILLED,
                "button_pressed": True,
                "expected_output": True
            },
            {
                "state": StateNames.KILLED,
                "button_pressed": False,
                "expected_output": False
            },
            {
                "state": StateNames.LN_LANDED,
                "button_pressed": True,
                "expected_output": True
            },
            {
                "state": StateNames.LN_LANDED,
                "button_pressed": False,
                "expected_output": False
            },
            {
                "state": "wrong_state",
                "button_pressed": True,
                "expected_output": False
            },
            {
                "state": "wrong_state",
                "button_pressed": False,
                "expected_output": False
            }
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.manual_shutdown.telemetry.sensors.button_press = Mock()
                mock_button_pressed.return_value = case["button_pressed"]
                self.manual_shutdown.state_machine_controller.state = case[
                    "state"]
                self.manual_shutdown.state_machine_controller.man_shutdown = Mock()

                output = self.manual_shutdown.check()

                if case['button_pressed'] and case["state"] != "wrong_state":
                    self.manual_shutdown.state_machine_controller.man_shutdown.assert_called_once()
                else:
                    self.manual_shutdown.state_machine_controller.man_shutdown.assert_not_called()
                self.assertEqual(case['expected_output'], output)

    @patch('flight_control.state_updater.checkers.manual_shutdown.Power')
    def test_button_pressed_should_return_proper_value_when_not_using_button(
            self, mock_shutdown_settings):
        mock_shutdown_settings.USE_MANUAL_SHUTDOWN = False
        self.telemetry.sensors.button_press.shutdown = True

        self.assertTrue(self.manual_shutdown._button_pressed)

    @patch('flight_control.state_updater.checkers.manual_shutdown.Power')
    def test_button_pressed_should_return_proper_value_when_using_button(
            self, mock_shutdown_settings):
        mock_shutdown_settings.USE_MANUAL_SHUTDOWN = True
        self.manual_shutdown._shutdown_button = Mock(spec=SysfsGPIO)
        self.manual_shutdown._shutdown_button.read.return_value = True

        self.assertTrue(self.manual_shutdown._button_pressed)
