"""cruise.py

Test for Cruise checker class which checks if UAV left maneuver zone after takeoff

Copyright 2019 Cervi Robotics
"""

from unittest.mock import MagicMock, Mock

from flight_control.route.point import Point
from flight_control.state_updater.checkers.cruise import Cruise
from tests.unit.state_updater.checkers.abstract_base_test import \
    AbstractBaseTest


class CruiseTest(AbstractBaseTest):
    def setUp(self) -> None:
        self.cruise = Cruise(self.telemetry, self.state_machine_controller)

    def test_is_maneuver_border_achieved_in_takeoff_should_return_TRUE_when_id_is_THE_SAME_as_home_maneuver_point(self):
        self.cruise.telemetry.previous_point = Mock(spec=Point, id=2)

        output = self.cruise._achieved_maneuver_zone

        self.assertTrue(output)

    def test_is_maneuver_border_achieved_in_takeoff_should_return_TRUE_when_id_is_HIGHER_than_home_maneuver_point(self):
        self.cruise.telemetry.previous_point = Mock(spec=Point, id=15)

        output = self.cruise._achieved_maneuver_zone

        self.assertTrue(output)

    def test_cruising_checker_should_trigger_transition_and_return_TRUE_when_in_correct_state_and_route_id(self):
        self.cruise.state_machine_controller.state = "takeoff_monitor"
        self.cruise.telemetry.previous_point = Mock(spec=Point, id=2)
        self.cruise.state_machine_controller.cruising = MagicMock()

        output = self.cruise.check()

        self.cruise.state_machine_controller.cruising.assert_called_once()
        self.assertTrue(output)

    def test_cruising_checker_should_return_FALSE_when_point_id_is_LESSER_than_home_maneuver_point(self):
        self.cruise.state_machine_controller.state = "takeoff_monitor"
        self.cruise.telemetry.previous_point = Mock(spec=Point, id=1)
        self.cruise.state_machine_controller.cruising = MagicMock()

        output = self.cruise.check()

        self.cruise.state_machine_controller.cruising.assert_not_called()
        self.assertFalse(output)

    def test_cruising_checker_should_return_FALSE_when_NOT_in_state_takeoff_monitor(self):
        self.cruise.state_machine_controller.state = "wrong_state"
        self.cruise.state_machine_controller.cruising = MagicMock()

        output = self.cruise.check()

        self.cruise.state_machine_controller.cruising.assert_not_called()
        self.assertFalse(output)

