"""test_maneuver.py

Maneuver checker class which checks if UAV come into maneuver zone while landing

Copyright 2019 Cervi Robotics
"""
from unittest.mock import MagicMock, patch, PropertyMock, Mock

from flight_control.commander import StateNames, StateDataNames
from flight_control.route.point import Point
from flight_control.state_updater.checkers.maneuver import Maneuver
from tests.unit.state_updater.checkers.abstract_base_test import AbstractBaseTest


class ManeuverTest(AbstractBaseTest):
    def setUp(self) -> None:
        self.maneuver = Maneuver(self.telemetry, self.state_machine_controller)

    def test_is_maneuver_zone_achieved_should_return_bool_when_call_specific_conditions(self):
        cases = [
            {"rth": False, "previous_point_id": 11, "expected_output": True},
            {"rth": False, "previous_point_id": 10, "expected_output": False},
            {"rth": True, "previous_point_id": 2, "expected_output": True},
            {"rth": True, "previous_point_id": 3, "expected_output": False},
            {"rth": False, "previous_point_id": 11, "expected_output": True},
            {"rth": False, "previous_point_id": 10, "expected_output": False}
        ]
        for case in cases:
            with self.subTest(msg=case):
                self.state_machine_controller.state_data[StateDataNames.FLAGS][
                    StateDataNames.Flag.RTH] = case["rth"]
                self.maneuver.telemetry.previous_point = Mock(
                    spec=Point, id=case["previous_point_id"]
                )

                output = self.maneuver._achieved_maneuver_zone

                self.assertEqual(case["expected_output"], output)

    @patch.object(Maneuver, '_achieved_maneuver_zone', new_callable=PropertyMock)
    def test_maneuver_checker_should_return_expected_output_when_got_specific_arguments(self, mock_checker):
        cases = [
            {
                "state": StateNames.APPROACH,
                "zone_achieved": True,
                "expected_output": True
            },
            {
                "state": StateNames.APPROACH_CRUISE,
                "zone_achieved": False,
                "expected_output": False
            },
            {
                "state": "wrong_state",
                "zone_achieved": False,
                "expected_output": False
            }
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.maneuver.state_machine_controller.state = case["state"]
                mock_checker.return_value = case["zone_achieved"]

                if case["zone_achieved"]:
                    self.state_machine_controller.in_maneuver = MagicMock()

                output = self.maneuver.check()

                if case["zone_achieved"]:
                    self.state_machine_controller.in_maneuver.assert_called_once()

                self.assertEqual(case['expected_output'], output)
