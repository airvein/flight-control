from unittest.mock import Mock, MagicMock, patch

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.battery_land import \
    BatteryLand
from tests.unit.state_updater.checkers.abstract_base_test import \
    AbstractBaseTest


class TestBatteryLand(AbstractBaseTest):
    def setUp(self) -> None:
        self.battery_land = BatteryLand(
            self.telemetry, self.state_machine_controller)

    @patch('flight_control.state_updater.checkers.battery_land.LowBattery')
    def test_check_should_return_true_and_call_state_trigger_when_conditions_are_met(
            self, mock_low_battery):
        mock_low_battery.LAND = 10.0
        allowed_states = (
            StateNames.CRUISE,
            StateNames.CRUISE_RETURN_HOME,
            StateNames.EMERGENCY_CRUISE,
            StateNames.APPROACH_CRUISE,
            StateNames.APPROACH_CRUISE_WAIT,
            StateNames.APPROACH,
        )

        self.assertTupleEqual(allowed_states, self.battery_land.ALLOWED_STATES)

        for state in allowed_states:
            with self.subTest(msg=state):
                self.battery_land.telemetry.battery.remaining = 5.0
                self.battery_land.state_machine_controller.state = state
                self.state_machine_controller.land_now = Mock()

                output = self.battery_land.check()

                self.assertTrue(output)
                self.state_machine_controller.land_now.assert_called_once()

    def test_check_should_return_false_and_not_call_state_trigger_when_conditions_are_not_met(self):
        cases = [
            {
                "state": StateNames.CRUISE,
                "battery_value": 50.0,
            },
            {
                "state": "wrong_state",
                "battery_value": 100.0,
            },
            {
                "state": "wrong_state",
                "battery_value": 0.0,
            }
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.battery_land.telemetry.battery.remaining = \
                    case["battery_value"]
                self.battery_land.state_machine_controller.state = case["state"]
                self.state_machine_controller.land_now = Mock()

                output = self.battery_land.check()

                self.assertFalse(output)
                self.state_machine_controller.land_now.assert_not_called()
