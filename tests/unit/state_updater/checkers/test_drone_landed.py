"""test_drone_landed.py

Tests for DroneLanded checker class which checks if UAV landed - touch
ground in landing place.

Copyright 2019 Cervi Robotics
"""

from unittest.mock import Mock, MagicMock, PropertyMock, patch

from drone_types import msg
from flight_control.commander import StateNames
from flight_control.state_updater.checkers.drone_landed import DroneLanded
from tests.unit.state_updater.checkers.abstract_base_test import \
    AbstractBaseTest


class DroneLandedTest(AbstractBaseTest):
    def setUp(self) -> None:
        self.state_machine_controller.landed = Mock()

        self.drone_landed = DroneLanded(
            self.telemetry, self.state_machine_controller)

    def test_is_drone_landed_should_return_specific_output_when_called(self):
        cases = [
            {"expected_output": True,
             "flight_state": msg.PxControlStatus.FLIGHT_STATE_ON_GROUND},
            {"expected_output": False,
             "flight_state": msg.PxControlStatus.FLIGHT_STATE_UNDEFINED},
            {"expected_output": False,
             "flight_state": msg.PxControlStatus.FLIGHT_STATE_TAKEOFF},
            {"expected_output": False,
             "flight_state": msg.PxControlStatus.FLIGHT_STATE_IN_AIR},
            {"expected_output": False,
             "flight_state": msg.PxControlStatus.FLIGHT_STATE_LANDING},
            {"expected_output": False,
             "flight_state": msg.PxControlStatus.FLIGHT_STATE_KILLED},
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.telemetry.px_control_status.flight_state = case["flight_state"]

                output = self.drone_landed._drone_landed

                self.assertEqual(case["expected_output"], output)

    @patch.object(DroneLanded, '_drone_landed', new_callable=PropertyMock)
    def test_checker_should_trigger_transition_and_return_TRUE_when_conditions_are_met(self, mock_checker):
        correct_cases = [
            {"machine_state": StateNames.LANDING,
             "_is_drone_landed_value": True},
            {"machine_state": StateNames.EMERGENCY_LANDING,
             "_is_drone_landed_value": True}
        ]

        for case in correct_cases:
            with self.subTest(msg=case):
                self.state_machine_controller.state = case["machine_state"]
                mock_checker.return_value = case["_is_drone_landed_value"]

                output = self.drone_landed.check()

                self.state_machine_controller.landed.assert_called()
                self.assertTrue(output)

    @patch.object(DroneLanded, '_drone_landed', new_callable=PropertyMock)
    def test_checker_should_NOT_trigger_transition_and_return_FALSE_when_conditions_are_NOT_met(self, mock_checker):
        wrong_cases = [
            {"machine_state": StateNames.LANDING,
             "_drone_landed_value": False},
            {"machine_state": StateNames.EMERGENCY_LANDING,
             "_drone_landed_value": False},
            {"machine_state": "wrong_state",
             "_drone_landed_value": False},
            {"machine_state": "wrong_state",
             "_drone_landed_value": True}
        ]

        for case in wrong_cases:
            with self.subTest(msg=case):
                self.state_machine_controller.state = case[
                    "machine_state"]
                mock_checker.return_value = case["_drone_landed_value"]

                output = self.drone_landed.check()

                self.state_machine_controller.landed.assert_not_called()
                self.assertFalse(output)
