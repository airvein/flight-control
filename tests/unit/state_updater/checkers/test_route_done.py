"""test_route_done.py

Tests for RouteDone checker class which checks if UAV did route -
is exactly above landing point

Copyright 2019 Cervi Robotics
"""

from unittest.mock import Mock

from flight_control.commander import StateNames
from flight_control.state_updater.checkers.route_done import RouteDone
from tests.unit.state_updater.checkers.abstract_base_test import \
    AbstractBaseTest


class TestRouteDone(AbstractBaseTest):
    def setUp(self) -> None:
        self.route_done = RouteDone(
            self.telemetry, self.state_machine_controller)

    def test_check_should_return_TRUE_and_trigger_transition_when_called_in_proper_conditions(self):
        cases = [
            {"is_route_done": True, "state": StateNames.EMERGENCY_CRUISE},
            {"is_route_done": True, "state": StateNames.CRUISE_LAND}
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.route_done.state_machine_controller.route_done = Mock()
                self.route_done.state_machine_controller.state = case["state"]
                self.telemetry.route_done = case["is_route_done"]

                output = self.route_done.check()

                self.assertTrue(output)
                self.route_done.state_machine_controller.route_done.assert_called_once()

    def test_check_should_return_FALSE_and_NOT_trigger_transition_when_called_in_wrong_conditions(
            self):
        cases = [
            {"is_route_done": False, "state": StateNames.EMERGENCY_CRUISE},
            {"is_route_done": False, "state": StateNames.CRUISE_LAND},
            {"is_route_done": True, "state": "any_other_state"},
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.route_done.state_machine_controller.route_done = Mock()
                self.route_done.state_machine_controller.state = case["state"]
                self.telemetry.route_done = case["is_route_done"]

                output = self.route_done.check()

                self.assertFalse(output)
                self.route_done.state_machine_controller.route_done.assert_not_called()
