"""test_exit_waiting.py

Tests for ExitWaiting checker

Copyright 2020 Cervi Robotics
"""

from unittest.mock import patch

from flight_control.state_updater.checkers.exit_waiting import ExitWaiting
from tests.unit.state_updater.checkers.abstract_base_test import \
    AbstractBaseTest
from flight_control.commander import StateDataNames, StateNames
from flight_control.settings import MAX_WAITING_TIME


class TestExitWaiting(AbstractBaseTest):
    def setUp(self) -> None:
        self.state_machine_controller.state = StateNames.CRUISE
        self.exit_waiting = ExitWaiting(
            self.telemetry, self.state_machine_controller)

    @patch('flight_control.state_updater.checkers.exit_waiting.get_timestamp')
    def test_check_should_set_timestamp_when_is_None(self, mock_timestamp):
        mock_timestamp.return_value = 1111111111000
        self.state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = True

        self.assertIsNone(self.exit_waiting.LAST_TIMESTAMP)

        self.exit_waiting.check()

        self.assertEqual(self.exit_waiting.LAST_TIMESTAMP, mock_timestamp())

    def test_check_should_return_False_and_set_timestamp_to_None_when_not_waiting_or_incorrect_state(self):
        cases = [
            {'waiting': False, 'state': StateNames.CRUISE},
            {'waiting': True, 'state': StateNames.APPROACH_CRUISE_WAIT},
            {'waiting': False, 'state': StateNames.APPROACH_CRUISE_WAIT},
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.state_machine_controller.state_data[StateDataNames.FLAGS][
                    StateDataNames.Flag.WAITING] = case['waiting']
                self.state_machine_controller.state = case['state']

                output = self.exit_waiting.check()

                self.assertIsNone(self.exit_waiting.LAST_TIMESTAMP)
                self.assertFalse(output)

    @patch('flight_control.state_updater.checkers.exit_waiting.get_timestamp')
    def test_check_should_set_flags_when_time_delta_exceeds_max_time(self,
                                                                     mock_timestamp):
        mock_timestamp.return_value = 1111111111000
        self.exit_waiting.LAST_TIMESTAMP = mock_timestamp()
        self.state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = True

        mock_timestamp.return_value = (mock_timestamp() + MAX_WAITING_TIME)

        output = self.exit_waiting.check()

        self.assertTrue(output)
        self.assertFalse(
            self.state_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.WAITING])
        self.assertTrue(
            self.state_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.WAITING_TIME_EXCEEDED])

    @patch('flight_control.state_updater.checkers.exit_waiting.get_timestamp')
    def test_check_should_NOT_change_flags_when_time_delta_is_under_max_time(
            self, mock_timestamp):
        mock_timestamp.return_value = 1111111111000
        self.state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING_TIME_EXCEEDED] = False
        self.exit_waiting.LAST_TIMESTAMP = mock_timestamp()
        self.state_machine_controller.state_data[StateDataNames.FLAGS][
            StateDataNames.Flag.WAITING] = True

        mock_timestamp.return_value = (mock_timestamp() + MAX_WAITING_TIME - 1)

        output = self.exit_waiting.check()

        self.assertFalse(output)
        self.assertTrue(
            self.state_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.WAITING])
        self.assertFalse(
            self.state_machine_controller.state_data[StateDataNames.FLAGS][
                StateDataNames.Flag.WAITING_TIME_EXCEEDED])
        self.assertEqual(mock_timestamp.return_value,
                         self.exit_waiting.LAST_TIMESTAMP)
