"""test_emergency_route.py

Tests for EmergencyRoute class storing data of drone emergency track

Copyright 2019 Cervi Robotics
"""
import unittest

from flight_control.route.emergency_route import EmergencyRoute


class TestsEmergencyRoute(unittest.TestCase):
    def setUp(self) -> None:
        self.emergency_route = EmergencyRoute()

    def test_emergency_route_should_have_specified_arguments_when_initialized(self):
        self.assertIsNone(self.emergency_route.route_id)
        self.assertIsNone(self.emergency_route.route_points)


if __name__ == '__main__':
    unittest.main()
