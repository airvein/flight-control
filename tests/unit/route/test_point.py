"""test_point.py

Tests for Point class storing data points of drone track

Copyright 2019 Cervi Robotics
"""
import unittest

from flight_control.route.point import Point


class TestPoint(unittest.TestCase):
    def test_point_should__raise_error_when_initialized_without_arguments(self):
        with self.assertRaises(TypeError):
            Point()

    def test_point_should_have_specified_arguments_when_initialized(self):
        point = Point(id=1, latitude=50.1, longitude=20.2, altitude=30.3)

        self.assertEqual(point.id, 1)
        self.assertEqual(point.latitude, 50.1)
        self.assertEqual(point.longitude, 20.2)
        self.assertEqual(point.altitude, 30.3)

    def test_point_should_raise_TypeError_when_called_with_wrong_type_of_id(
            self):
        with self.assertRaises(TypeError):
            Point(id="1", latitude=50.1, longitude=20.2, altitude=30.3)

    def test_point_should_raise_TypeError_when_called_with_wrong_type_of_latitude(
            self):
        with self.assertRaises(TypeError):
            Point(id=1, latitude="50.1", longitude=20.2, altitude=30.3)

    def test_point_should_raise_TypeError_when_called_with_wrong_type_of_longitude(
            self):
        with self.assertRaises(TypeError):
            Point(id=1, latitude=50.1, longitude="20.2", altitude=30.3)

    def test_point_should_raise_TypeError_when_called_with_wrong_type_of_altitude(
            self):
        with self.assertRaises(TypeError):
            Point(id=1, latitude=50.1, longitude=20.2, altitude="30.3")

    def test_point_should_convert_id_type_to_int_when_float(self):
        point = Point(id=2.137, latitude=50.1, longitude=20.2, altitude=30.3)

        self.assertIsInstance(point.id, int)
        self.assertEqual(int(2.137), point.id)

    def test_point_should_convert_latitude_type_to_float_when_int(self):
        point = Point(id=1, latitude=50, longitude=20.1, altitude=30.2)

        self.assertIsInstance(point.latitude, float)
        self.assertEqual(float(50), point.latitude)

    def test_point_should_convert_longitude_type_to_float_when_int(self):
        point = Point(id=1, latitude=50.1, longitude=20, altitude=30.2)

        self.assertIsInstance(point.longitude, float)
        self.assertEqual(float(20), point.longitude)

    def test_point_should_convert_altitude_type_to_float_when_int(self):
        point = Point(id=1, latitude=50.1, longitude=20.2, altitude=30)

        self.assertIsInstance(point.altitude, float)
        self.assertEqual(float(30), point.altitude)

    def test_list_should_convert_point_to_list_when_called(self):
        point = Point(id=1, latitude=50.1, longitude=20, altitude=30.2)

        output = point.list()

        self.assertEqual(output[0], point.id)
        self.assertEqual(output[1], point.latitude)
        self.assertEqual(output[2], point.longitude)
        self.assertEqual(output[3], point.altitude)
