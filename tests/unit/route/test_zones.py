"""test_zones.py

Tests for Zones class storing zones data

Copyright 2019 Cervi Robotics
"""
import unittest

from flight_control.route.zones import Zones


class TestZones(unittest.TestCase):
    # def setUp(self) -> None:
    #     self.zones = Zones()

    def test_zones_should_have_specifed_arguments_when_initialized(self):
        expected_message = 'Zones MUST be called with arguments'
        with self.assertRaises(TypeError) as error:
            Zones()
        self.assertEqual(expected_message, str(error.exception))

    def test_zones_should_raise_TypeError_when_home_maneuver_type_is_INCORRECT(self):
        expected_message = 'home_maneuver of point MUST be integer'
        with self.assertRaises(TypeError) as error:
            Zones(
                home_maneuver="3",
                home_approach=4,
                landing_approach=10,
                landing_maneuver=11
            )
        self.assertEqual(expected_message, str(error.exception))

    def test_zones_should_raise_TypeError_when_home_approach_type_is_INCORRECT(self):
        expected_message = 'home_approach of point MUST be integer'
        with self.assertRaises(TypeError) as error:
            Zones(
                home_maneuver=3,
                home_approach="4",
                landing_approach=10,
                landing_maneuver=11
            )
        self.assertEqual(expected_message, str(error.exception))

    def test_zones_should_raise_TypeError_when_landing_approach_type_is_INCORRECT(self):
        expected_message = 'landing_approach of point MUST be integer'
        with self.assertRaises(TypeError) as error:
            Zones(
                home_maneuver=3,
                home_approach=4,
                landing_approach="10",
                landing_maneuver=11
            )
        self.assertEqual(expected_message, str(error.exception))

    def test_zones_should_raise_TypeError_when_landing_maneuver_type_is_INCORRECT(self):
        expected_message = 'landing_maneuver of point MUST be integer'
        with self.assertRaises(TypeError) as error:
            Zones(
                home_maneuver=3,
                home_approach=4,
                landing_approach=10,
                landing_maneuver="11"
            )
        self.assertEqual(expected_message, str(error.exception))

    def test_zones_instance_should_be_created_when_all_is_OK(self):
        zones = Zones(
            home_maneuver=3,
            home_approach=4,
            landing_approach=10,
            landing_maneuver=11
        )
        self.assertIsInstance(zones, Zones)
