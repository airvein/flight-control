"""test_route.py

Tests for Route class storing data of drone track

Copyright 2019 Cervi Robotics
"""
import json
import unittest
from typing import Iterable

from flight_control.route.route import Route, EmergencyRoute, Point, ParseError

FIXTURES_PATH = "/".join(__file__.split("/")[:-2]) + '/fixtures/'


class TestsRoute(unittest.TestCase):
    @staticmethod
    def _read_json_data():
        with open(FIXTURES_PATH + 'track.json') as f:
            data = f.read()
            return data

    def setUp(self) -> None:
        self.json_data = self._read_json_data()
        self.data = json.loads(self.json_data)

        self.route = Route()

    def test_route_should_have_specified_arguments_when_initialized(self):
        self.assertIsNone(self.route.route_id)
        self.assertIsNone(self.route.start_point)
        self.assertIsNone(self.route.end_point)
        self.assertIsNone(self.route.route_points)
        self.assertIsNone(self.route.zones)
        self.assertIsNone(self.route.route_exit_points)
        self.assertFalse(self.route.emergency_routes)
        self.assertIsNone(self.route.user_points)

    def test_parse_route_id_should_parse_route_id_when_called(self):
        self.route._parse_route_id(self.data)
        self.assertEqual(self.route.route_id, "1000")

    def test_parse_start_point_should_parse_start_point_when_called(self):
        expected_point = Point(id=0, latitude=50.01828, longitude=21.98508,
                               altitude=210.0)
        expected_yaw = 21.37

        self.route._parse_start_point(self.data)

        self.assertEqual(self.route.start_point.id, expected_point.id)
        self.assertEqual(self.route.start_point.latitude, expected_point.latitude)
        self.assertEqual(self.route.start_point.longitude, expected_point.longitude)
        self.assertEqual(self.route.start_point.altitude, expected_point.altitude)
        self.assertEqual(self.route.start_yaw, expected_yaw)

    def test_parse_start_point_should_return_IndexError_when_incorrect_type_of_start_point(self):
        data = {"start_point": [0, 50.01828, 21.98508]}
        self.assertRaises(ParseError, self.route._parse_start_point, data)

    def test_from_json_should_parse_end_point_when_called(self):
        expected_point = Point(id=24, latitude=50.01828, longitude=21.98508,
                               altitude=210.0)
        expected_yaw = 37.21

        self.route._parse_end_point(self.data)

        self.assertEqual(self.route.end_point.id, expected_point.id)
        self.assertEqual(self.route.end_point.latitude, expected_point.latitude)
        self.assertEqual(self.route.end_point.longitude, expected_point.longitude)
        self.assertEqual(self.route.end_point.altitude, expected_point.altitude)
        self.assertEqual(self.route.end_yaw, expected_yaw)

    def test_parse_start_point_should_return_IndexError_when_incorrect_type_of_end_point(self):
        data = {"end_point": [0, 50.01828, 21.98508]}
        self.assertRaises(ParseError, self.route._parse_end_point, data)

    def test_from_json_should_parse_route_points_when_called(self):
        self.route._parse_route_points(self.data)

        self.assertIsInstance(self.route.route_points, Iterable)

    def test_parse_route_points_should_error_when_called_incomplete_data(self):
        data = {"route_points": [
            [0, 50.01828, 21.98508],
            [1, 50.01828, 21.98508, 25.0],
            [2, 50.01818920190005, 21.984901798695162, 25.0],
        ]}

        self.assertRaises(ParseError, self.route._parse_route_points, data)

    def test_get_route_points_should_error_when_called_id_with_wrong_data(self):
        data = {"route_points": [
            ["Error string", 50.01828, 21.98508, 0.0],
            [1, 50.01828, 21.98508, 25.0],
            [2, 50.01818920190005, 21.984901798695162, 25.0],
        ]}

        self.assertRaises(TypeError, self.route._get_route_points, data)

    def test_get_route_points_should_error_when_called_latitude_with_wrong_data(self):
        data = {"route_points": [
            [0, "Error string", 21.98508, 0.0],
            [1, 50.01828, 21.98508, 25.0],
            [2, 50.01818920190005, 21.984901798695162, 25.0],
        ]}

        self.assertRaises(TypeError, self.route._get_route_points, data)

    def test_get_route_points_should_error_when_called_longitude_with_wrong_data(self):
        data = {"route_points": [
            [0, 50.01828, "Error string", 0.0],
            [1, 50.01828, 21.98508, 25.0],
            [2, 50.01818920190005, 21.984901798695162, 25.0],
        ]}

        self.assertRaises(TypeError, self.route._get_route_points, data)

    def test_get_route_points_should_error_when_called_altitude_with_wrong_data(self):
        data = {"route_points": [
            [0, 50.01828, 21.98508, "Error string"],
            [1, 50.01828, 21.98508, 25.0],
            [2, 50.01818920190005, 21.984901798695162, 25.0],
        ]}

        self.assertRaises(TypeError, self.route._get_route_points, data)

    def test_get_route_points_should_return_list_of_points_when_called(
            self):
        points = [
            [0, 50.01828, 21.98508, 0.0],
            [1, 50.01828, 21.98508, 25.0],
            [2, 50.01818920190005, 21.984901798695162, 25.0],
            [3, 50.018187294901686, 21.984794683045113, 25.0],
            [4, 50.01823563582035, 21.98462545500784, 24.0],
        ]

        route_points = self.route._get_route_points(points)

        self.assertEqual(route_points[0], (0, 50.01828, 21.98508, 0.0))
        self.assertEqual(route_points[1], (1, 50.01828, 21.98508, 25.0))
        self.assertEqual(route_points[2], (2, 50.01818920190005, 21.984901798695162, 25.0))
        self.assertEqual(route_points[3], (3, 50.018187294901686, 21.984794683045113, 25.0))
        self.assertEqual(route_points[4], (4, 50.01823563582035, 21.98462545500784, 24.0))

    def test_parse_zones_should_return_zones_class_when_called(self):
        zones = self.route._parse_zones(self.data)

        self.assertEqual(zones, self.route.zones)
        self.assertEqual(2, self.route.zones.home_maneuver)
        self.assertEqual(3, self.route.zones.home_approach)
        self.assertEqual(21, self.route.zones.landing_approach)
        self.assertEqual(22, self.route.zones.landing_maneuver)

    def test_parse_route_exit_points_should_return_list_and_save_as_class_argument_when_called(self):
        route_exit_points = self.route._parse_route_exit_points(self.data)

        self.assertEqual(route_exit_points, [5, 16])
        self.assertEqual(self.route.route_exit_points, [5, 16])

    def test_get_emergency_routes_should_return_emergency_routes_when_called(self):
        emergency_routes = self.route._get_emergency_routes(self.data)

        self.assertIsInstance(emergency_routes, list)
        self.assertEqual(len(emergency_routes), 2)
        self.assertIsInstance(emergency_routes[0], EmergencyRoute)

    def test_parse_emergency_routes_should_return_list_of_emergency_routes_when_called(self):
        emergency_routes = self.route._parse_emergency_routes(self.data)

        self.assertEqual(len(self.route.emergency_routes), 2)
        self.assertIsInstance(emergency_routes, list)
        self.assertIsInstance(self.route.emergency_routes, list)
        self.assertIsInstance(self.route.emergency_routes[0], EmergencyRoute)

    def test_parse_emergency_routes_should_raise_error_when_called_incomplete_data(self):
        data = {"emergency_routes_points": {"5": [
            [0, 50.01828, 21.98508],
            [1, 50.01828, 21.98508, 25.0],
            [2, 50.01818920190005, 21.984901798695162, 25.0],
        ]}}

        self.assertRaises(ParseError, self.route._parse_emergency_routes, data)

    def test_parse_user_points_should_return_list_integers_and_save_as_class_arguments_when_called(self):
        expected_output = [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
        ]

        user_points = self.route._parse_user_points(self.data)

        self.assertEqual(expected_output, user_points)
        self.assertEqual(expected_output, self.route.user_points)

    def test_from_JSON_should_raise_ParseError_when_with_wrong_data(self):
        wrong_json = self.json_data.replace(",\n    210.0,\n  ", "")
        self.assertRaises(ParseError, Route.from_json, wrong_json)

    def test_from_JSON_should_raise_ParseError_when_with_wrong_data_with_exception_JSONDecodeError(self):
        wrong_json = self.json_data.replace("210.0", "")
        self.assertRaises(ParseError, Route.from_json, wrong_json)

    def test_from_JSON_should_parse_JSON_file_to_Route_objects_when_called(self):
        route = Route.from_json(self.json_data)

        self.assertIsNotNone(route.route_id)
        self.assertIsNotNone(route.start_point)
        self.assertIsNotNone(route.end_point)
        self.assertIsNotNone(route.route_points)
        self.assertIsNotNone(route.zones.home_maneuver)
        self.assertIsNotNone(route.zones.home_approach)
        self.assertIsNotNone(route.zones.landing_approach)
        self.assertIsNotNone(route.zones.landing_maneuver)
        self.assertIsNotNone(route.route_exit_points)
        self.assertIsNotNone(route.emergency_routes)
        self.assertIsNotNone(route.user_points)
        self.assertIsNotNone(route.start_yaw)
        self.assertIsNotNone(route.end_yaw)
