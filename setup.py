from setuptools import setup

package_name = 'flight_control'

setup(
    name=package_name,
    version='2.0.0',
    py_modules=[
        'fc_run'
    ],
    packages=[
        'flight_control'
    ],
    data_files=[
        ('share/' + package_name, [
            'package.xml',
        ]),
    ],
    install_requires=[
        'setuptools',
        'launch',
        'pymap3d'
    ],
    zip_safe=True,
    author='Marcin Kornat, Andrzej Michalski',
    author_email='marcin.kornat@cervirobotics.com, andrzej.michalski@cervirobotics.com',
    maintainer='Marcin Kornat, Andrzej Michalski',
    maintainer_email='marcin.kornat@cervirobotics.com, andrzej.michalski@cervirobotics.com',
    classifiers=[
        'Topic :: Software Development :: Embedded Systems'
    ],
    keywords=['ROS'],
    description=['Flight Control module'],
    license='Copyright 2018 Cervi Robotics sp. z o.o, all rights reserved',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'fc_run = fc_run:main',
        ]
    }

)
